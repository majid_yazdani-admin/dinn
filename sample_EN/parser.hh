# IMPORTANT: 
# Read first *ih format description before starting to read this description
#
#
# Format:
# [FIND_LAST|FIND_FIRST] ORIG_WORD_SPEC TARG_WORD_SPEC [FOLLOW_ACTION] [OFFSET]
# Where
# FIND_LAST/FIND_FIRST indicates whether the most recent state (FIND_LAST) or the 
# the most remote state in the history (FIND_FIRST) is to be preferred. If more 
# than one state correspond to the following specification then this preference 
# will be taken into account.
#
# ORIG_WORD_SPEC defines a discrete function which returns a word in respect to 
# current position t
# TARG_WORD_SPEC defines a discrete function which returns a word in respect to 
# some position t' in history
# If these 2 words are the same (refer to the same entity/index in the sentence)
# then latent state vectors t' and t  can be connected.
# The parser connects not more than one pair of states per type, taking into account 
# FIND_LAST/FIND_FIRST preference
#
#   ORIG_WORD_SPEC is a specification of the word at current position t :
#           SRC_STRUCTURE OFFSET_IN_STRUCTURE HEAD_TRANSITION_N R/L_CHILD_TRANSITIONS_N R/L_SIBLING_TRANSITIONS
#   i.e. it is the same as specification as used in MALTParser feature model (and our *ih file)
#   but without offset in the input sequence. 
#   Example of ORIG_WORD_SPEC: 
#   FIND_LAST STACK 0 0 1 0 - rightmost right child of the current TOP
#
#   TARG_WORD_SPEC is a specification of the word for word at some previous position t'. It has
#   simpler specification ORIG_WORD_SPEC
#           SRC_STRUCTURE OFFSET_IN_STRUCTURE
#   Example of TARG_WORD_SPEC:
#   STACK 0 - top of the stack
#   
#   Additional restriction can be placed: only the parser state
#   which were followed by a particular decision can be connected to the current
#   state:
#   FOLLOW_ACTION SHIFT|LA|RA|RED|ANY, ANY - is default
#
#   Also we can place another restriction:
#   OFFSET defines that only the state with the time offset OFFSET can be considered.
#   If it matches - it will be connected, if not - no state will be connected
#   E.g. OFFSET of 1 is previous state. 2 - one more state before.

#Connect the most recent state with the same queue
FIND_LAST INPUT 0 0 0 0 INPUT 0

#Connect the most recent state with the same element on top of the stack
FIND_LAST STACK 0 0 0 0 STACK 0

#Connect the most recent state where current rightmost right child of TOP was at TOP 
#FIND_LAST STACK 0 0 1 0 STACK 0

#... leftmost left child of TOP was at TOP
#FIND_LAST STACK 0 0 -1 0 STACK 0

#... head of current TOP was at TOP
#FIND_LAST STACK 0 1 0 0 STACK 0

#... leftmost child of queue tail was at TOP
#FIND_LAST INPUT 0 0 -1 0 STACK 0

# This operation is added to guarantee that previous state is always
# connected to the current state, so that information can
# flow between any 2 states in the model
# Previous state is connected if its FRONT of queue is current TOP of
# the stack
FIND_LAST STACK 0 0 0 0 INPUT 0 ANY 1


