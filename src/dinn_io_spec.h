#ifndef _IDP_IO_SPEC_H 
#define _IDP_IO_SPEC_H 


//NB: *_INP_* are assumed to be at least as large as *_OUT_*
#define MAX_DEPREL_SIZE 200

#define MAX_POS_INP_SIZE 50
#define MAX_POS_OUT_SIZE 50
#define MAX_FEAT_INP_SIZE 1
#define MAX_FEAT_OUT_SIZE 1
#define MAX_WORD_INP_SIZE 13103
#define MAX_WORD_OUT_SIZE 13103


//to be used only in inputs
#define MAX_CPOS_SIZE 49
#define MAX_WORD_SIZE 36657
#define MAX_LEMMA_SIZE 29425
#define MAX_ELFEAT_SIZE 1




//set MAX_OUT_SIZE to MAX of *_OUT_* and MAX_DEPREL_SIZE
#define MAX_OUT 2*MAX_DEPREL_SIZE+3

//maximum length of a sentence (hardcoded, not computed)
#define MAX_SENT_LEN 400

//maximum number of arguments (hardcoded, not computed)
#define MAX_ARG_NUM 45

//maximum number of different roles (per treebank)
#define MAX_ROLE_SIZE 53

//maximum number of senses (per lemma per treebank)
#define MAX_SENSE_SIZE 11

#endif
