/*
    Majid Yazdani and James Henderson

	Incremental Recurrent Neural Network Dependency Parser with Search-based Discriminative Training , CoNLL 2015
*/
#include "dinn.h"


int st_look(STACK *st, int i) {
    if (st->size > i) {
        return st->elems[st->size - i - 1];
    } else {
        return 0;
    }
}

// returns top of stack or -1 if not available
int st_peek(STACK *st) {
    return st_look(st, 0);
}

int st_pop(STACK *st) {
    if (st->size > 0) {
        return st->elems[(st->size--) - 1];
    } else {
        return 0;
    }
}

void st_push(STACK *st, int x) {
    ASSERT(st->size < MAX_SENT_LEN);
    st->elems[(st->size++)] = x;
}
