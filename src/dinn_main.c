/*
    Majid Yazdani and James Henderson

	Incremental Recurrent Neural Network Dependency Parser with Search-based Discriminative Training , CoNLL 2015
*/
#include "dinn.h"


int print_usage() {
    fprintf(stderr, "Usage: ./dinn [-parse|-train] settings_file [ADDITIONAL_PARAMETER=VALUE]*\n");
    fprintf(stderr,"\t-train  trains the model\n");
    fprintf(stderr,"\t-parse  parses the files\n");
    exit(1);
}


void process_train(APPROX_PARAMS *ap, MODEL_PARAMS *mp, char* WEfile) {
    validation_training(ap, mp, WEfile);

}

void process_parse(APPROX_PARAMS *ap, MODEL_PARAMS *mp) {
    char best_weights_fname[MAX_NAME];
    strcpy(best_weights_fname, ap->model_name);
    strcat(best_weights_fname, ".best.wgt");
    load_weights(ap, mp, best_weights_fname);
    testing(ap, mp);
}



int main(int argc, char **argv) {
    

    if (argc < 3) {
        print_usage();
    }
   
    char *mode = argv[1];
    char *set_fname = argv[2];
    char *WEfile = argv[3];
    
    APPROX_PARAMS *ap = read_params(set_fname);
    rewrite_parameters(ap, argc, argv, 4);
    
    char *sap = get_params_string(ap);
    printf("Parameters used\n===============\n%s===============\n", sap);
    free(sap);
            
    
    srand48(ap->seed);
  
    DEF_ALLOC(mp, MODEL_PARAMS);
    read_io_spec(ap->io_spec_file, mp);

    read_ih_links_spec(ap->ih_links_spec_file, mp, mp->ih_links_specs, &mp->ih_links_num);
    read_hh_links_spec(ap->hh_links_spec_file, mp);

    allocate_weights(ap, mp);
    if (strcmp(mode, "-parse") == 0) {
        process_parse(ap, mp);

    } else if (strcmp(mode, "-train") == 0) {
        if (ap->train_dataset_size == 0) {
            ap->train_dataset_size = MAXINT;
        }
        process_train(ap, mp, WEfile);

    } else {
        fprintf(stderr, "Error: mode '%s' is not supported\n", mode);
        print_usage();
    }
    
    free(ap);
    free_weights(mp, ap);
    free(mp);
    mp = NULL;
    return 0; 
}    

