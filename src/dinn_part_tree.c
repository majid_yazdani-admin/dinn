/*
    Majid Yazdani and James Henderson

	Incremental Recurrent Neural Network Dependency Parser with Search-based Discriminative Training , CoNLL 2015
*/
#include "dinn.h"

PART_TREE *pt_alloc(int sent_len) {
    DEF_ALLOC(pt, PART_TREE);
    pt->len = sent_len;
    ALLOC_ARRAY(pt->nodes, NODE, sent_len + 1);
    int i;
    for (i = 0; i < sent_len + 1; i++) {
        pt->nodes[i].head = 0;
        pt->nodes[i].pos = 0;
        pt->nodes[i].deprel = ROOT_DEPREL;
        pt->nodes[i].child_num = 0;
        //ALLOC_ARRAY(pt->nodes[i].childs, int, sent_len);
    }
    return pt;
}
int pt_pos(PART_TREE *pt, int i) {
    return  pt->nodes[i].pos;
}

PART_TREE *pt_clone(PART_TREE *pt) {
    PART_TREE *ptc = pt_alloc(pt->len);
    memcpy(ptc->nodes, pt->nodes, sizeof(NODE) * (pt->len + 1)); 
    return ptc;
}

void pt_free(PART_TREE **pt) {
/*    int i;
    for (i = 0; i < pt->len + 1; i++) {
        free(pt->nodes[i].childs);
    }     */
    free((*pt)->nodes);
    free(*pt);
    *pt = NULL;
}

//leftmost child of i, zero if no
int pt_lc(PART_TREE *pt, int i) {
    ASSERT(i <= pt->len);
    if (i == 0) {
        return 0;
    }
    if (pt->nodes[i].child_num == 0) {
        return 0;
    } else {
        return pt->nodes[i].childs[0];
    }
}

//leftmost left child of i, zero if no left children
int pt_llc(PART_TREE *pt, int i) {
    ASSERT(i <= pt->len);
    if (i == 0) {
        return 0;
    }
    if (pt->nodes[i].child_num == 0) {
        return 0;
    } else {
        if (pt->nodes[i].childs[0] < i) {
            return pt->nodes[i].childs[0];
        } else {
            return 0;
        }
    }
}


//rightmost child of i, zero if no
int pt_rc(PART_TREE *pt, int i) {
    ASSERT(i <= pt->len);
    if (i == 0) {
        return 0;
    }
    if (pt->nodes[i].child_num == 0) {
        return 0;
    } else {
        return pt->nodes[i].childs[pt->nodes[i].child_num - 1];
    }
}

//rightmost right child of i, zero if no
int pt_rrc(PART_TREE *pt, int i) {
    ASSERT(i <= pt->len);
    if (i == 0) {
        return 0;
    }
    if (pt->nodes[i].child_num == 0) {
        return 0;
    } else {
        if (pt->nodes[i].childs[pt->nodes[i].child_num - 1] > i) {
            return pt->nodes[i].childs[pt->nodes[i].child_num - 1];
        } else {
            return 0;
        }
    }
}

//left sibling of i, zero if no
int pt_ls(PART_TREE *pt, int i) {
    if (i == 0) {
        return 0;
    }
    int h = pt_head(pt, i);
    if (h == 0) {
        return 0;
    }
    
    int c = 0;
    while (pt->nodes[h].childs[c] != i) {
        c++;
    }
    
    if (c == 0) {
        return 0;
    } else {
        return pt->nodes[h].childs[c-1];
    }
}

//right sibling of i, zero if no
int pt_rs(PART_TREE *pt, int i) {
    if (i == 0) {
        return 0;
    }
    int h = pt_head(pt, i);
    if (h == 0) {
        return 0;
    }
    
    int c = 0;
    while (pt->nodes[h].childs[c] != i) {
        c++;
    }
   
    if (c == pt->nodes[h].child_num - 1) {
        return 0;
    } else {
        return pt->nodes[h].childs[c + 1];
    }
}

void pt_add_link(PART_TREE *pt, int h, int d, int deprel) {
    ASSERT(d > 0 && h >= 0);
    pt->nodes[d].head = h;
    pt->nodes[d].deprel = deprel;

    if (h > 0) {
        int i;
        for (i = pt->nodes[h].child_num - 1; i >= 0; i--) {
            int c_d = pt->nodes[h].childs[i];
            if (c_d > d) {
                pt->nodes[h].childs[i + 1] = c_d;
            } else {
                ASSERT(c_d != d);
                break;     
            }
        }
        
        pt->nodes[h].childs[i + 1] = d;
        pt->nodes[h].child_num++;
    } 
}
void pt_add_pos(PART_TREE *pt, int wrd, int pos) {
    ASSERT(wrd > 0 && wrd <= pt->len + 1 && pos >= 0);
    pt->nodes[wrd].pos = pos;
    //printf("pos(%d)=%d  ", wrd, pos);
}

void pt_fill_sentence(PART_TREE *pt, MODEL_PARAMS *mp, SENTENCE *sent) {
    ASSERT(pt->len == sent->len);
    int t;
    for (t = 1; t <= sent->len; t++) {

        sent->head[t] = pt->nodes[t].head;
        sent->deprel[t] = pt->nodes[t].deprel;
        if (sent->deprel[t] == ROOT_DEPREL) {
            strcpy(sent->s_deprel[t], mp->s_deprel_root);
        } else {
            strcpy(sent->s_deprel[t], mp->s_deprel[sent->deprel[t]]);
        }
    }
}

