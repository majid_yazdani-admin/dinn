/*
    Majid Yazdani and James Henderson

	Incremental Recurrent Neural Network Dependency Parser with Search-based Discriminative Training , CoNLL 2015
*/

#include "dinn.h"

#define get_pos_out_num() ((ap->input_offset > 0) ? mp->pos_info_out.num + ap->input_offset : mp->pos_info_out.num) 


STATE *root_prediction(APPROX_PARAMS *ap, MODEL_PARAMS *mp, SENTENCE *sent) {//, FILE* fp


    STACK st; st.size = 0;
    STATE *a_state = create_state(-1, -1, NULL, 1, &st, 1, pt_alloc(sent->len), &mp->out_link, MAX_OUT);

    set_links(ap, mp, a_state, sent);//, fp
    comp_state(ap, mp, a_state);
    fill_lprob(a_state);

    return a_state;
}





void add_to_best_list(APPROX_PARAMS *ap, STATE **new_b_states, int *new_cand_num, STATE *to_add) {

    ASSERT(*new_cand_num < ap->beam || to_add->lprob > new_b_states[ap->beam - 1]->lprob);


    
    int i; 
    for (i = *new_cand_num - 1; i >= 0; i--) {
        if (new_b_states[i]->lprob < to_add->lprob) {
            new_b_states[i + 1] = new_b_states[i];
        } else {
            new_b_states[i + 1] = to_add;
            break;
        }
    }

    if (i == -1) {
        new_b_states[0] = to_add;
    }
    if (*new_cand_num == ap->beam) {
        pt_free(&(new_b_states[ap->beam]->pt));
        free_states(new_b_states[ap->beam]);
    } else {
        (*new_cand_num)++;
    }
    return;         
    
}


STATE_LIST *process_a_state(APPROX_PARAMS *ap, MODEL_PARAMS *mp,  SENTENCE *sent, STATE **new_b_states, int *new_cand_num,
        STATE *a_state, STATE_LIST *ol) {//, FILE* fp

	ASSERT(a_state->outputs.num == MAX_OUT);
    
    double min_lprob = - MAXDOUBLE;

    if (*new_cand_num == ap->beam) {
        min_lprob = new_b_states[ap->beam - 1]->lprob; //- max_word_pred_lprob(ap, mp, sent, new_b_states, *new_cand_num,  a_state);

    }
    
    //do not need to consider
    if (a_state->lprob < min_lprob) {
        pt_free(&(a_state->pt));
        free_states(a_state);
        return ol;
    }

    ACTION_SET as = get_next_actions(ap, mp, a_state);

    set_mask(mp, a_state, &as);
    comp_state(ap, mp, a_state);

    //check SHIFT , correctness @ shifts, chunk based decoding
    if (as.acts[SHIFT]) {
        if (log(a_state->outputs.q[SHIFT]) + a_state->lprob+ log(a_state->outputs.q[MAX_OUT-1])  >= min_lprob) { //

        	STATE *as_a_state = create_state( SHIFT, SHIFT, a_state, a_state->period+1, &a_state->stack, a_state->queue, pt_clone(a_state->pt), &mp->out_link,
                    MAX_OUT);
            st_push(&as_a_state->stack, as_a_state->queue);
			as_a_state->queue++;

            set_links(ap, mp, as_a_state, sent);//,fp

            comp_state(ap, mp, as_a_state);


            fill_lprob(as_a_state);

           	add_to_best_list(ap, new_b_states, new_cand_num, as_a_state);

        }
    }

    //update
    if (*new_cand_num == ap->beam) {
        min_lprob = new_b_states[ap->beam - 1]->lprob;

    }
          
    //check LA
    if (as.acts[LA]) {
        int d = st_peek(&a_state->stack);
        int h = a_state->queue;
        int ac = 2;
        int maxac =2;
        double max = -MAXDOUBLE;
        for(ac = 2;ac< MAX_DEPREL_SIZE+2;ac++){
        	if(max < a_state->outputs.q[ac]){
        		max = a_state->outputs.q[ac];
        		maxac = ac;
        	}
        }

        if (log(a_state->outputs.q[maxac]) + a_state->lprob >= min_lprob) {//+log(a_state->outputs.q[MAX_OUT-1])

        		STATE *as_a_state = create_state( LA, maxac, a_state, a_state->period, &a_state->stack, a_state->queue, pt_clone(a_state->pt), &mp->out_link, MAX_OUT);

				pt_add_link(as_a_state->pt, h, d, maxac-2);

        	    st_pop(&as_a_state->stack);
				set_links(ap, mp, as_a_state, sent);//, fp
				fill_lprob(as_a_state);

				ol = increase_list_asc(ol, as_a_state);


			}

        }

    //check RA

    if (as.acts[RA]) {

        int d = a_state->queue;
        int h = st_peek(&a_state->stack);
        int ac;
        int maxac = MAX_DEPREL_SIZE+2;
        double max = -MAXDOUBLE;
        for(ac = MAX_DEPREL_SIZE+2;ac< MAX_OUT-1;ac++){
        	if(max < a_state->outputs.q[ac]){
        		max = a_state->outputs.q[ac];
        		maxac = ac;
        	}
        }
		if (log(a_state->outputs.q[maxac]) + a_state->lprob >= min_lprob) {//+log(a_state->outputs.q[MAX_OUT-1])

            	STATE *as_a_state = create_state( RA,maxac, a_state, a_state->period, &a_state->stack, a_state->queue, pt_clone(a_state->pt), &mp->out_link, MAX_OUT);

				pt_add_link(as_a_state->pt, h, d, maxac-2-MAX_DEPREL_SIZE);


                set_links(ap, mp, as_a_state, sent);//,fp
                fill_lprob(as_a_state);

                ol = increase_list_asc(ol, as_a_state);

            }




	}


    //check RED
    if (as.acts[RED]) {
        if (log(a_state->outputs.q[RED]) + a_state->lprob>= min_lprob) {// +log(a_state->outputs.q[MAX_OUT-1])
            STATE *as_a_state = create_state( RED, RED, a_state, a_state->period, &a_state->stack, a_state->queue, pt_clone(a_state->pt), &mp->out_link,
                    MAX_OUT);
            st_pop(&as_a_state->stack);
            set_links(ap, mp, as_a_state, sent);//,fp
            fill_lprob(as_a_state);
            ol = increase_list_asc(ol, as_a_state);

        }
    }
  
    if (a_state->next_states->prev == NULL) {
        free_states(a_state);
    }
    return ol;
}

int Chunk_Beam_Search(APPROX_PARAMS *ap, MODEL_PARAMS *mp, SENTENCE *sent, STATE **prev_b_states, int prev_cand_num, STATE **new_b_states) {//, FILE* fp
    STATE_LIST *ol = increase_list(NULL, NULL);
    int i;
    for (i = 0; i < prev_cand_num; i++) {
        increase_list_asc(ol, prev_b_states[i]);
    }

    int new_cand_num = 0;
    int c=0;
    while(ol->prev != NULL) {
        STATE *a_state = ol->prev->elem;
        ol = reduce_list(ol);
        c++;
        process_a_state(ap, mp, sent, new_b_states, &new_cand_num, a_state, ol);
    }

    free(ol);
    return new_cand_num;
}

double parse_sentence(APPROX_PARAMS *ap, MODEL_PARAMS *mp, SENTENCE *sent) {//, FILE* fp
    ASSERT(ap->beam >= 1 && ap->beam < MAX_BEAM);    


    STATE  *best_states[MAX_SENT_LEN + 1][MAX_BEAM + 1];

    best_states[0][0] = root_prediction(ap, mp, sent);//, fp
    int cand_num = 1; 

    int t;
    for (t = 1; t < sent->len + 1; t++) {
        cand_num = Chunk_Beam_Search(ap, mp, sent, best_states[t-1], cand_num, best_states[t]);//,fp
        ASSERT(cand_num <= ap->beam);
    }
    double best_lprob = best_states[sent->len][0]->lprob;
    pt_fill_sentence(best_states[sent->len][0]->pt, mp, sent);

    save_candidates(ap, mp, sent, best_states[sent->len], cand_num);
    
    int i;
    for (i = 0; i < cand_num; i++) {
        pt_free(&(best_states[sent->len][i]->pt));
        free_states(best_states[sent->len][i]);
    }
    return best_lprob;
}
