/*
    Majid Yazdani and James Henderson

	Incremental Recurrent Neural Network Dependency Parser with Search-based Discriminative Training , CoNLL 2015
*/

#include "dinn.h"

void save_stata(APPROX_PARAMS *ap, char *s) {
    char prog_fname[MAX_NAME];
    strcpy(prog_fname, ap->model_name);
    strcat(prog_fname, ".prog");

    DEF_FOPEN(fp, prog_fname, "w");
    
    fprintf(fp, s);
    fclose(fp);

}

void remove_stata(APPROX_PARAMS *ap) {
    char prog_fname[MAX_NAME];
    strcpy(prog_fname, ap->model_name);
    strcat(prog_fname, ".prog");
    remove(prog_fname);
}

double testing(APPROX_PARAMS *p, MODEL_PARAMS *w) {

    int i;

    DEF_FOPEN(ofp, p->out_file, "w");

    SENTENCE *gld_sent;
    int matched = 0, all = 0;
    double err = 0.;
    int c = 0;



    int n=0;

    p->testing = 1;
    while(n< p->test_dataset_size){

    	gld_sent = &w->testset[n];
        n++;
        DEF_ALLOC(mod_sent, SENTENCE);
        memcpy(mod_sent, gld_sent, sizeof(SENTENCE));
        for (i = 0; i < gld_sent->len + 1; i++) {
            mod_sent->head[i] = -10;
            strcpy(mod_sent->s_deprel[i], "UNKNOWN-DEPREL");
            mod_sent->deprel[i] = -10;
        }

        err += parse_sentence(p, w, mod_sent);

        save_sentence(ofp, mod_sent, 1);

        all += mod_sent->len;
        matched += get_matched(mod_sent, gld_sent, 1);


        free(mod_sent);
        c++;
    }
    printf("done. Processed %d sentences\n", c);
    printf("Testing err: %f\n", err);
    printf("Testing accuracy: %f\n", matched / ( (double) all));
    //printf("meanB %e \n",p->AverageB/p->ShiftNum);
    return matched / ( (double) all);
}

double training(APPROX_PARAMS *p, MODEL_PARAMS *w) {

    SENTENCE *tr_sent;
    double train_err = 0.;
    double ctrain_err = 0.;
    double ctrain_num = 0.;
    double margin = 1;
    int c = 0;
    int words_read = 0;
    time_t time_state = time(NULL);

    p->testing = 0;

    ////////////////// only train actions

    while(c< p->train_dataset_size && p->WITHNEGPATH==0){

 	   		int i= rand()% p->train_dataset_size;

 	   		tr_sent = &w->trainset[i];

 	  		words_read += tr_sent->len;
 	   		STATE_LIST *der_first, *der_last;

 	   		get_seq_list(p, w, tr_sent, &der_first, &der_last, 1);
			double train_err_del = -comp_sentence(p, w, der_first);

 	        train_err += train_err_del;

 	       	comp_err(p, w, der_last,1);

 	       	free_derivation(der_last);

 	       	////////// TODO add minibatch

 	   		update_weights(p, w, p->WE_FIXED);
			c++;
 			if(c%1000 == 0){
 				printf("%e %d %d \n" , train_err/c, w->feature_found, w->feature_notfound);
 				w->feature_found=0;
 				w->feature_notfound=0;

 			}

    }
    train_err=0;
    c=0;

    while(c<p->train_dataset_size && p->WITHNEGPATH ==1){//

     	   		int i= rand()% p->train_dataset_size;

     	   		tr_sent = &w->trainset[i];


     	   		words_read += tr_sent->len;
     	   		STATE_LIST *der_first, *der_last;
     	   		STATE_LIST *der_first_neg, *der_last_neg;


     	   		get_seq_list(p, w, tr_sent, &der_first, &der_last, 1);
    			double train_err_del = -comp_sentence(p, w, der_first);


    			p->lambda = 0.1;
     	       	comp_err(p, w, der_last,1);

     	   		free_derivation(der_last);



     	   		/////////////////////////// Best neg path

     	   		STATE  *best_states[MAX_SENT_LEN + 1][MAX_BEAM + 1];

    			best_states[0][0] = root_prediction(p,w, tr_sent);

    			int cand_num = 1;
    			int B=p->beam;

    			p->beam = 2;
    			p->testing = 1;

    			int t;
    			for (t = 1; t < tr_sent->len + 1; t++) {
    				cand_num = Chunk_Beam_Search(p, w, tr_sent, best_states[t-1], cand_num, best_states[t] );
    				ASSERT(cand_num <= p->beam);
    			}

    			DEF_ALLOC(mod_sent, SENTENCE);
    			memcpy(mod_sent, tr_sent, sizeof(SENTENCE));
    			for (i = 0; i < tr_sent->len + 1; i++) {
    				mod_sent->head[i] = -10;
    				strcpy(mod_sent->s_deprel[i], "UNKNOWN-DEPREL");
    				mod_sent->deprel[i] = -10;
    			}
    			pt_fill_sentence(best_states[tr_sent->len][0]->pt, w, mod_sent);
    			double all = tr_sent->len;
    			double matched = get_matched(mod_sent, tr_sent, 1);
    			double perc = matched/all;
    			double fneg = -best_states[tr_sent->len][0]->lprob;

    			if(perc<1 && fneg-margin < train_err_del){// && fneg < train_err_del){
    				p->testing = 0;
    				der_last_neg = increase_list(NULL, NULL);

    				der_first_neg = der_last_neg;

    				STATE* a_state_t = get_seq_list_fromlast(p, w,  tr_sent,1, best_states[tr_sent->len][0]);//best_states[tr_sent->len][b];

    				while (a_state_t->previous_state != NULL) {

    							a_state_t = a_state_t->previous_state;

    				}
    				int l=0;
    				while (1) {

    					l++;
    					der_last_neg = increase_list(der_last_neg, a_state_t);

    					if (a_state_t->next_states->prev == NULL) {
    							break;
    						}
    					a_state_t = a_state_t->next_states->prev->elem;
    				}
    				double fneg = -comp_sentence(p, w, der_first_neg);




    				train_err += (1-perc);//-fneg;
    				ctrain_err += (1-perc);//-fneg;
    				ctrain_num +=1;
    				p->lambda = 0.1;//(1-perc);

    				comp_err(p, w, der_last_neg, -1);


    				free_derivation(der_last_neg);

    			}
    			else{
    				if(cand_num >1){
    					double fneg_second = -best_states[tr_sent->len][1]->lprob;
						if(fneg_second-margin <fneg){
							p->testing = 0;
							der_last_neg = increase_list(NULL, NULL);

							der_first_neg = der_last_neg;

							STATE* a_state_t = get_seq_list_fromlast(p, w,  tr_sent,1, best_states[tr_sent->len][1]);//best_states[tr_sent->len][b];

							while (a_state_t->previous_state != NULL) {

								a_state_t = a_state_t->previous_state;

							}
							int l=0;
							while (1) {

								l++;
								der_last_neg = increase_list(der_last_neg, a_state_t);

								if (a_state_t->next_states->prev == NULL) {
										break;
									}
								a_state_t = a_state_t->next_states->prev->elem;
							}
							double fneg = -comp_sentence(p, w, der_first_neg);




							train_err += (1-perc);//-fneg;
							ctrain_err += (1-perc);//-fneg;
							ctrain_num +=1;
							p->lambda = 0.1;//(1-perc);

							comp_err(p, w, der_last_neg, -1);
							free_derivation(der_last_neg);
						}

    				}
				}
    			p->beam = B;
    			update_weights(p, w, p->WE_FIXED);


    			int ii;

    			for (ii = 0; ii < cand_num; ii++) {
    				pt_free(&(best_states[t-1][ii]->pt));
    				free_states(best_states[t-1][ii]);
    			}

    			free(mod_sent);

     			c++;
     			if(c%1000 == 0){
     				printf("%e %e %d %d \n" , ctrain_num/1000, ctrain_err/1000, w->feature_found, w->feature_notfound);
     				w->feature_found=0;
     				w->feature_notfound=0;
     				ctrain_err = 0;
     				ctrain_num = 0;
     			}

        }

    time_t time_end = time(NULL);
    printf("Read %d words, %f s. per sentence.\n", words_read, ((double) (time_end - time_state)) / c);

    remove_stata(p);

    return train_err; 
}



void Write_features(APPROX_PARAMS *p, MODEL_PARAMS *w) {
    char buffer[MAX_LINE];

    SENTENCE *tr_sent;
    double train_err = 0.;


    int c = 0;
    int words_read = 0;



    int n=0;

    DEF_FOPEN(fp, "features", "w");

    p->fp = fp;
    for(n=0;n< p->train_dataset_size;n++){
 		   	n++;

 	   		tr_sent = &w->trainset[n];

 	   		sprintf(buffer, "Computing sentence %d (%d words processed), average err: /sent = %.4f, /word = %.4f\n", c, words_read, train_err / c, train_err / words_read);

 	   		save_stata(p, buffer);
 	   		words_read += tr_sent->len;
 	   		STATE_LIST *der_first, *der_last;
 	   	get_seq_list(p, w, tr_sent, &der_first, &der_last, 1);
		//double train_err_del = -comp_sentence(p, w, der_first);
 	   	free_derivation(der_last);

 	       //double train_err_del = -comp_sentence(p, w, der_first);

    }
    fclose(p->fp);


}

void validation_training(APPROX_PARAMS *ap, MODEL_PARAMS *mp, char* WEfile) {

    initialize_weights(ap, mp, WEfile,ap->WITHWE);

    char weights_fname[MAX_NAME];
    strcpy(weights_fname, ap->model_name);
    strcat(weights_fname, ".new.wgt");

    char weight_dels_fname[MAX_NAME];
    strcpy(weight_dels_fname, ap->model_name);
    strcat(weight_dels_fname, ".new.wgt.mom");

    
    char best_weights_fname[MAX_NAME];
    strcpy(best_weights_fname, ap->model_name);
    strcat(best_weights_fname, ".best.wgt");

    char best_weight_dels_fname[MAX_NAME];
    strcpy(best_weight_dels_fname, ap->model_name);
    strcat(best_weight_dels_fname, ".best.wgt.mom");


    
    char s[MAX_LINE *25] = "";
    if (load_state(ap)) {
        print_state(s, ap);
        printf("Resuming training with the following parameters:\n====\n%s====\n", s);
        if (strcmp(ap->st.where, PL_FINISHED) == 0) {
            printf("Error: Training had already been finished. WHERE in the state file was set to '%s'\n", PL_FINISHED);
            exit(EXIT_FAILURE);
        }
        if (strcmp(ap->st.where, PL_STARTED) != 0) {
            load_weights(ap, mp, weights_fname);
            load_weight_dels(ap, mp, weight_dels_fname);
        }
    } else {
        ap->st.curr_eta  = ap->init_eta;
        ap->st.curr_reg  = ap->init_reg;
        ap->st.last_tr_err = MAXDOUBLE;
        ap->st.last_score = -MAXDOUBLE;
        ap->st.max_score =  -MAXDOUBLE;
        ap->st.loops_num = 0;
        ap->st.num_sub_a_state = 0;
        strcpy(ap->st.where, PL_STARTED);
    }
    ap->st.decay = 1. - ap->st.curr_reg * ap->st.curr_eta;
    save_state(ap);
   
    ASSERT((ap->st.loops_num == 0)  == (strcmp(ap->st.where, PL_STARTED) == 0));
    
    double new_tr_err, new_score;
    while (ap->st.loops_num < ap->max_loops) {
        if (ap->st.loops_num > 0 && ap->st.loops_num % ap->loops_between_val == 0 && strcmp(ap->st.where, PL_TRAIN_LOOP_END) == 0) {
            new_score = testing(ap, mp);
            printf("[ValTrain] Validation acc :  %f\%%\n", new_score * 100.);
            if (new_score < ap->st.max_score) {
                ap->st.num_sub_a_state++;
            } else {
                printf("[ValTrain] Best accuracy improved! Saving best weights to %s.\n", best_weights_fname);
                save_weights(ap, mp, best_weights_fname);
                save_weight_dels(ap, mp, best_weight_dels_fname);
                ap->st.num_sub_a_state = 0;
                ap->st.max_score = new_score;
            }
            if (new_score < ap->st.last_score) {
                printf("[ValTrain] Accuracy decreased.\n");
                if (ap->st.num_sub_a_state >= ap->max_loops_wo_accur_impr) {
                    printf("[ValTrain] Too many steps without improvement. Stopping.\n");
                    break;
                } 
                if (ap->eta_red_rate * ap->st.curr_eta / ap->init_eta >= ap->max_eta_red) {
                    ap->st.curr_eta *= ap->eta_red_rate;
                    printf("[ValTrain] New eta = %e\n", ap->st.curr_eta);
                }
                if (ap->reg_red_rate * ap->st.curr_reg / ap->init_reg >= ap->max_reg_red) {
                    ap->st.curr_reg *= ap->reg_red_rate;
                    printf("[ValTrain] New reg = %e\n", ap->st.curr_reg);
                }
                ap->st.decay = 1. - ap->st.curr_reg * ap->st.curr_eta;
            }                
            ap->st.last_score = new_score;  
            strcpy(ap->st.where, PL_VAL_END);
            save_state(ap);         
        }
        ASSERT(strcmp(ap->st.where, PL_STARTED) || strcmp(ap->st.where, PL_VAL_END));
        new_tr_err = training(ap, mp);

        printf("[ValTrain] Training error: %f\n", new_tr_err);
        save_weights(ap, mp, weights_fname);
        save_weight_dels(ap, mp, weight_dels_fname);
        if (ap->st.last_tr_err < new_tr_err) {
            printf("[ValTrain] Error increase\n");
            if (ap->eta_red_rate * ap->st.curr_eta / ap->init_eta >= ap->max_eta_red) {
                ap->st.curr_eta *= ap->eta_red_rate;
                printf("[ValTrain] New eta = %e\n", ap->st.curr_eta);
            }
            ap->st.decay = 1. - ap->st.curr_reg * ap->st.curr_eta;
             
        }   
        ap->st.last_tr_err = new_tr_err; 
        strcpy(ap->st.where, PL_TRAIN_LOOP_END);
        ap->st.loops_num++;
        save_state(ap);
    } 
    printf("[ValTrain] Finished with the best acurracy: %f\%%\n", ap->st.max_score * 100);
    
    strcpy(ap->st.where, PL_FINISHED);
    save_state(ap);
    
}
