/*
    Majid Yazdani and James Henderson

	Incremental Recurrent Neural Network Dependency Parser with Search-based Discriminative Training , CoNLL 2015

*/

#include "dinn.h"


#define MAXWORDLENGTH 200

//exception handlers
char *field_missing_wrapper(char* s, char *name) {
    if (s == NULL) {
        fprintf(stderr, "Error: field %s is missing\n", name);
        exit(1);
    }
    return s;
}
char *field_missing_wrapper_ext(char* s, char *name, char *file_type) {
    if (s == NULL) {
        fprintf(stderr, "Error: field %s is missing in file '%s'\n", name, file_type);
        exit(1);
    }
    return s;
}
void err_chk(void *p, char* msg) {
    if (p == NULL) {
        fprintf(stderr, "Error: %s\n", msg);
        exit(1);
    }
}

#define RAND_WEIGHT ( (ap->rand_range)*2.0*drand48()-(ap->rand_range))
#define RAND_EMISS_WEIGHT ((ap->em_rand_range)*2.0*drand48()-(ap->em_rand_range))
//loads parameters 


void loadTrainSet(APPROX_PARAMS *p, MODEL_PARAMS *w){


   int c = 0;
   int words_read = 0;


   DEF_FOPEN(ifp, p->train_file, "r");
   read_sentence_train(w, ifp, 1,c);
   while (&w->trainset[c] !=NULL  && (c) < p->trainset_size) {

	    words_read += w->trainset[c].len;
	    c++;
	    read_sentence_train(w, ifp, 1,c);
	}
	printf("%d %d\n", words_read,c);
   fclose(ifp);

   c = 0;
   words_read = 0;
   DEF_FOPEN(fp, p->test_file, "r");
   read_sentence_test(w, fp, 1,c);
   while (&w->testset[c] !=NULL  && (c) < p->test_dataset_size) {

	    words_read += w->testset[c].len;
	    c++;
	    read_sentence_test(w, fp, 1,c);
	}
	printf("%d %d\n", words_read,c);
   fclose(fp);




}


void initialize_weights(APPROX_PARAMS *ap, MODEL_PARAMS *mp, char *WEfile, int WE) {

    //sample input links
	loadTrainSet(ap,mp);

    if(WE==1)
    	load_WE(ap, mp, WEfile);

    int i, h;
    DEF_FOPEN(ifp, "FreqFeatures", "r");
	int l;
	int ind = -1;
	int id = 1;
	while (!feof (ifp))
		{

		  fscanf (ifp, "%d", &l);
		  fscanf (ifp, "%d", &ind);
		  mp->chosenfeature[l][ind] = id;
		  id++;
		}
	mp->composed_feat_num = id-1;
	fclose(ifp);


  //printf("done\n");
   for (i = 0; i < mp->deprel_num + 2; i++) {
       for (h = 0; h < ap->emb_size; h++) {
           mp->deprel[i].b[h] = RAND_WEIGHT;

       }
   }
   //printf("dep done\n");

   for (i = 0; i < mp->pos_info_in.num + 1; i++) {
       for (h = 0; h < ap->emb_size; h++) {
           mp->pos[i].b[h] = RAND_WEIGHT;

       }
   }
   //printf("pos done\n");
   for (i = 0; i < mp->lemma_num + 1; i++) {
       for (h = 0; h < ap->emb_size; h++) {

           mp->lemma[i].b[h] = RAND_WEIGHT;


       }
   }
  //printf("lem done\n");
   for (i = 0; i < mp->word_num + 1; i++) {
	   if(WE==0 || i==mp->word_num){
		   for (h = 0; h < ap->emb_size; h++) {
		   			mp->word[i].b[h] = RAND_WEIGHT;



		   		}

	   }
	}
  // printf("word done\n");

   for (i = 0; i < mp->elfeat_num + 1; i++) {
 	   for (h = 0; h < ap->emb_size; h++) {
 		   mp->elfeat[i].b[h] = RAND_WEIGHT;

 	   }

 	}
  // printf("elf done\n");


  	//printf("f done\n");
//
   for (i = 0; i < mp->composed_feat_num + 1; i++) {
 	   for (h = 0; h < ap->hid_size; h++) {
 		   			mp->composed_feat[i].b[h] = RAND_WEIGHT;


 		   		}


 	}
   //printf("cf done\n");


    for (l = 0; l < mp->ih_links_num; l++) {

    	int h1, h2;

		for (h1 = 0; h1 < ap->hid_size; h1++) {
			for (h2 = 0; h2 < ap->emb_size; h2++) {
				mp->eh_link_ih[l].w[h1][h2] = RAND_WEIGHT;
			}
		}



    }
    //printf("ih done\n");


    for (h = 0; h < ap->hid_size; h++) {
        mp->init_bias.b[h] = RAND_WEIGHT;

        mp->hid_bias.b[h] = RAND_WEIGHT;

    }
    //printf("bias done\n");

    int a;
    for (a = 0; a < ACTION_NUM; a++) {
        for (h = 0; h < ap->hid_size; h++) {
            mp->prev_act_bias[a].b[h] = RAND_WEIGHT;

        } 
        int d;
        for (d = 0; d < mp->deprel_num; d++) {
            for (h = 0; h < ap->hid_size; h++) {
                mp->prev_deprel_bias[a][d].b[h] = RAND_WEIGHT;

            } 
        }
    }
    
    //printf("act done\n");
    //sample HID-HID weight matrixes
    int h1, h2;
    for (l = 0; l < mp->hh_links_num+1; l++) {
        for (h1 = 0; h1 < ap->hid_size; h1++) {
            for (h2 = 0; h2 < ap->hid_size; h2++) {

                mp->hh_link[l].w[h1][h2] =RAND_WEIGHT;// RAND_WEIGHT;

            }
        }
    }
    //printf("h h done\n");
        
    //sample output weights
	#define SAMPLE_OUT_LINK(out_link) \
	{\
		(out_link).b[t] = RAND_EMISS_WEIGHT;\
		for (i = 0; i < ap->hid_size; i++) {\
			(out_link).w[t][i] = RAND_EMISS_WEIGHT;\
		}\
	}
    
    
    int t;
    for (t = 0; t < MAX_OUT-1; t++) {
        SAMPLE_OUT_LINK(mp->out_link);
    }
    mp->out_link.b[MAX_OUT-1] = RAND_EMISS_WEIGHT;
    for (i = 0; i < ap->hid_size; i++) {\
    	mp->out_link.w[MAX_OUT-1][i] = RAND_EMISS_WEIGHT;

    }



    
}


void load_WE(APPROX_PARAMS *ap, MODEL_PARAMS *mp, char* fname){

	 FILE *fp;
	    if ((fp = fopen(fname, "r")) == NULL) {
	        fprintf(stderr, "Error: can't open file %s\n", fname);
	        exit(1);
	    }

	    char buffer[MAX_WGT_LINE];

	#define READ_IH_WEIGHTS(ih, name)\
	    {\
	        do { \
	            err_chk(fgets(buffer, MAX_WGT_LINE, fp), "Problem with reading weights"); \
	        } while (buffer[0] == '#'); \
	        (ih).b[0] = atof(field_missing_wrapper(strtok(buffer, FIELD_SEPS), name));\
	        for (h = 1; h < ap->emb_size; h++) { \
	            (ih).b[h] = atof(field_missing_wrapper(strtok(NULL, FIELD_SEPS), name)); \
	        }\
	    }

	    //read input links
	    int l;
	    int h;


	    for (l = 0; l < mp->word_num ; l++) {

	     	READ_IH_WEIGHTS(mp->word[l] , "ih_word");
	 	}

	    fclose(fp);
}
//loads parameters 
void load_weights(APPROX_PARAMS *ap, MODEL_PARAMS *mp, char* fname) {
	loadTrainSet(ap,mp);

    DEF_FOPEN(ifp, "FreqFeatures", "r");

    int li =-1;
    int ind = -1;
    int id =1;
    while (!feof (ifp))
        {

          fscanf (ifp, "%d", &li);
          fscanf (ifp, "%d", &ind);
          mp->chosenfeature[li][ind] = id;
          id++;
        }
    mp->composed_feat_num = id-1;
    fclose(ifp);

    FILE *fp;
    if ((fp = fopen(fname, "r")) == NULL) {
        fprintf(stderr, "Error: can't open file %s\n", fname);
        exit(1);
    }

    char buffer[MAX_WGT_LINE];

#define READ_IH_WEIGHTS(ih, name)\
    {\
        do { \
            err_chk(fgets(buffer, MAX_WGT_LINE, fp), "Problem with reading weights"); \
        } while (buffer[0] == '#'); \
        (ih).b[0] = atof(field_missing_wrapper(strtok(buffer, FIELD_SEPS), name));\
        for (h = 1; h < ap->hid_size; h++) { \
            (ih).b[h] = atof(field_missing_wrapper(strtok(NULL, FIELD_SEPS), name)); \
        }\
    }
#define READ_IE_WEIGHTS(ih, name)\
    {\
        do { \
            err_chk(fgets(buffer, MAX_WGT_LINE, fp), "Problem with reading weights"); \
        } while (buffer[0] == '#'); \
        (ih).b[0] = atof(field_missing_wrapper(strtok(buffer, FIELD_SEPS), name));\
        for (h = 1; h < ap->emb_size; h++) { \
            (ih).b[h] = atof(field_missing_wrapper(strtok(NULL, FIELD_SEPS), name)); \
        }\
    } 
    
    int h;
    int i;


	for (i = 0; i < mp->deprel_num + 2; i++) {
		   READ_IE_WEIGHTS(mp->deprel[i],"deprel");
	}
    for (i = 0; i < mp->pos_info_in.num + 1; i++) {
        READ_IE_WEIGHTS(mp->pos[i], "pos");
    }

    for (i = 0; i < mp->lemma_num + 1; i++) {
            READ_IE_WEIGHTS(mp->lemma[i], "lemma");
        }
	for (i = 0; i < mp->word_num + 1; i++) {
		 READ_IE_WEIGHTS(mp->word[i], "word");
	 }

	for (i = 0; i < mp->elfeat_num + 1; i++) {
		 READ_IE_WEIGHTS(mp->elfeat[i], "elfeats");
	 }


	for (i = 0; i < mp->composed_feat_num + 1; i++) {
		 READ_IH_WEIGHTS(mp->composed_feat[i], "composed");
	 }
    //read input links
    int l;

    int h1, h2;
    for (l = 0; l < mp->ih_links_num; l++) {
        for (h1 = 0; h1 < ap->hid_size; h1++) {
            do {
                err_chk(fgets(buffer, MAX_WGT_LINE, fp), "Problem with reading weights");
            } while (buffer[0] == '#');
            mp->eh_link_ih[l].w[h1][0] = atof(field_missing_wrapper(strtok(buffer, FIELD_SEPS), "eh_link_ih"));
            for (h2 = 1; h2 < ap->emb_size; h2++) {
                mp->eh_link_ih[l].w[h1][h2] = atof(field_missing_wrapper(strtok(NULL, FIELD_SEPS), "eh_link_ih"));
            }
        }
    }


/*
    for (l = 0; l < mp->ih_links_num; l++) {
        for (h1 = 0; h1 < ap->hid_size; h1++) {
            do {
                err_chk(fgets(buffer, MAX_WGT_LINE, fp), "Problem with reading weights");
            } while (buffer[0] == '#');
		    mp->hh_link_deprel[l].w[h1][0] = atof(field_missing_wrapper(strtok(buffer, FIELD_SEPS), "hh_link_deprel"));
			for (h2 = 1; h2 < ap->hid_size; h2++) {
				mp->hh_link_deprel[l].w[h1][h2] = atof(field_missing_wrapper(strtok(NULL, FIELD_SEPS), "hh_link_deprel"));
			}
        }
    }

    for (l = 0; l < mp->ih_links_num; l++) {
        for (h1 = 0; h1 < ap->hid_size; h1++) {
            do {
                err_chk(fgets(buffer, MAX_WGT_LINE, fp), "Problem with reading weights");
            } while (buffer[0] == '#');
		    mp->hh_link_lemmaonly[l].w[h1][0] = atof(field_missing_wrapper(strtok(buffer, FIELD_SEPS), "hh_link_lemmaonly"));
			for (h2 = 1; h2 < ap->hid_size; h2++) {
				mp->hh_link_lemmaonly[l].w[h1][h2] = atof(field_missing_wrapper(strtok(NULL, FIELD_SEPS), "hh_link_lemmapnly"));

			}
        }
    }

    for (l = 0; l < mp->ih_links_num; l++) {
        for (h1 = 0; h1 < ap->hid_size; h1++) {
            do {
                err_chk(fgets(buffer, MAX_WGT_LINE, fp), "Problem with reading weights");
            } while (buffer[0] == '#');
			mp->hh_link_wordonly[l].w[h1][0] = atof(field_missing_wrapper(strtok(buffer, FIELD_SEPS), "hh_link_wordonly"));
			for (h2 = 1; h2 < ap->hid_size; h2++) {
				mp->hh_link_wordonly[l].w[h1][h2] = atof(field_missing_wrapper(strtok(NULL, FIELD_SEPS), "hh_link_wordpnly"));

			}
        }
    }*/



    READ_IH_WEIGHTS(mp->init_bias, "init_bias");
    READ_IH_WEIGHTS(mp->hid_bias, "hid_bias");

    int a;
    for (a = 0; a < ACTION_NUM; a++) {
        READ_IH_WEIGHTS(mp->prev_act_bias[a], "prev_act_bias");
    }

    for (a = 0; a < ACTION_NUM; a++) {
        int d;
        for (d = 0; d < mp->deprel_num; d++) {
            READ_IH_WEIGHTS(mp->prev_deprel_bias[a][d], "prev_deprel_bias");
        }
    }        
 
    //load HID-HID weight matrixes

    for (l = 0; l < mp->hh_links_num+1; l++) {
        for (h1 = 0; h1 < ap->hid_size; h1++) {
            do { 
                err_chk(fgets(buffer, MAX_WGT_LINE, fp), "Problem with reading weights");
            } while (buffer[0] == '#');     
            mp->hh_link[l].w[h1][0] = atof(field_missing_wrapper(strtok(buffer, FIELD_SEPS), "hh_link"));
            for (h2 = 1; h2 < ap->hid_size; h2++) {
                mp->hh_link[l].w[h1][h2] = atof(field_missing_wrapper(strtok(NULL, FIELD_SEPS), "hh_link"));
            }
        }
    }
        
    //load output weights
#define READ_OUT_LINK(out_link, name)\
    {\
        do { \
            err_chk(fgets(buffer, MAX_WGT_LINE, fp), "Problem with reading weights"); \
        } while (buffer[0] == '#'); \
        (out_link).b[t] = atof(field_missing_wrapper(strtok(buffer, FIELD_SEPS), name));\
        for (i = 0; i < ap->hid_size; i++) { \
            (out_link).w[t][i] = atof(field_missing_wrapper(strtok(NULL, FIELD_SEPS), name)); \
        }\
    } 
     
    
    int t;
    for (t = 0; t < MAX_OUT; t++) {
        READ_OUT_LINK(mp->out_link, "out_link");
    }

/*    for (t = 0; t < mp->deprel_num; t++) {
        READ_OUT_LINK(mp->out_link_la_label, "out_link_la_label");
        READ_OUT_LINK(mp->out_link_ra_label, "out_link_ra_label");
    }*/

    
    
    fclose(fp);

}
//loads weight deltas 
void load_weight_dels(APPROX_PARAMS *ap, MODEL_PARAMS *mp, char* fname) {

    FILE *fp;
    if ((fp = fopen(fname, "r")) == NULL) {
        fprintf(stderr, "Error: can't open file %s\n", fname);
        exit(1);
    }

    char buffer[MAX_WGT_LINE];

#define READ_IE_WEIGHT_DELS(ih, name)\
    {\
        do { \
            err_chk(fgets(buffer, MAX_WGT_LINE, fp), "Problem with reading weights"); \
        } while (buffer[0] == '#'); \
        (ih).i_b_del[0] = atof(field_missing_wrapper(strtok(buffer, FIELD_SEPS), name));\
        for (h = 1; h < ap->emb_size; h++) { \
            (ih).i_b_del[h] = atof(field_missing_wrapper(strtok(NULL, FIELD_SEPS), name)); \
        }\
    }
#define READ_IH_WEIGHT_DELS(ih, name)\
    {\
        do { \
            err_chk(fgets(buffer, MAX_WGT_LINE, fp), "Problem with reading weights"); \
        } while (buffer[0] == '#'); \
        (ih).i_b_del[0] = atof(field_missing_wrapper(strtok(buffer, FIELD_SEPS), name));\
        for (h = 1; h < ap->hid_size; h++) { \
            (ih).i_b_del[h] = atof(field_missing_wrapper(strtok(NULL, FIELD_SEPS), name)); \
            }\
    } 
    int i;
    int h;



    for (i = 0; i < mp->deprel_num + 2; i++) {
		   READ_IE_WEIGHT_DELS(mp->deprel[i],"deprel");
	}

    for (i = 0; i < mp->pos_info_in.num + 1; i++) {
		READ_IE_WEIGHT_DELS(mp->pos[i], "pos");
	}

    for (i = 0; i < mp->lemma_num + 1; i++) {
            READ_IE_WEIGHT_DELS(mp->lemma[i], "lemma");
        }

    for (i = 0; i < mp->word_num + 1; i++) {
		 READ_IE_WEIGHT_DELS(mp->word[i], "word");
	 }

	for (i = 0; i < mp->elfeat_num + 1; i++) {
		 READ_IE_WEIGHT_DELS(mp->elfeat[i], "elfeats");
	 }




    for (i = 0; i < mp->composed_feat_num + 1; i++) {
		 READ_IH_WEIGHT_DELS(mp->composed_feat[i], "composed");
	 }
    //read input links
    int l;
        
    int h1, h2;
       for (l = 0; l < mp->ih_links_num; l++) {
           for (h1 = 0; h1 < ap->hid_size; h1++) {
               do {
                   err_chk(fgets(buffer, MAX_WGT_LINE, fp), "Problem with reading weights");
               } while (buffer[0] == '#');
               mp->eh_link_ih[l].i_w_del[h1][0] = atof(field_missing_wrapper(strtok(buffer, FIELD_SEPS), "eh_link_ih"));

               for (h2 = 1; h2 < ap->emb_size; h2++) {
                   mp->eh_link_ih[l].i_w_del[h1][h2] = atof(field_missing_wrapper(strtok(NULL, FIELD_SEPS), "eh_link_ih"));

               }
           }
       }



   /*    for (l = 0; l < mp->ih_links_num; l++) {
           for (h1 = 0; h1 < ap->hid_size; h1++) {
               do {
                   err_chk(fgets(buffer, MAX_WGT_LINE, fp), "Problem with reading weights");
               } while (buffer[0] == '#');
   		    mp->hh_link_deprel[l].w_del[h1][0] = atof(field_missing_wrapper(strtok(buffer, FIELD_SEPS), "hh_link_deprel"));
   			for (h2 = 1; h2 < ap->hid_size; h2++) {
   				mp->hh_link_deprel[l].w_del[h1][h2] = atof(field_missing_wrapper(strtok(NULL, FIELD_SEPS), "hh_link_deprel"));
   			}
           }
       }

       for (l = 0; l < mp->ih_links_num; l++) {
           for (h1 = 0; h1 < ap->hid_size; h1++) {
               do {
                   err_chk(fgets(buffer, MAX_WGT_LINE, fp), "Problem with reading weights");
               } while (buffer[0] == '#');
   		    mp->hh_link_lemmaonly[l].w_del[h1][0] = atof(field_missing_wrapper(strtok(buffer, FIELD_SEPS), "hh_link_lemmaonly"));
   			for (h2 = 1; h2 < ap->hid_size; h2++) {
   				mp->hh_link_lemmaonly[l].w_del[h1][h2] = atof(field_missing_wrapper(strtok(NULL, FIELD_SEPS), "hh_link_lemmapnly"));

   			}
           }
       }

       for (l = 0; l < mp->ih_links_num; l++) {
           for (h1 = 0; h1 < ap->hid_size; h1++) {
               do {
                   err_chk(fgets(buffer, MAX_WGT_LINE, fp), "Problem with reading weights");
               } while (buffer[0] == '#');
   			mp->hh_link_wordonly[l].w_del[h1][0] = atof(field_missing_wrapper(strtok(buffer, FIELD_SEPS), "hh_link_wordonly"));
   			for (h2 = 1; h2 < ap->hid_size; h2++) {
   				mp->hh_link_wordonly[l].w_del[h1][h2] = atof(field_missing_wrapper(strtok(NULL, FIELD_SEPS), "hh_link_wordpnly"));

   			}
           }
       }*/


    READ_IH_WEIGHT_DELS(mp->init_bias, "init_bias");
    READ_IH_WEIGHT_DELS(mp->hid_bias, "hid_bias");

    int a;
    for (a = 0; a < ACTION_NUM; a++) {
        READ_IH_WEIGHT_DELS(mp->prev_act_bias[a], "prev_act_bias");
    }

    for (a = 0; a < ACTION_NUM; a++) {
        int d;
        for (d = 0; d < mp->deprel_num; d++) {
            READ_IH_WEIGHT_DELS(mp->prev_deprel_bias[a][d], "prev_deprel_bias");
        }
    }        
 
    //load HID-HID weight matrixes

    for (l = 0; l < mp->hh_links_num+1; l++) {
        for (h1 = 0; h1 < ap->hid_size; h1++) {
            do { 
                err_chk(fgets(buffer, MAX_WGT_LINE, fp), "Problem with reading weights");
            } while (buffer[0] == '#');     
            mp->hh_link[l].i_w_del[h1][0] = atof(field_missing_wrapper(strtok(buffer, FIELD_SEPS), "hh_link"));

            for (h2 = 1; h2 < ap->hid_size; h2++) {
                mp->hh_link[l].i_w_del[h1][h2] = atof(field_missing_wrapper(strtok(NULL, FIELD_SEPS), "hh_link"));

            }
        }
    }
        
    //load output weights
#define RED_OUT_LINK_DEL(out_link, name)\
    {\
        do { \
            err_chk(fgets(buffer, MAX_WGT_LINE, fp), "Problem with reading weights"); \
        } while (buffer[0] == '#'); \
        (out_link).i_b_del[t] = atof(field_missing_wrapper(strtok(buffer, FIELD_SEPS), name));\
        for (i = 0; i < ap->hid_size; i++) { \
            (out_link).i_w_del[t][i] = atof(field_missing_wrapper(strtok(NULL, FIELD_SEPS), name)); \
          }\
    } 
     
    
    int t;
    for (t = 0; t < MAX_OUT; t++) {
        RED_OUT_LINK_DEL(mp->out_link, "out_link");
    }

/*    for (t = 0; t < mp->deprel_num; t++) {
        RED_OUT_LINK_DEL(mp->out_link_la_label, "out_link_la_label");
        RED_OUT_LINK_DEL(mp->out_link_ra_label, "out_link_ra_label");
    }*/


    
    fclose(fp);

}
void save_weights(APPROX_PARAMS *ap, MODEL_PARAMS *mp, char* fname) {

    FILE *fp;
    if ((fp = fopen(fname, "w")) == NULL) {
        fprintf(stderr, "Error: can't open file %s\n", fname);
        exit(1);
    }

    
#define SAVE_IE_WEIGHTS(ih)\
    {\
        fprintf(fp, "%.20f", (ih).b[0]); \
        for (h = 1; h < ap->emb_size; h++) { \
             fprintf(fp, " %.20f", (ih).b[h]); \
        }\
        fprintf(fp, "\n");\
    }
#define SAVE_IH_WEIGHTS(ih)\
    {\
        fprintf(fp, "%.20f", (ih).b[0]); \
        for (h = 1; h < ap->hid_size; h++) { \
             fprintf(fp, " %.20f", (ih).b[h]); \
        }\
        fprintf(fp, "\n");\
    } 
    
    //save input links
      int l;

      int i;
      int h;


      for (i = 0; i < mp->deprel_num + 2; i++) {
                SAVE_IE_WEIGHTS(mp->deprel[i]);
         }


      for (i = 0; i < mp->pos_info_in.num + 1; i++) {
                      SAVE_IE_WEIGHTS(mp->pos[i]);
                  }

      for (i = 0; i < mp->lemma_num + 1; i++) {
          SAVE_IE_WEIGHTS(mp->lemma[i]);
      }
      for (i = 0; i < mp->word_num + 1; i++) {
          SAVE_IE_WEIGHTS(mp->word[i]);
      }

  	for (i = 0; i < mp->elfeat_num + 1; i++) {
  		 SAVE_IE_WEIGHTS(mp->elfeat[i]);
  	 }


      for (i = 0; i < mp->composed_feat_num + 1; i++) {
          SAVE_IH_WEIGHTS(mp->composed_feat[i]);
      }
      int h1, h2;
      fprintf(fp, "#======================\n#ih_links\n#======================\n");
      for (l = 0; l < mp->ih_links_num; l++) {
          fprintf(fp, "#Link  %d (HH)\n", l);
          for (h1 = 0; h1 < ap->hid_size; h1++) {
              fprintf(fp, "%.20f", mp->eh_link_ih[l].w[h1][0]);
              for (h2 = 1; h2 < ap->emb_size; h2++) {
                  fprintf(fp, " %.20f", mp->eh_link_ih[l].w[h1][h2]);
              }
              fprintf(fp, "\n");
          }
      }





    fprintf(fp, "#======================\n#init_bias\n");
    SAVE_IH_WEIGHTS(mp->init_bias);
    fprintf(fp, "#======================\n#hid_bias\n");
    SAVE_IH_WEIGHTS(mp->hid_bias);

    int a;
    fprintf(fp, "#======================\n#prev_act_bias\n");
    for (a = 0; a < ACTION_NUM; a++) {
        SAVE_IH_WEIGHTS(mp->prev_act_bias[a]);
    }

    fprintf(fp, "#======================\n#prev_deprel_bias\n");
    for (a = 0; a < ACTION_NUM; a++) {
        int d;
        for (d = 0; d < mp->deprel_num; d++) {
            SAVE_IH_WEIGHTS(mp->prev_deprel_bias[a][d]);
        }
    }        
    

    //save HID-HID weight matrixes

    fprintf(fp, "#======================\n#hh_links\n#======================\n");
    for (l = 0; l < mp->hh_links_num+1; l++) {
        fprintf(fp, "#Link  %d (HH)\n", l);
        for (h1 = 0; h1 < ap->hid_size; h1++) {
            fprintf(fp, "%.20f", mp->hh_link[l].w[h1][0]); 
            for (h2 = 1; h2 < ap->hid_size; h2++) {
                fprintf(fp, " %.20f", mp->hh_link[l].w[h1][h2]);
            }
            fprintf(fp, "\n");
        }
    }
        
    
    //save output weights
#define SAVE_OUT_LINK(out_link)\
    {\
        fprintf(fp, "%.20f", (out_link).b[t]);\
        for (i = 0; i < ap->hid_size; i++) { \
            fprintf(fp, " %.20f", (out_link).w[t][i]);\
        }\
        fprintf(fp, "\n");\
    } 
    fprintf(fp, "#======================\n#out_links\n#======================\n"); 
    
    int t;
    fprintf(fp,"#out_link_act\n");
    for (t = 0; t < MAX_OUT; t++) {
        SAVE_OUT_LINK(mp->out_link);
    }

    
    
    fclose(fp);
}
//saves parameters 
void save_weight_dels(APPROX_PARAMS *ap, MODEL_PARAMS *mp, char* fname) {

    FILE *fp;
    if ((fp = fopen(fname, "w")) == NULL) {
        fprintf(stderr, "Error: can't open file %s\n", fname);
        exit(1);
    }

    
#define SAVE_IE_WEIGHT_DELS(ih)\
    {\
        fprintf(fp, "%.20f", (ih).i_b_del[0]); \
        for (h = 1; h < ap->emb_size; h++) { \
             fprintf(fp, " %.20f", (ih).i_b_del[h]); \
        }\
        fprintf(fp, "\n");\
    }
#define SAVE_IH_WEIGHT_DELS(ih)\
    {\
        fprintf(fp, "%.20f", (ih).i_b_del[0]); \
        for (h = 1; h < ap->hid_size; h++) { \
             fprintf(fp, " %.20f", (ih).i_b_del[h]); \
    	}\
        fprintf(fp, "\n");\
    }
    
    //save input links
    int l;

    int i;
    int h;


    fprintf(fp, "#Link  DEPREL_TYPE\n");
	for (i = 0; i < mp->deprel_num + 2; i++) {
		   SAVE_IE_WEIGHT_DELS(mp->deprel[i]);
	}
    fprintf(fp, "#Link  POS_TYPE\n");
	for (i = 0; i < mp->pos_info_in.num + 1; i++) {
		SAVE_IE_WEIGHT_DELS(mp->pos[i]);
	}
    fprintf(fp, "#Link  LEMMAONLY_TYPE\n");
	for (i = 0; i < mp->lemma_num + 1; i++) {
		SAVE_IE_WEIGHT_DELS(mp->lemma[i]);
	}
	fprintf(fp, "#Link  WORDONLY_TYPE\n");
	 for (i = 0; i < mp->word_num + 1; i++) {
		 SAVE_IE_WEIGHT_DELS(mp->word[i]);
	 }


	for (i = 0; i < mp->elfeat_num + 1; i++) {
		 SAVE_IE_WEIGHT_DELS(mp->elfeat[i]);
	 }

	fprintf(fp, "#Link  FEATURE_TYPE\n");
	 for (i = 0; i < mp->composed_feat_num + 1; i++) {
		 SAVE_IH_WEIGHT_DELS(mp->composed_feat[i]);
	 }


     int h1, h2;
     fprintf(fp, "#======================\n#ih_links\n#======================\n");
     for (l = 0; l < mp->ih_links_num; l++) {
         fprintf(fp, "#Link  %d (HH)\n", l);
         for (h1 = 0; h1 < ap->hid_size; h1++) {
             fprintf(fp, "%.20f", mp->eh_link_ih[l].i_w_del[h1][0]);

             for (h2 = 1; h2 < ap->emb_size; h2++) {
                 fprintf(fp, " %.20f", mp->eh_link_ih[l].i_w_del[h1][h2]);
             }
             fprintf(fp, "\n");
         }
     }






    fprintf(fp, "#======================\n#init_bias\n");
    SAVE_IH_WEIGHT_DELS(mp->init_bias);
    fprintf(fp, "#======================\n#hid_bias\n");
    SAVE_IH_WEIGHT_DELS(mp->hid_bias);

    int a;
    fprintf(fp, "#======================\n#prev_act_bias\n");
    for (a = 0; a < ACTION_NUM; a++) {
        SAVE_IH_WEIGHT_DELS(mp->prev_act_bias[a]);
    }

    fprintf(fp, "#======================\n#prev_deprel_bias\n");
    for (a = 0; a < ACTION_NUM; a++) {
        int d;
        for (d = 0; d < mp->deprel_num; d++) {
            SAVE_IH_WEIGHT_DELS(mp->prev_deprel_bias[a][d]);
        }
    }        
    
    //save HID-HID weight matrixes

    fprintf(fp, "#======================\n#hh_links\n#======================\n");
    for (l = 0; l < mp->hh_links_num+1; l++) {
        fprintf(fp, "#Link  %d (HH)\n", l);
        for (h1 = 0; h1 < ap->hid_size; h1++) {
            fprintf(fp, "%.20f", mp->hh_link[l].i_w_del[h1][0]);
            for (h2 = 1; h2 < ap->hid_size; h2++) {
                fprintf(fp, " %.20f", mp->hh_link[l].i_w_del[h1][h2]);
            }
            fprintf(fp, "\n");
        }
    }
        
    
    //save output weights
#define SAVE_OUT_LINK_DEL(out_link)\
    {\
        fprintf(fp, "%.20f", (out_link).i_b_del[t]);\
        for (i = 0; i < ap->hid_size; i++) { \
            fprintf(fp, " %.20f", (out_link).i_w_del[t][i]);\
        }\
        fprintf(fp, "\n");\
    } 
    fprintf(fp, "#======================\n#out_links\n#======================\n"); 
    
    int t;
    fprintf(fp,"#out_link_act\n");
    for (t = 0; t < MAX_OUT; t++) {
        SAVE_OUT_LINK_DEL(mp->out_link);
    }

    
    

    fclose(fp);
}

char *print_sent(SENTENCE *sent, int with_links) {
    ASSERT(sent->len >= 0 && sent->len <= MAX_SENT_LEN);
    DEF_ALLOC_ARRAY(s, char, sent->len * MAX_LINE + 1); 
    s[0] = 0;
    char buf[MAX_LINE];
    int i;
    
    for (i = 1; i <= sent->len; i++) {
        /*
    	sprintf(buf, "%d\t%s\t%d\t%d\t%d\t%s\t%d\t%s\t%d\t%s\t%d\t%s\t%d\t%d\t",
                i, 
                sent->s_word[i], sent->word_in[i], sent->word_out[i],sent->word[i],
                sent->s_lemma[i], sent->lemma[i],
                sent->s_cpos[i], sent->cpos[i],
                sent->s_pos[i], sent->pos[i],
                sent->s_feat[i], sent->feat_in[i], sent->feat_out[i]);
                */
    /*	sprintf(buf, "%d\t%d\t%d\t%d\t%d\t%d\t",
    	                i,
    	                sent->word[i], //sent->word_in[i], sent->word_out[i],sent->word[i],
    	                sent->lemma[i],// sent->lemma[i],
    	                sent->cpos[i],// sent->cpos[i],
    	                sent->pos[i],// sent->pos[i],
    	                sent->elfeat[i]); //sent->feat_in[i], sent->feat_out[i]);

        strcat(s, buf);*/

    	sprintf(buf, "%d\t%s\t%s\t%s\t%s\t%s\t",
    	                i,
    	                sent->s_word[i], //sent->word_in[i], sent->word_out[i],sent->word[i],
    	                sent->s_lemma[i],// sent->lemma[i],
    	                sent->s_cpos[i],// sent->cpos[i],
    	                sent->s_pos[i],// sent->pos[i],
    	                sent->s_feat[i]); //sent->feat_in[i], sent->feat_out[i]);
    	strcat(s, buf);
        /*
        if (sent->elfeat_len[i] == 0) {
            strcat(s, "_");
        } else {
            int f;
            for (f = 0; f < sent->elfeat_len[i]; f++) {
                if (f != 0) {
                    strcat(s, "|");
                }
                sprintf(buf, "%d", sent->elfeat[i][f]);
                strcat(s, buf);  
            }
        }

        if (with_links) {
            sprintf(buf, "\th:%d\t%s\t%d",
                    sent->head[i], 
                    sent->s_deprel[i], sent->deprel[i]);
            strcat(s, buf);
       }*/
        if (with_links) {
                    sprintf(buf, "%d\t%s\t%d\t%s",
                            sent->head[i],
                            sent->s_deprel[i], sent->head[i],
                            sent->s_deprel[i]);
                    strcat(s, buf);
                }
      strcat(s,"\n");
    } 
    return s;
}

void append_sentence(char *fname, SENTENCE *sen, int with_links) {
    FILE *fp;
    if ((fp = fopen(fname, "w+")) == NULL) {
        fprintf(stderr, "Error: can't open file %s\n", fname);
        exit(1);
    }
    char *s = print_sent(sen, with_links);
    fprintf(fp, "%s\n", s);
    free(s);
}

void save_sentence(FILE *fp, SENTENCE *sen, int with_links) {
    char *s = print_sent(sen, with_links);
    fprintf(fp, "%s\n", s);
    free(s);
}

SENTENCE *create_blind_sent(SENTENCE *gld_sent) {
    DEF_ALLOC(blind_sent, SENTENCE);
    memcpy(blind_sent, gld_sent, sizeof(SENTENCE));
    int i;
    for (i = 1; i <= blind_sent->len; i++) {
        blind_sent->head[i] = -1;
        strcpy(blind_sent->s_deprel[i], "");
        blind_sent->deprel[i] = ROOT_DEPREL;
    }

    return blind_sent;
}


void fill_decomp_feats(MODEL_PARAMS *mp, SENTENCE *sen, int t, char *decomp_feats) {
    sen->elfeat_len[t] = 0;
    if (strcmp(decomp_feats, "_") == 0) {
        return;
    }

    char *s = strtok(decomp_feats, "|");
    if (s == NULL) {
        return;
    } else {
        sen->elfeat[t][sen->elfeat_len[t]++] = atoi(s);
    }
    while ((s = strtok(NULL, "|")) != NULL) {
        if (sen->elfeat_len[t] >= MAX_SIMULT_FEAT) {
            fprintf(stderr, "Error: number of features for sentence %d is larger than MAX_SIMULT_FEAT (%d)\n", t, MAX_SIMULT_FEAT);
            exit(1);
        }
        sen->elfeat[t][sen->elfeat_len[t]++] = atoi(s);
        if (sen->elfeat[t][sen->elfeat_len[t]-1] >= mp->elfeat_num) {
            fprintf(stderr, "Error: wrong feature id %d for sentence %d, it should not exceed %d\n", sen->elfeat[t][sen->elfeat_len[t]-1], t, mp->elfeat_num - 1);
        }
    }
}



// for use with WORDSONLYINPUT=1
#define  features_in(s)  0
#define  features_out(s)  0
#define  features(s)  ""


SENTENCE *read_sentence(MODEL_PARAMS *mp, FILE *fp, int with_links) {
    DEF_ALLOC(sen, SENTENCE);

    char buffer[MAX_LINE];

    int t = 1;
    int read_smth = 0;
    while (fgets(buffer, MAX_LINE, fp) != NULL) {
        if (buffer[0] == '#') {
            read_smth = 1;
            continue;
        }
        char *s = strtok(buffer, FIELD_SEPS);
        if (s == NULL) {
            break;
        }
        strncpy(sen->s_word[t], strtok(NULL, FIELD_SEPS), MAX_REC_LEN);
        sen->word_in[t] = atoi(strtok(NULL, FIELD_SEPS));
        sen->word_out[t] = atoi(strtok(NULL, FIELD_SEPS));

        strncpy(sen->s_lemma[t], strtok(NULL, FIELD_SEPS), MAX_REC_LEN);
        sen->lemma[t] = atoi(strtok(NULL, FIELD_SEPS));

        strncpy(sen->s_cpos[t], strtok(NULL, FIELD_SEPS), MAX_REC_LEN);
        sen->cpos[t] = atoi(strtok(NULL, FIELD_SEPS));

        strncpy(sen->s_pos[t], strtok(NULL, FIELD_SEPS), MAX_REC_LEN);
        sen->pos[t] = atoi(strtok(NULL, FIELD_SEPS));

        strncpy(sen->s_feat[t], strtok(NULL, FIELD_SEPS), MAX_REC_LEN);
        sen->feat_in[t] = atoi(strtok(NULL, FIELD_SEPS));
        sen->feat_out[t] = atoi(strtok(NULL, FIELD_SEPS));

        char *decomp_feats = strtok(NULL, FIELD_SEPS);

        #define CHECK_RANGE(x, size, name) if ((x) >= (size)) { fprintf(stderr, "Error: wrong %s: %d, exceeds allowed values of %d\n", name, (x), (size - 1)); exit(1);}
        CHECK_RANGE(sen->pos[t], mp->pos_info_in.num, "POS");
        CHECK_RANGE(sen->cpos[t], mp->cpos_num, "CPOS");
        CHECK_RANGE(sen->lemma[t], mp->lemma_num, "LEMMA");
        CHECK_RANGE(sen->feat_in[t], mp->pos_info_in.feat_infos[sen->pos[t]].num, "FEAT_IN");
        CHECK_RANGE(sen->feat_out[t], mp->pos_info_out.feat_infos[sen->pos[t]].num, "FEAT_OUT");
        if (sen->word_in[t] >= mp->pos_info_in.feat_infos[sen->pos[t]].word_infos[sen->feat_in[t]].num) {
            printf(".");
        }
        CHECK_RANGE(sen->word_in[t],  mp->pos_info_in.feat_infos[sen->pos[t]].word_infos[sen->feat_in[t]].num, "WORD_IN");

        CHECK_RANGE(sen->word_out[t], mp->pos_info_out.feat_infos[sen->pos[t]].word_infos[sen->feat_out[t]].num, "WORD_OUT");


        if (with_links) {
            sen->head[t] = atoi(strtok(NULL, FIELD_SEPS));
            char *s_deprel = strtok(NULL, FIELD_SEPS);
            strncpy(sen->s_deprel[t], s_deprel, MAX_REC_LEN);
            sen->deprel[t] = atoi(strtok(NULL, FIELD_SEPS));

            CHECK_RANGE(sen->deprel[t], mp->deprel_num, "DEPREL");
        }

        fill_decomp_feats(mp, sen, t, decomp_feats);

        t++;
    }


    //starting symbols
    strcpy(sen->s_word[0], "^^^");
    sen->word_in[0] = 0;
    sen->word_out[0] = 0;

    strcpy(sen->s_lemma[0], "^^^");
    sen->lemma[0] = mp->lemma_num;

    strcpy(sen->s_cpos[0], "^^^");
    sen->cpos[0] = mp->cpos_num;

    strcpy(sen->s_pos[0], "^^^");
    sen->pos[0] = mp->pos_info_in.num;
    ASSERT(mp->pos_info_in.num == mp->pos_info_out.num);

    strcpy(sen->s_feat[0], "^^^");
    sen->feat_in[0] =  0;
    sen->feat_out[0] = 0;
    sen->elfeat_len[0] = 1;
    sen->elfeat[0][0] = mp->elfeat_num;

    if (with_links) {
        sen->head[0] = 0;
        strcpy(sen->s_deprel[0], "^^^");
        sen->deprel[0] = mp->deprel_num;
    }

    //ending symbol
    strcpy(sen->s_word[t], "$$$");
    sen->word_in[t] = 0;
    sen->word_out[t] = 0;

    strcpy(sen->s_lemma[t], "$$$");
    sen->lemma[t] = mp->lemma_num;

    strcpy(sen->s_cpos[t], "$$$");
    sen->cpos[t] = mp->cpos_num;

    strcpy(sen->s_pos[t], "$$$");
    sen->pos[t] = mp->pos_info_in.num;

    strcpy(sen->s_feat[t], "$$$");
    sen->feat_in[t] =  0;
    sen->feat_out[t] = 0;
    sen->elfeat_len[t] = 1;
    sen->elfeat[t][0] = mp->elfeat_num;

    if (with_links) {
        sen->head[t] = 0;
        strcpy(sen->s_deprel[t], "$$$");
        sen->deprel[t] = mp->deprel_num;
    }

    sen->len = t - 1;
    if (sen->len == 0 && !read_smth) {
        free(sen);
        return NULL;
    }  else {
        return sen;
    }
}


void read_sentence_train(MODEL_PARAMS *mp, FILE *fp, int with_links, int c) {


    char buffer[MAX_LINE];
    
    int t = 1;
    int read_smth = 0;
    while (fgets(buffer, MAX_LINE, fp) != NULL) {
        if (buffer[0] == '#') {
            read_smth = 1;
            continue;
        }
        char *s = strtok(buffer, FIELD_SEPS);
        if (s == NULL) {
            break;
        }


        strncpy(mp->trainset[c].s_word[t], strtok(NULL, FIELD_SEPS), MAX_REC_LEN);

        mp->trainset[c].word_in[t] = atoi(strtok(NULL, FIELD_SEPS));

        mp->trainset[c].word_out[t] = atoi(strtok(NULL, FIELD_SEPS));

        mp->trainset[c].word[t] = atoi(strtok(NULL, FIELD_SEPS));

        strncpy(mp->trainset[c].s_lemma[t], strtok(NULL, FIELD_SEPS), MAX_REC_LEN);

        mp->trainset[c].lemma[t] = atoi(strtok(NULL, FIELD_SEPS));

        strncpy(mp->trainset[c].s_cpos[t], strtok(NULL, FIELD_SEPS), MAX_REC_LEN);

        mp->trainset[c].cpos[t] = atoi(strtok(NULL, FIELD_SEPS));
        strncpy(mp->trainset[c].s_pos[t], strtok(NULL, FIELD_SEPS), MAX_REC_LEN);

        mp->trainset[c].pos[t] = atoi(strtok(NULL, FIELD_SEPS));

        strncpy(mp->trainset[c].s_feat[t], strtok(NULL, FIELD_SEPS), MAX_REC_LEN);

        mp->trainset[c].feat_in[t] = atoi(strtok(NULL, FIELD_SEPS));

        mp->trainset[c].feat_out[t] = atoi(strtok(NULL, FIELD_SEPS));

        
    	char *decomp_feats = strtok(NULL, FIELD_SEPS);
		#define CHECK_RANGE(x, size, name) if ((x) >= (size)) { fprintf(stderr, "Error: wrong %s: %d, exceeds allowed values of %d\n", name, (x), (size - 1)); exit(1);}
		CHECK_RANGE(mp->trainset[c].pos[t], mp->pos_info_in.num, "POS");
		CHECK_RANGE(mp->trainset[c].cpos[t], mp->cpos_num, "CPOS");
		CHECK_RANGE(mp->trainset[c].lemma[t], mp->lemma_num, "LEMMA");
		CHECK_RANGE(mp->trainset[c].feat_in[t], mp->pos_info_in.feat_infos[mp->trainset[c].pos[t]].num, "FEAT_IN");
		CHECK_RANGE(mp->trainset[c].feat_out[t], mp->pos_info_out.feat_infos[mp->trainset[c].pos[t]].num, "FEAT_OUT");

		CHECK_RANGE(mp->trainset[c].word_in[t],  mp->pos_info_in.feat_infos[mp->trainset[c].pos[t]].word_infos[mp->trainset[c].feat_in[t]].num, "WORD_IN");

		CHECK_RANGE(mp->trainset[c].word_out[t], mp->pos_info_out.feat_infos[mp->trainset[c].pos[t]].word_infos[mp->trainset[c].feat_out[t]].num, "WORD_OUT");
        if (with_links) {
        	mp->trainset[c].head[t] = atoi(strtok(NULL, FIELD_SEPS));

            char *s_deprel = strtok(NULL, FIELD_SEPS);
            strncpy(mp->trainset[c].s_deprel[t], s_deprel, MAX_REC_LEN);

            mp->trainset[c].deprel[t] = atoi(strtok(NULL, FIELD_SEPS));


            CHECK_RANGE(mp->trainset[c].deprel[t], mp->deprel_num, "DEPREL");
        }

        fill_decomp_feats(mp, &mp->trainset[c], t, decomp_feats);
        
        t++;
    }


    //starting symbols
    strcpy(mp->trainset[c].s_word[0], "^^^");
    mp->trainset[c].word_in[0] = 0;
    mp->trainset[c].word_out[0] = 0;
    
    strcpy(mp->trainset[c].s_lemma[0], "^^^");
    mp->trainset[c].lemma[0] = mp->lemma_num;
    
    strcpy(mp->trainset[c].s_cpos[0], "^^^");
    mp->trainset[c].cpos[0] = mp->cpos_num;
    
    strcpy(mp->trainset[c].s_pos[0], "^^^");
    mp->trainset[c].pos[0] = mp->pos_info_in.num;
    ASSERT(mp->pos_info_in.num == mp->pos_info_out.num);
    
    strcpy(mp->trainset[c].s_feat[0], "^^^");
    mp->trainset[c].feat_in[0] =  0;
    mp->trainset[c].feat_out[0] = 0;
    mp->trainset[c].elfeat_len[0] = 1;
    mp->trainset[c].elfeat[0][0] = mp->elfeat_num;
     
    if (with_links) {
    	mp->trainset[c].head[0] = 0;
        strcpy(mp->trainset[c].s_deprel[0], "^^^");
        mp->trainset[c].deprel[0] = mp->deprel_num;
    }
    
    //ending symbol
    strcpy(mp->trainset[c].s_word[t], "$$$");
    mp->trainset[c].word_in[t] = 0;
    mp->trainset[c].word_out[t] = 0;
    
    strcpy(mp->trainset[c].s_lemma[t], "$$$");
    mp->trainset[c].lemma[t] = mp->lemma_num;
    
    strcpy(mp->trainset[c].s_cpos[t], "$$$");
    mp->trainset[c].cpos[t] = mp->cpos_num;
    
    strcpy(mp->trainset[c].s_pos[t], "$$$");
    mp->trainset[c].pos[t] = mp->pos_info_in.num;
    
    strcpy(mp->trainset[c].s_feat[t], "$$$");
    mp->trainset[c].feat_in[t] =  0;
    mp->trainset[c].feat_out[t] = 0;
    mp->trainset[c].elfeat_len[t] = 1;
    mp->trainset[c].elfeat[t][0] = mp->elfeat_num;
     
    if (with_links) {
    	mp->trainset[c].head[t] = 0;
        strcpy(mp->trainset[c].s_deprel[t], "$$$");
        mp->trainset[c].deprel[t] = mp->deprel_num;
    }
    
    mp->trainset[c].len = t - 1;

    if (mp->trainset[c].len == 0 && !read_smth) {
    }  else {

    }
}

void read_sentence_test(MODEL_PARAMS *mp, FILE *fp, int with_links, int c) {


    char buffer[MAX_LINE];

    int t = 1;
    int read_smth = 0;
    while (fgets(buffer, MAX_LINE, fp) != NULL) {
			if (buffer[0] == '#') {
				read_smth = 1;
				continue;
			}
			char *s = strtok(buffer, FIELD_SEPS);
			if (s == NULL) {
				break;
			}


	        strncpy(mp->testset[c].s_word[t], strtok(NULL, FIELD_SEPS), MAX_REC_LEN);

	        mp->testset[c].word_in[t] = atoi(strtok(NULL, FIELD_SEPS));

	        mp->testset[c].word_out[t] = atoi(strtok(NULL, FIELD_SEPS));

	        mp->testset[c].word[t] = atoi(strtok(NULL, FIELD_SEPS));

	        strncpy(mp->testset[c].s_lemma[t], strtok(NULL, FIELD_SEPS), MAX_REC_LEN);

	        mp->testset[c].lemma[t] = atoi(strtok(NULL, FIELD_SEPS));

	        strncpy(mp->testset[c].s_cpos[t], strtok(NULL, FIELD_SEPS), MAX_REC_LEN);
	        mp->testset[c].cpos[t] = atoi(strtok(NULL, FIELD_SEPS));
	        strncpy(mp->testset[c].s_pos[t], strtok(NULL, FIELD_SEPS), MAX_REC_LEN);
	        mp->testset[c].pos[t] = atoi(strtok(NULL, FIELD_SEPS));

	        strncpy(mp->testset[c].s_feat[t], strtok(NULL, FIELD_SEPS), MAX_REC_LEN);

	        mp->testset[c].feat_in[t] = atoi(strtok(NULL, FIELD_SEPS));

	        mp->testset[c].feat_out[t] = atoi(strtok(NULL, FIELD_SEPS));



	    	char *decomp_feats = strtok(NULL, FIELD_SEPS);
			#define CHECK_RANGE(x, size, name) if ((x) >= (size)) { fprintf(stderr, "Error: wrong %s: %d, exceeds allowed values of %d\n", name, (x), (size - 1)); exit(1);}
			CHECK_RANGE(mp->testset[c].pos[t], mp->pos_info_in.num, "POS");
			CHECK_RANGE(mp->testset[c].cpos[t], mp->cpos_num, "CPOS");
			CHECK_RANGE(mp->testset[c].lemma[t], mp->lemma_num, "LEMMA");
			CHECK_RANGE(mp->testset[c].feat_in[t], mp->pos_info_in.feat_infos[mp->testset[c].pos[t]].num, "FEAT_IN");
			CHECK_RANGE(mp->testset[c].feat_out[t], mp->pos_info_out.feat_infos[mp->testset[c].pos[t]].num, "FEAT_OUT");
	        if (with_links) {
	        	mp->testset[c].head[t] = atoi(strtok(NULL, FIELD_SEPS));

	            char *s_deprel = strtok(NULL, FIELD_SEPS);
	            strncpy(mp->testset[c].s_deprel[t], s_deprel, MAX_REC_LEN);

	            mp->testset[c].deprel[t] = atoi(strtok(NULL, FIELD_SEPS));


	            CHECK_RANGE(mp->testset[c].deprel[t], mp->deprel_num, "DEPREL");
	        }

	        fill_decomp_feats(mp, &mp->testset[c], t, decomp_feats);

	        t++;
	    }


	    //starting symbols
	    strcpy(mp->testset[c].s_word[0], "^^^");
	    mp->testset[c].word_in[0] = 0;
	    mp->testset[c].word_out[0] = 0;

	    strcpy(mp->testset[c].s_lemma[0], "^^^");
	    mp->testset[c].lemma[0] = mp->lemma_num;

	    strcpy(mp->testset[c].s_cpos[0], "^^^");
	    mp->testset[c].cpos[0] = mp->cpos_num;

	    strcpy(mp->testset[c].s_pos[0], "^^^");
	    mp->testset[c].pos[0] = mp->pos_info_in.num;
	    ASSERT(mp->pos_info_in.num == mp->pos_info_out.num);

	    strcpy(mp->testset[c].s_feat[0], "^^^");
	    mp->testset[c].feat_in[0] =  0;
	    mp->testset[c].feat_out[0] = 0;
	    mp->testset[c].elfeat_len[0] = 1;
	    mp->testset[c].elfeat[0][0] = mp->elfeat_num;

	    if (with_links) {
	    	mp->testset[c].head[0] = 0;
	        strcpy(mp->testset[c].s_deprel[0], "^^^");
	        mp->testset[c].deprel[0] = mp->deprel_num;
	    }

	    //ending symbol
	    strcpy(mp->testset[c].s_word[t], "$$$");
	    mp->testset[c].word_in[t] = 0;
	    mp->testset[c].word_out[t] = 0;

	    strcpy(mp->testset[c].s_lemma[t], "$$$");
	    mp->testset[c].lemma[t] = mp->lemma_num;

	    strcpy(mp->testset[c].s_cpos[t], "$$$");
	    mp->testset[c].cpos[t] = mp->cpos_num;

	    strcpy(mp->testset[c].s_pos[t], "$$$");
	    mp->testset[c].pos[t] = mp->pos_info_in.num;

	    strcpy(mp->testset[c].s_feat[t], "$$$");
	    mp->testset[c].feat_in[t] =  0;
	    mp->testset[c].feat_out[t] = 0;
	    mp->testset[c].elfeat_len[t] = 1;
	    mp->testset[c].elfeat[t][0] = mp->elfeat_num;

	    if (with_links) {
	    	mp->testset[c].head[t] = 0;
	        strcpy(mp->testset[c].s_deprel[t], "$$$");
	        mp->testset[c].deprel[t] = mp->deprel_num;
	    }

	    mp->testset[c].len = t - 1;


	    if (mp->testset[c].len == 0 && !read_smth) {
	    }  else {

	    }
}


void cat_print_i(char *s, char *field, int x) {
    char buf[MAX_LINE];
    sprintf(buf, "%s %d\n", field, x);
    ASSERT(strlen(buf) + strlen(s) + 1 < MAX_LINE * 25);
    strcat(s, buf);
}

void cat_print_f(char *s, char *field, double x) {
    char buf[MAX_LINE];
    sprintf(buf, "%s %e\n", field, x);
    ASSERT(strlen(buf) + strlen(s) + 1 < MAX_LINE * 25);
    strcat(s, buf);
}

void cat_print_s(char *s, char *field, char *x) {
    char buf[MAX_LINE];
    sprintf(buf,"%s %s\n", field, x);
    ASSERT(strlen(buf) + strlen(s) + 1 < MAX_LINE * 25);
    strcat(s, buf);
}

char *get_params_string(APPROX_PARAMS *ap) {
    DEF_ALLOC_ARRAY(s, char,  sizeof(char) * MAX_LINE * 25);

    
    cat_print_s(s,"MODEL_NAME", ap->model_name);
    cat_print_s(s,"TRAIN_FILE", ap->train_file);
    cat_print_i(s,"TRAIN_DATASET_SIZE", ap->train_dataset_size);
    cat_print_s(s,"TEST_FILE", ap->test_file);
    cat_print_i(s,"TEST_DATASET_SIZE", ap->test_dataset_size);
    cat_print_s(s,"OUT_FILE", ap->out_file);
    cat_print_s(s,"IO_SPEC_FILE", ap->io_spec_file);

    cat_print_s(s,"IH_LINK_SPEC_FILE",ap->ih_links_spec_file);
    cat_print_s(s, "HH_LINK_SPEC_FILE",ap->hh_links_spec_file);


       
    strcat(s, "\n");

    cat_print_i(s, "BEAM", ap->beam);

    cat_print_i(s, "RETURN_CAND", ap->return_cand);
    cat_print_i(s, "CAND_NUM", ap->cand_num);

    cat_print_i(s, "SEED", ap->seed);
    cat_print_f(s, "RAND_RANGE", ap->rand_range);
    cat_print_f(s, "EM_RAND_RANGE", ap->em_rand_range);
    strcat(s, "\n");
    
    
    cat_print_i(s, "HID_SIZE", ap->hid_size);

    strcat(s, "\n");
    
    cat_print_f(s,"INIT_ETA", ap->init_eta);
    cat_print_f(s,"ETA_RED_RATE", ap->eta_red_rate);
    cat_print_f(s,"MAX_ETA_RED", ap->max_eta_red);
    strcat(s, "\n");

    cat_print_f(s,"INIT_REG", ap->init_reg);
    cat_print_f(s,"REG_RED_RATE", ap->reg_red_rate);
    cat_print_f(s,"MAX_REG_RED", ap->max_reg_red);
    strcat(s, "\n");
    
    cat_print_f(s,"MOM", ap->mom);
    strcat(s, "\n");

    cat_print_i(s,"MAX_LOOPS", ap->max_loops);
    cat_print_i(s,"MAX_LOOPS_WO_ACCUR_IMPR", ap->max_loops_wo_accur_impr);
    cat_print_i(s,"LOOPS_BETWEEN_VAL", ap->loops_between_val);


    /// Word embedding hyper-params
    cat_print_i(s,"EMB_SIZE", ap->emb_size);
    cat_print_i(s,"WITHCACHE", ap->WITHCACHE);
    cat_print_i(s,"INITCOMP", ap->INITCOMP);
    cat_print_i(s,"WE_FIXED", ap->WE_FIXED);
    cat_print_i(s,"WithWE", ap->WITHWE);

    /// with search_base

    cat_print_i(s,"WITHNEG", ap->WITHNEGPATH);

    cat_print_i(s,"TRAINSET_SIZE", ap->trainset_size);
    cat_print_i(s,"TESTSET_SIZE", ap->testset_size);



    strcat(s, "\n");


    strcat(s, "\n");
    
    
    return s;

}

#define IF_EQ_SET_N_CONT_S(name, var) \
if (strcmp(field, name) == 0) {\
   char *s = field_missing_wrapper(strtok(NULL, FIELD_SEPS), name);\
   ASSERT(strlen(s) < MAX_LINE); \
   strcpy(var, s);\
   continue; \
}
#define IF_EQ_SET_N_CONT_I(name, var) \
if (strcmp(field, name) == 0) {\
   var =  atoi(field_missing_wrapper(strtok(NULL, FIELD_SEPS), name));\
   continue; \
}
#define IF_EQ_SET_N_CONT_F(name, var) \
if (strcmp(field, name) == 0) {\
   var =  atof(field_missing_wrapper(strtok(NULL, FIELD_SEPS), name));\
   continue; \
}

APPROX_PARAMS *read_params(char *fname) {
    DEF_FOPEN(fp, fname, "r");
    DEF_ALLOC(ap, APPROX_PARAMS);

    char buffer[MAX_LINE], *s;
    int line = 0;
    do {
        do {
            s = fgets(buffer, MAX_LINE, fp);
            if (s == NULL) {
                break;
            }
            line++;
        } while (buffer[0] == '#' || buffer[0] == '\0');
        if (s == NULL) {
            break;
        }

        char *field = strtok(buffer, FIELD_SEPS);
        if (field == NULL) {
            //empty line
            continue;
       }

        
        IF_EQ_SET_N_CONT_S("MODEL_NAME", ap->model_name);


        IF_EQ_SET_N_CONT_S("TRAIN_FILE", ap->train_file);
        IF_EQ_SET_N_CONT_I("TRAIN_DATASET_SIZE", ap->train_dataset_size);
        IF_EQ_SET_N_CONT_S("TEST_FILE", ap->test_file);
        IF_EQ_SET_N_CONT_I("TEST_DATASET_SIZE", ap->test_dataset_size);

        IF_EQ_SET_N_CONT_S("OUT_FILE", ap->out_file);
        IF_EQ_SET_N_CONT_S("IO_SPEC_FILE", ap->io_spec_file);

        IF_EQ_SET_N_CONT_S("IH_LINK_SPEC_FILE",ap->ih_links_spec_file);
        IF_EQ_SET_N_CONT_S("HH_LINK_SPEC_FILE",ap->hh_links_spec_file);

        


        IF_EQ_SET_N_CONT_I("BEAM", ap->beam);

        IF_EQ_SET_N_CONT_I("RETURN_CAND", ap->return_cand);
        IF_EQ_SET_N_CONT_I("CAND_NUM", ap->cand_num);
        
        
        IF_EQ_SET_N_CONT_I("SEED", ap->seed);
        IF_EQ_SET_N_CONT_F("RAND_RANGE", ap->rand_range);
        IF_EQ_SET_N_CONT_F("EM_RAND_RANGE", ap->em_rand_range);
        
        IF_EQ_SET_N_CONT_I("HID_SIZE", ap->hid_size);

        
        /// SGD with momentum
        IF_EQ_SET_N_CONT_F("INIT_ETA", ap->init_eta);
        IF_EQ_SET_N_CONT_F("ETA_RED_RATE", ap->eta_red_rate);
        IF_EQ_SET_N_CONT_F("MAX_ETA_RED", ap->max_eta_red);
        IF_EQ_SET_N_CONT_F("MOM", ap->mom);
        /// L2 Reg params

        IF_EQ_SET_N_CONT_F("INIT_REG", ap->init_reg);
        IF_EQ_SET_N_CONT_F("REG_RED_RATE", ap->reg_red_rate);
        IF_EQ_SET_N_CONT_F("MAX_REG_RED", ap->max_reg_red);
        
        

        IF_EQ_SET_N_CONT_I("MAX_LOOPS", ap->max_loops);
        IF_EQ_SET_N_CONT_I("MAX_LOOPS_WO_ACCUR_IMPR", ap->max_loops_wo_accur_impr);
        IF_EQ_SET_N_CONT_I("LOOPS_BETWEEN_VAL", ap->loops_between_val); 
        

        /// Word embedding hyper-params
        IF_EQ_SET_N_CONT_I("EMB_SIZE", ap->emb_size);
        IF_EQ_SET_N_CONT_I("WITHCACHE", ap->WITHCACHE);
        IF_EQ_SET_N_CONT_I("INITCOMP", ap->INITCOMP);
        IF_EQ_SET_N_CONT_I("WE_FIXED", ap->WE_FIXED);
        IF_EQ_SET_N_CONT_I("WithWE", ap->WITHWE);

        /// with search_base

        IF_EQ_SET_N_CONT_I("WITHNEG", ap->WITHNEGPATH);

        IF_EQ_SET_N_CONT_I("TRAINSET_SIZE", ap->trainset_size);
        IF_EQ_SET_N_CONT_I("TESTSET_SIZE", ap->testset_size);

        fprintf(stderr, "Error: unknown field '%s' in the settings file\n", field);
        exit(1);
    } while (1); 
    fclose(fp);
    return ap;
}

//an input feature specification, these features are used as  inputs to hidden layer
//specification approach is replicated from MALT parser
int a_state_int_field_wrapper(char* s) {
    if (s == NULL) {
        return 0;
    } else {
        return atoi(s);
    }
}

void skip_and_read(char *buffer, char *fname, FILE *fp) {
    fgets(buffer, MAX_LINE, fp);
    while (buffer[0] == '#' || buffer[0] == '\0' || buffer[0] == '\n') {
        if (fgets(buffer, MAX_LINE, fp) == NULL) {
            break;
        }
    } 
    
    if (strcmp(buffer,"") == 0) {
        fprintf(stderr, "Error: file %s is not complete\n", fname);
        exit(1);
    }
}

#define check_if_exceeds(val, s_val, bound, s_bound) if ( (val) > (bound) ) { fprintf(stderr, "Error: %s (%d) exceeds %s (%d)\n", s_val, val, s_bound, bound); exit(1); }

//reads IO specification file
void read_io_spec(char *fname, MODEL_PARAMS *mp) {
	FILE *fp;
		if ((fp = fopen(fname, "r")) == NULL) {
			fprintf(stderr, "Error: can't open file %s\n", fname);
			exit(1);
		}

		//CPOS
		char buffer[MAX_LINE];
		skip_and_read(buffer, fname, fp);
		mp->cpos_num = atoi(field_missing_wrapper_ext(strtok(buffer, FIELD_SEPS), "cpos_num", fname));
		check_if_exceeds(mp->cpos_num, "cpos_num", MAX_CPOS_SIZE, "MAX_CPOS_SIZE");

		//POS
		int pos_idx;
		skip_and_read(buffer, fname, fp);
		mp->pos_info_in.num = atoi(field_missing_wrapper_ext(strtok(buffer, FIELD_SEPS), "pos_num", fname));
		mp->pos_info_out.num = mp->pos_info_in.num;
		if (mp->pos_info_in.num > MAX_POS_INP_SIZE || mp->pos_info_out.num > MAX_POS_OUT_SIZE) {
			fprintf(stderr, "Error: pos_info_in(out).num (%d) exceeds MAX_POS_OUT_SIZE (%d) or/and MAX_POS_INP_SIZE (%d)\n", mp->pos_info_out.num, MAX_POS_OUT_SIZE, MAX_POS_INP_SIZE);
			exit(1);
		}

		fgets(buffer, MAX_LINE, fp);  // "# Contents"
		for (fgets(buffer, MAX_LINE, fp); buffer [0] == '#';
		     fgets(buffer, MAX_LINE, fp)) {
		  strtok(buffer, FIELD_SEPS);
		  pos_idx = atoi(field_missing_wrapper_ext(strtok(NULL, FIELD_SEPS),
							   "pos_name_id", fname));
		  char *pos_name = field_missing_wrapper_ext(strtok(NULL, FIELD_SEPS),
							     "pos_name", fname);

		}



		//FEAT IN
		skip_and_read(buffer, fname, fp);

		pos_idx = 0;
		mp->pos_info_in.feat_infos[0].num = atoi(field_missing_wrapper_ext(strtok(buffer, FIELD_SEPS), "feat[0]", fname));
		check_if_exceeds(mp->pos_info_in.feat_infos[pos_idx].num, "feat_num",  MAX_FEAT_INP_SIZE, "MAX_FEAT_INP_SIZE");
		for (pos_idx = 1; pos_idx < mp->pos_info_in.num; pos_idx++) {
			mp->pos_info_in.feat_infos[pos_idx].num = atoi(field_missing_wrapper_ext(strtok(NULL, FIELD_SEPS), "feat[pos_idx]", fname));
			check_if_exceeds(mp->pos_info_in.feat_infos[pos_idx].num, "feat_num",  MAX_FEAT_INP_SIZE, "MAX_FEAT_INP_SIZE");
		}

		//reserve for $$$ or ^^^ tag (not added in the real set) (it can be the same, because ^^^ - never happens in output, $$$ - never happens in input)
		mp->pos_info_in.feat_infos[mp->pos_info_in.num].num = 1;

		//WORD IN
		for (pos_idx = 0; pos_idx < mp->pos_info_in.num; pos_idx++) {
			skip_and_read(buffer, fname, fp);
			int feat_idx = 0;
			mp->pos_info_in.feat_infos[pos_idx].word_infos[feat_idx].num = atoi(field_missing_wrapper_ext(strtok(buffer, FIELD_SEPS),
					"word[pos_idx][feat_idx]", fname));
			check_if_exceeds(mp->pos_info_in.feat_infos[pos_idx].word_infos[feat_idx].num, "word_num", MAX_WORD_INP_SIZE, "MAX_WORD_INP_SIZE");

			for (feat_idx = 1; feat_idx < mp->pos_info_in.feat_infos[pos_idx].num; feat_idx++) {
				mp->pos_info_in.feat_infos[pos_idx].word_infos[feat_idx].num = atoi(field_missing_wrapper_ext(strtok(NULL, FIELD_SEPS), "word[pos_idx][feat_idx]", fname));
				check_if_exceeds(mp->pos_info_in.feat_infos[pos_idx].word_infos[feat_idx].num, "word_num", MAX_WORD_INP_SIZE, "MAX_WORD_INP_SIZE");
			}
		}

		//^^^
		mp->pos_info_in.feat_infos[mp->pos_info_in.num].word_infos[0].num = 1;



		//FEAT OUT
		skip_and_read(buffer, fname, fp);
		pos_idx = 0;
		mp->pos_info_out.feat_infos[pos_idx].num = atoi(field_missing_wrapper_ext(strtok(buffer, FIELD_SEPS), "feat[0]", fname));
		check_if_exceeds(mp->pos_info_out.feat_infos[pos_idx].num, "feat_num",  MAX_FEAT_OUT_SIZE, "MAX_FEAT_OUT_SIZE");
		for (pos_idx = 1; pos_idx < mp->pos_info_out.num; pos_idx++) {
			mp->pos_info_out.feat_infos[pos_idx].num = atoi(field_missing_wrapper_ext(strtok(NULL, FIELD_SEPS), "feat[pos_idx]", fname));
			check_if_exceeds(mp->pos_info_out.feat_infos[pos_idx].num, "feat_num",  MAX_FEAT_OUT_SIZE, "MAX_FEAT_OUT_SIZE");
		}

		//reserve for $$$ or ^^^ tag (not added in the real set) (it can be the same, because ^^^ - never happens in output, $$$ - never happens in input)
		mp->pos_info_out.feat_infos[mp->pos_info_out.num].num = 1;

		//WORD OUT
		for (pos_idx = 0; pos_idx < mp->pos_info_out.num; pos_idx++) {
			skip_and_read(buffer, fname, fp);
			int feat_idx = 0;
			mp->pos_info_out.feat_infos[pos_idx].word_infos[feat_idx].num = atoi(field_missing_wrapper_ext(strtok(buffer, FIELD_SEPS),
					"word[pos_idx][feat_idx]", fname));
			check_if_exceeds(mp->pos_info_out.feat_infos[pos_idx].word_infos[feat_idx].num, "word_num",  MAX_WORD_OUT_SIZE, "MAX_WORD_OUT_SIZE");

			for (feat_idx = 1; feat_idx < mp->pos_info_out.feat_infos[pos_idx].num; feat_idx++) {
				mp->pos_info_out.feat_infos[pos_idx].word_infos[feat_idx].num = atoi(field_missing_wrapper_ext(strtok(NULL, FIELD_SEPS), "word[pos_idx][feat_idx]", fname));
				check_if_exceeds(mp->pos_info_out.feat_infos[pos_idx].word_infos[feat_idx].num, "word_num",  MAX_WORD_OUT_SIZE, "MAX_WORD_OUT_SIZE");
			}
		}

		// $$$
		mp->pos_info_out.feat_infos[mp->pos_info_out.num].word_infos[0].num = 1;

		//WORD
		skip_and_read(buffer, fname, fp);
		mp->word_num = atoi(field_missing_wrapper_ext(strtok(buffer, FIELD_SEPS), "word_num", fname));
		check_if_exceeds(mp->word_num, "word_num", MAX_WORD_SIZE, "MAX_WORD_SIZE");
		//for WORDSONLYINPUT
	        set_word_index_hashtable(fp);

	    	//LEMMA
		skip_and_read(buffer, fname, fp);
		mp->lemma_num = atoi(field_missing_wrapper_ext(strtok(buffer, FIELD_SEPS), "lemma_num", fname));
		check_if_exceeds(mp->lemma_num, "lemma_num", MAX_LEMMA_SIZE, "MAX_LEMMA_SIZE");

	    	//WORD LEMMAS
		skip_and_read(buffer, fname, fp);
	    set_word_lemmas(buffer, fp, mp->word_num);

		//DEPREL
		skip_and_read(buffer, fname, fp);
		mp->deprel_num = atoi(field_missing_wrapper_ext(strtok(buffer, FIELD_SEPS), "deprel_num", fname));
		check_if_exceeds(mp->deprel_num, "deprel_num", MAX_DEPREL_SIZE, "MAX_DEPREL_SIZE");

		//ROOT DEPREL ID
		skip_and_read(buffer, fname, fp);
		int root_id = atoi(field_missing_wrapper_ext(strtok(buffer, FIELD_SEPS), "ROOT_DEPREL", fname));
		if (root_id != ROOT_DEPREL) {
			fprintf(stderr, "Error: wrong id for  ROOT_DEPREL in '%s',  %d instead of %d\n", fname, root_id, ROOT_DEPREL);
			exit(1);
		}


		strncpy(mp->s_deprel_root, field_missing_wrapper_ext(strtok(NULL, FIELD_SEPS), "s_deprel_root", fname), MAX_REC_LEN);

		int d;
		for (d = 0; d < mp->deprel_num; d++) {
			skip_and_read(buffer, fname, fp);
			int id = atoi(field_missing_wrapper_ext(strtok(buffer, FIELD_SEPS), "deprel", fname));
			if (id != d) {
				fprintf(stderr, "Error: wrong id for deprel in '%s',  %d instead of %d\n", fname, id, d);
				exit(1);
			}
			char *s_deprel = field_missing_wrapper_ext(strtok(NULL, FIELD_SEPS), "s_deprel", fname);
			strncpy(mp->s_deprel[d], s_deprel, MAX_REC_LEN);
		}

		//ELFEAT
		skip_and_read(buffer, fname, fp);
		mp->elfeat_num = atoi(field_missing_wrapper_ext(strtok(buffer, FIELD_SEPS), "elfeat_num", fname));
		check_if_exceeds(mp->elfeat_num, "elfeat_num", MAX_ELFEAT_SIZE, "MAX_ELFEAT_SIZE");







		fclose(fp);

		return;
}

int is_empty(char *s) {
    ASSERT(s != NULL);
    int i;
    for (i = 0; i < strlen(s); i++) {
        if (s[i] != ' ' && s[i] != '\t' && s[i] != '\n' && s[i] != '\r') {
            return 0;
        }
    }
    
    return 1;
}


//reads and reserves space in mp for corresponding weights
int  read_ih_links_spec(char *fname, MODEL_PARAMS *mp, IH_LINK_SPEC *specs, int *num) {
    FILE *fp;
    if ((fp = fopen(fname, "r")) == NULL) {
        fprintf(stderr, "Error: can't open file %s\n", fname);
        exit(1);
    }

    char buffer[MAX_LINE];

    int links_before = *num ;//mp->ih_links_num;
    while (fgets(buffer, MAX_LINE, fp) != NULL) {
        while (buffer[0] == '#') {
            if (fgets(buffer, MAX_LINE, fp) == NULL) {
                break;
            }
        } 
        if (is_empty(buffer)) {
            continue;
        }
        
        IH_LINK_SPEC *ih_spec = &(specs[(*num)++]);   //&(mp->ih_links_specs[mp->ih_links_num++])
        
        char *type = field_missing_wrapper_ext(strtok(buffer, FIELD_SEPS), "type", fname);
        if (strcmp("LEX", type) == 0) {
            ih_spec->type = WORD_TYPE;
        } else if (strcmp("POS", type) == 0) {
            ih_spec->type = POS_TYPE;
        } else if (strcmp("DEP", type) == 0) {
            ih_spec->type = DEPREL_TYPE;
        } else if (strcmp("LEMMA", type) == 0) {
            ih_spec->type = LEMMA_TYPE;
        } else if (strcmp("CPOS", type) == 0) {
            ih_spec->type = CPOS_TYPE;
        } else if (strcmp("FEATS", type) == 0) {
            ih_spec->type = FEAT_TYPE;
        } else {
            fprintf(stderr, "Error: value '%s' is wrong for the first field in file '%s'\n", type, fname);
            exit(1);
        }
        
        char *src = field_missing_wrapper_ext(strtok(NULL, FIELD_SEPS), "src", fname);
        if (strcmp("STACK", src) == 0) {
            ih_spec->src = STACK_SRC;
        } else if (strcmp("INPUT", src) == 0) {
            ih_spec->src = INP_SRC;
        } else {
            fprintf(stderr, "Error: value '%s' is wrong for the second field in file '%s'\n", type, fname);
            exit(1);
        }

        ih_spec->offset_curr = a_state_int_field_wrapper(strtok(NULL, FIELD_SEPS));
        ih_spec->offset_orig = a_state_int_field_wrapper(strtok(NULL, FIELD_SEPS));
        ih_spec->head = a_state_int_field_wrapper(strtok(NULL, FIELD_SEPS));
        ih_spec->rc = a_state_int_field_wrapper(strtok(NULL, FIELD_SEPS));
        ih_spec->rs = a_state_int_field_wrapper(strtok(NULL, FIELD_SEPS));
        
        if (*num >= MAX_IH_LINKS) {
            fprintf(stderr, "Number of input links exceeded MAX_IH_LINKS = %d\n", MAX_IH_LINKS);
            exit(1);
        }
    }
    
   fclose(fp); 
   return *num - links_before;
}


//reads and reserves space in mp for corresponding weights
int read_hh_links_spec(char *fname, MODEL_PARAMS *mp) {
    FILE *fp;
    if ((fp = fopen(fname, "r")) == NULL) {
        fprintf(stderr, "Error: can't open file %s\n", fname);
        exit(1);
    }

    char buffer[MAX_LINE];

    int links_before = mp->hh_links_num+1;

    while (fgets(buffer, MAX_LINE, fp) != NULL) {
        while (buffer[0] == '#') {
            if (fgets(buffer, MAX_LINE, fp) == NULL) {
                break;
            }
        } 
        if (is_empty(buffer)) {
            continue;
        }
        
        HH_LINK_SPEC *hh_spec = &(mp->hh_links_specs[mp->hh_links_num++]);

            
        char *when = field_missing_wrapper_ext(strtok(buffer, FIELD_SEPS), "when", fname);
        if (strcmp("FIND_FIRST", when) == 0) {
            hh_spec->when = FIND_FIRST;
        } else if (strcmp("FIND_LAST", when) == 0) {
            hh_spec->when = FIND_LAST;
        } else {
            fprintf(stderr, "Error: value '%s' is wrong for the first field in file '%s'\n", when, fname);
            exit(1);
        }
        
        char *src = field_missing_wrapper_ext(strtok(NULL, FIELD_SEPS), "orig_src", fname);
        if (strcmp("STACK", src) == 0) {
            hh_spec->orig_src = STACK_SRC;
        } else if (strcmp("INPUT", src) == 0) {
            hh_spec->orig_src = INP_SRC;
        } else {
            fprintf(stderr, "Error: value '%s' is wrong for the second field in file '%s'\n", src, fname);
            exit(1);
        }

        hh_spec->orig_offset = atoi(field_missing_wrapper_ext(strtok(NULL, FIELD_SEPS), "orig_offset", fname));
        hh_spec->orig_head = atoi(field_missing_wrapper_ext(strtok(NULL, FIELD_SEPS), "orig_head", fname));
        hh_spec->orig_rrc = atoi(field_missing_wrapper_ext(strtok(NULL, FIELD_SEPS), "orig_rrc", fname));
        hh_spec->orig_rs = atoi(field_missing_wrapper_ext(strtok(NULL, FIELD_SEPS), "orig_rs", fname));

        
        src = field_missing_wrapper_ext(strtok(NULL, FIELD_SEPS), "target_src", fname);
        if (strcmp("STACK", src) == 0) {
            hh_spec->target_src = STACK_SRC;
        } else if (strcmp("INPUT", src) == 0) {
            hh_spec->target_src = INP_SRC;
        } else {
            fprintf(stderr, "Error: value '%s' is wrong in file '%s'\n", src, fname);
            exit(1);
        }

        hh_spec->target_offset = atoi(field_missing_wrapper_ext(strtok(NULL, FIELD_SEPS), "target_offset", fname));
       
        char *act = strtok(NULL, FIELD_SEPS);
        
        if (act == NULL) {
            act = "ANY";
        }
        
        if (strcmp("LA", act) == 0) {
            hh_spec->act = LA;
        } else if (strcmp("RA", act) == 0) {
            hh_spec->act = RA;
        } else if (strcmp("SHIFT", act) == 0) {
            hh_spec->act = SHIFT;
        } else if (strcmp("RED", act) == 0) {
            hh_spec->act = RED;
        } else if (strcmp("ANY", act) == 0) {
            hh_spec->act = ANY;
        } else {    
            fprintf(stderr, "Error: value '%s' is wrong for in file '%s'\n", act, fname);
            exit(1);
        }
       
        char *offset = strtok(NULL, FIELD_SEPS);

        if (offset == NULL) {
            //default offset is any 
            hh_spec->offset = -1;
        } else {
            hh_spec->offset = atoi(offset);
        }

        
        if (mp->hh_links_num+1 >= MAX_HH_LINKS) {
            fprintf(stderr, "Number of input links exceeded MAX_HH_LINKS = %d\n", MAX_HH_LINKS);
            exit(1);
        }
    }
    fclose(fp);
    return mp->hh_links_num+1 - links_before;
}

//allocates memory space for all the weights
//should be invoked after reading hh, ih and io specs
void allocate_weights(APPROX_PARAMS *ap, MODEL_PARAMS *mp) {

    
    /////////////// cached features
	DEF_FOPEN(ifp, "FreqFeatures", "r");
	int l;
	int ind = -1;
	int id = 1;
	while (!feof (ifp))
		{

		  fscanf (ifp, "%d", &l);
		  fscanf (ifp, "%d", &ind);
		  mp->chosenfeature[l][ind] = id;
		  id++;
		}
	mp->composed_feat_num = id-1;
	fclose(ifp);


	ALLOC_ARRAY(mp->trainset, SENTENCE, ap->trainset_size);
	ALLOC_ARRAY(mp->testset, SENTENCE, ap->testset_size);


    ALLOC_ARRAY(mp->deprel, IM_LINK, mp->deprel_num + 2);
    ALLOC_ARRAY(mp->pos, IM_LINK, mp->pos_info_in.num + 1);
    ALLOC_ARRAY(mp->lemma, IM_LINK, mp->lemma_num + 1);
	ALLOC_ARRAY(mp->word, IM_LINK, mp->word_num + 1);
	ALLOC_ARRAY(mp->elfeat, IM_LINK, mp->elfeat_num + 1);
	ALLOC_ARRAY(mp->composed_feat, IH_LINK, mp->composed_feat_num + 1);


	ALLOC_ARRAY(mp->eh_link_ih, EH_LINK, mp->ih_links_num);



    ALLOC_ARRAY(mp->hh_link, HH_LINK, mp->hh_links_num+1);





}

void free_weights(MODEL_PARAMS *mp, APPROX_PARAMS *ap) {

    //free input links

    free(mp->deprel);

    free(mp->pos);


    free(mp->word);
    free(mp->lemma);
    free(mp->elfeat);




    free(mp->composed_feat);


    free(mp->eh_link_ih);



    //free HID-HID weight matrixes
    free(mp->hh_link);
    



}

int get_matched(SENTENCE *sent1, SENTENCE *sent2, int labeled) {
    if (sent1->len != sent2->len) {
        fprintf(stderr, "Error: comparing sentences of different length: %d vs %d\n", sent1->len, sent2->len);
        exit(-1);
    }
    int t, matched = 0;
    for (t = 1; t < sent1->len + 1; t++) {
        ASSERT(strcmp(sent1->s_word[t], sent2->s_word[t]) == 0);
        //printf("%d-%d ", sent1->head[t], sent2->head[t]);
        if (labeled) {
            matched += (sent1->head[t] == sent2->head[t]) && (strcmp(sent1->s_deprel[t], sent2->s_deprel[t]) == 0);
        } else {
            matched += (sent1->head[t] == sent2->head[t]);
        }
    }

    return matched;
}

void print_state(char *s, APPROX_PARAMS *ap) {
    cat_print_s(s, "WHERE", ap->st.where);
    cat_print_f(s, "CURR_REG", ap->st.curr_reg);
    cat_print_f(s, "CURR_ETA", ap->st.curr_eta);
    cat_print_f(s, "MAX_SCORE", ap->st.max_score);
    cat_print_f(s, "LAST_SCORE", ap->st.last_score);
    cat_print_f(s, "LAST_TR_ERR", ap->st.last_tr_err);
    cat_print_i(s, "LOOPS_NUM", ap->st.loops_num);
    cat_print_i(s, "NUM_SUB_OPT", ap->st.num_sub_a_state);
}


void save_state(APPROX_PARAMS *ap) {
    char state_fname[MAX_NAME];
    strcpy(state_fname, ap->model_name);
    strcat(state_fname, ".state");

    FILE *fp;
    if ((fp = fopen(state_fname, "w")) == NULL) {
        fprintf(stderr, "Error: can't open file %s\n", state_fname);
        exit(1);
    }
    DEF_ALLOC_ARRAY(s, char,  sizeof(char) * MAX_LINE * 25);
        
    print_state(s, ap);
    
    fprintf(fp, s);
    free(s);
    fclose(fp); 
}

int load_state(APPROX_PARAMS *ap) {
    char state_fname[MAX_NAME];
    strcpy(state_fname, ap->model_name);
    strcat(state_fname, ".state");

    FILE *fp;
    if ((fp = fopen(state_fname, "r")) == NULL) {
        return 0;
    }

    char buffer[MAX_LINE], *s;
    int line = 0;
    do {
        do {
            s = fgets(buffer, MAX_LINE, fp);
            if (s == NULL) {
                break;
            }
            line++;
        } while (buffer[0] == '#' || buffer[0] == '\0');
        if (s == NULL) {
            break;
        }

        char *field = strtok(buffer, FIELD_SEPS);
        if (field == NULL) {
            continue;
        }

        IF_EQ_SET_N_CONT_S("WHERE", ap->st.where);
        IF_EQ_SET_N_CONT_F("CURR_REG", ap->st.curr_reg);
        IF_EQ_SET_N_CONT_F("CURR_ETA", ap->st.curr_eta);
        IF_EQ_SET_N_CONT_F("MAX_SCORE", ap->st.max_score);
        IF_EQ_SET_N_CONT_F("LAST_SCORE", ap->st.last_score);
        IF_EQ_SET_N_CONT_F("LAST_TR_ERR", ap->st.last_tr_err);
        IF_EQ_SET_N_CONT_I("LOOPS_NUM", ap->st.loops_num);   
        IF_EQ_SET_N_CONT_I("NUM_SUB_OPT", ap->st.num_sub_a_state);

        
        fprintf(stderr, "Error: unknown field '%s' in the state file\n", field);
        exit(1);
    } while (1); 
    fclose(fp);
    return 1;

}


void save_candidates(APPROX_PARAMS *ap, MODEL_PARAMS *mp, SENTENCE *inp_sent, STATE **best_states, int avail_num) {
    if (!ap->return_cand) {
        return;
    }
    
    char cand_fname[MAX_NAME];
    strcpy(cand_fname, ap->out_file);
    strcat(cand_fname, ".top");

    DEF_FOPEN(fp, cand_fname, "a");

    int num = ap->cand_num <= avail_num ? ap->cand_num : avail_num;
    
    fprintf(fp, "%d\n\n", num);
    int i;
    for (i = 0; i < num; i++) {
        DEF_ALLOC(sent, SENTENCE);
        memcpy(sent, inp_sent, sizeof(SENTENCE));
        pt_fill_sentence(best_states[i]->pt, mp, sent);
        char *s = print_sent(sent, 1);
        fprintf(fp, "%e\n%s\n", best_states[i]->lprob, s);
        free(s);
        free(sent);
    } 

    fclose(fp);
}

#define REWR_IF_EQ_SET_N_CONT_S(name, var) \
{\
    char s[MAX_NAME]; \
    strcpy(s, argv[i]);\
    char *val = index(s, '='); \
    if (val == NULL) {\
        fprintf(stderr, "Error: Wrong parameter '%s'\n", argv[i]);\
        exit(1);\
    }\
    val[0] = '\0'; \
    val++;\
    if (strcmp(s, name) == 0) {\
        strcpy(var, val);\
        continue;\
    }\
}
#define REWR_IF_EQ_SET_N_CONT_I(name, var) \
{\
    char s[MAX_NAME]; \
    strcpy(s, argv[i]);\
    char *val = index(s, '='); \
    if (val == NULL) {\
        fprintf(stderr, "Error: Wrong parameter '%s'\n", argv[i]);\
        exit(1);\
    }\
    val[0] = '\0'; \
    val++;\
    if (strcmp(s, name) == 0) {\
        var = atoi(val);\
        continue;\
    }\
}
#define REWR_IF_EQ_SET_N_CONT_F(name, var) \
{\
    char s[MAX_NAME]; \
    strcpy(s, argv[i]);\
    char *val = index(s, '='); \
    if (val == NULL) {\
        fprintf(stderr, "Error: Wrong parameter '%s'\n", argv[i]);\
        exit(1);\
    }\
    val[0] = '\0'; \
    val++;\
    if (strcmp(s, name) == 0) {\
        var = atof(val);\
        continue;\
    }\
}

void rewrite_parameters(APPROX_PARAMS *ap, int argc, char** argv, int start_idx) {

    int i;
    for (i = start_idx; i < argc; i++) {

        char field[MAX_NAME]; 
        strcpy(field, argv[i]);
        char *type = index(field, '='); 
        if (type == NULL) {
            fprintf(stderr, "Error: Wrong parameter '%s'\n", argv[i]);
            exit(1);
        }
        type[0] = '\0'; 
        type++;

        REWR_IF_EQ_SET_N_CONT_S("MODEL_NAME", ap->model_name);

        REWR_IF_EQ_SET_N_CONT_S("TRAIN_FILE", ap->train_file);
        REWR_IF_EQ_SET_N_CONT_I("TRAIN_DATASET_SIZE", ap->train_dataset_size);
        REWR_IF_EQ_SET_N_CONT_S("TEST_FILE", ap->test_file);
        REWR_IF_EQ_SET_N_CONT_I("TEST_DATASET_SIZE", ap->test_dataset_size);

        REWR_IF_EQ_SET_N_CONT_S("OUT_FILE", ap->out_file);

        REWR_IF_EQ_SET_N_CONT_S("IO_SPEC_FILE", ap->io_spec_file);

        REWR_IF_EQ_SET_N_CONT_S("IH_LINK_SPEC_FILE",ap->ih_links_spec_file);
        REWR_IF_EQ_SET_N_CONT_S("HH_LINK_SPEC_FILE",ap->hh_links_spec_file);



        REWR_IF_EQ_SET_N_CONT_I("BEAM", ap->beam);

        REWR_IF_EQ_SET_N_CONT_I("RETURN_CAND", ap->return_cand);
        REWR_IF_EQ_SET_N_CONT_I("CAND_NUM", ap->cand_num);

        REWR_IF_EQ_SET_N_CONT_I("SEED", ap->seed);
        REWR_IF_EQ_SET_N_CONT_F("RAND_RANGE", ap->rand_range);
        REWR_IF_EQ_SET_N_CONT_F("EM_RAND_RANGE", ap->em_rand_range);
        
        REWR_IF_EQ_SET_N_CONT_I("HID_SIZE", ap->hid_size);


        REWR_IF_EQ_SET_N_CONT_F("INIT_ETA", ap->init_eta);
        REWR_IF_EQ_SET_N_CONT_F("ETA_RED_RATE", ap->eta_red_rate);
        REWR_IF_EQ_SET_N_CONT_F("MAX_ETA_RED", ap->max_eta_red);
        
        REWR_IF_EQ_SET_N_CONT_F("INIT_REG", ap->init_reg);
        REWR_IF_EQ_SET_N_CONT_F("REG_RED_RATE", ap->reg_red_rate);
        REWR_IF_EQ_SET_N_CONT_F("MAX_REG_RED", ap->max_reg_red);
        
        REWR_IF_EQ_SET_N_CONT_F("MOM", ap->mom);
        
        REWR_IF_EQ_SET_N_CONT_I("MAX_LOOPS", ap->max_loops);
        REWR_IF_EQ_SET_N_CONT_I("MAX_LOOPS_WO_ACCUR_IMPR", ap->max_loops_wo_accur_impr);
        REWR_IF_EQ_SET_N_CONT_I("LOOPS_BETWEEN_VAL", ap->loops_between_val); 


		/// Word embedding hyper-params
        REWR_IF_EQ_SET_N_CONT_I("EMB_SIZE", ap->emb_size);
        REWR_IF_EQ_SET_N_CONT_I("WITHCACHE", ap->WITHCACHE);
        REWR_IF_EQ_SET_N_CONT_I("INITCOMP", ap->INITCOMP);
        REWR_IF_EQ_SET_N_CONT_I("WE_FIXED", ap->WE_FIXED);
        REWR_IF_EQ_SET_N_CONT_I("WithWE", ap->WITHWE);

		/// with search_base

        REWR_IF_EQ_SET_N_CONT_I("WITHNEG", ap->WITHNEGPATH);

        REWR_IF_EQ_SET_N_CONT_I("TRAINSET_SIZE", ap->trainset_size);
        REWR_IF_EQ_SET_N_CONT_I("TESTSET_SIZE", ap->testset_size);

        fprintf(stderr, "Error: unknown field '%s' in the command line\n", argv[i]);
        exit(1);
    }

    if (ap->hid_size > MAX_HID_SIZE) {
        fprintf(stderr, "Error: HID_SIZE (%d) exceeds MAX_HID_SIZE (%d), try adjusting MAX_HID_SIZE and rebuilding the parser\n",
                ap->hid_size, MAX_HID_SIZE);
        exit(1);
    }


    if (ap->emb_size > MAX_EMB_SIZE) {
        fprintf(stderr, "Error: EMB_SIZE (%d) exceeds MAX_EMB_SIZE (%d), try adjusting MAX_EMB_SIZE and rebuilding the parser\n",
                ap->emb_size, MAX_EMB_SIZE);
        exit(1);
    }
    return;
}


//////////////////// dummy functions to be compatible with Jamie's io format

void set_word_lemmas(char *line, FILE *specfileptr, int numwords) {

  char buffer[MAXWORDLENGTH + 11];


  if (specfileptr != NULL) {

    strcpy(buffer, line);
    while (buffer[0] != '\n') {
     if (fgets(buffer, MAXWORDLENGTH + 10, specfileptr) == NULL)
    	 break;
    }

  }
  else {
    fprintf(stderr, "Error: could not read spec file for words.\n");
    exit(0);
  }
}


void set_word_index_hashtable(FILE *specfileptr) {

  char buffer[MAXWORDLENGTH + 11];

  if (specfileptr != NULL) {
    //while (fgets(buffer, MAXWORDLENGTH + 10, specfileptr) != NULL) {
    //  if (!strncmp(buffer, "# ------ WORD ---------\n", 20))
    // 	  break;
    //}
    //fgets(buffer, MAXWORDLENGTH + 10, specfileptr);  // # Size
    //fgets(buffer, MAXWORDLENGTH + 10, specfileptr);  // size
    while (fgets(buffer, MAXWORDLENGTH + 10, specfileptr) != NULL) {
      if (buffer[0] == '#')
	continue;
      if (buffer[0] == '\n')
	break;


    }
  }
  else {
    fprintf(stderr, "Error: could not read spec file for words.\n");
    exit(0);
  }


}

