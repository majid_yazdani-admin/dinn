/*
    Majid Yazdani and James Henderson

	Incremental Recurrent Neural Network Dependency Parser with Search-based Discriminative Training , CoNLL 2015
*/

//COMMON CONSTS FOR DEP PARSING EXPERIMENTS

#ifndef _DINN_H
#define _DINN_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdarg.h>
#include <values.h>
#include <time.h>
#include <gsl/gsl_linalg.h>


#include "dinn_io_spec.h"

//DEF FUNCTIONS

#define CHECK_PTR(x) if ((x) == NULL) { printf("Can't allocate: %s:%d\n", __FILE__,  __LINE__); exit(-1);}
#define ALLOC(p, type) p = (type *) calloc(sizeof(type), 1); if ((p) == NULL) { printf("Can't allocate: %s:%d\n", __FILE__,  __LINE__); exit(-1);}
#define DEF_ALLOC(p, type) type *p = (type *) calloc(sizeof(type), 1); if ((p) == NULL) { printf("Can't allocate: %s:%d\n", __FILE__,  __LINE__); exit(-1);}
#define FREE(p) free(p)
#define ALLOC_ARRAY(p, type, size) p = (type *) calloc(sizeof(type), size); if ((p) == NULL) { printf("Can't allocate: %s:%d\n", __FILE__,  __LINE__); exit(-1);}
#define DEF_ALLOC_ARRAY(p, type, size) type *p = (type *) calloc(sizeof(type), size); if ((p) == NULL) { printf("Can't allocate: %s:%d\n", __FILE__,  __LINE__); exit(-1);}

#define DEF_FOPEN(fp, fname, access) \
         FILE *fp;\
         if (!(fp = fopen(fname, access))) { \
         fprintf(stderr, "Error: can't open file '%s'\n", fname); \
         exit(1); \
         } 
#define FOPEN(fp, fname, access) \
         if (!(fp = fopen(fname, access))) { \
         fprintf(stderr, "Error: can't open file '%s'\n", fname); \
         exit(1); \
         } 


#define ASSERT_ON 1
#if ASSERT_ON
#define ASSERT(x) if (!(x)) {  printf("Assertion failed: '%s':%d\n", __FILE__,  __LINE__);}
#else
#define ASSERT(x)
#endif

#define SQR(x)  ((x) * (x))
#define CUB(x)  ((x) * (x) * (x))
#define MAX(x,y) (((x) > (y)) ? (x) : (y));
#define BIN_RAND(mju) ((drand48() < (mju)) ? 1 : 0)

//maximum length of a record
#define MAX_REC_LEN 100


//CONSTANTS
#define MAX_HID_SIZE 200
#define MAX_EMB_SIZE 200

#define MAX_BEAM 100

#define OCCURED_MIN 0
#define min_pad 0.0001
#define MAX_LINE 10000
#define MAX_WGT_LINE  100000

#define MAX_NAME 1000

#define SLICE_ALT_EPS 1.e-5
#define MJU_STEP_EPS 1.e-12

#define PERCENTAGE 0.5
#define WITHHH 1
#define WITHdrop 0
#define FF 1
#define MF 2
//#define lambda 1



//to be used both in outputs and inputs
#define ACTION_NUM 4
#define SHIFT 0 //shift to stack (and predict word) 
#define RED 1  //reduce from stack
#define LA 2 // left arc operation ( <--- + reduce from stack)
#define RA 3 // right arc operation 
#define ANY -1 //matches any operation


#define ROOT_DEPREL -1

#define FIELD_SEPS " \t\n\r"



typedef struct layer {
    double hidden[MAX_HID_SIZE];
    double hidden_mask[MAX_HID_SIZE];
    double  hidden_err[MAX_HID_SIZE];
} LAYER;

typedef struct hh_links {
    double w[MAX_HID_SIZE][MAX_HID_SIZE];
    double i_w_del[MAX_HID_SIZE][MAX_HID_SIZE];
    double s_w_del[MAX_HID_SIZE][MAX_HID_SIZE];
    double occured;
} HH_LINK;

typedef struct eh_links {
    double w[MAX_HID_SIZE][MAX_EMB_SIZE];
    double i_w_del[MAX_HID_SIZE][MAX_EMB_SIZE];
    double s_w_del[MAX_HID_SIZE][MAX_EMB_SIZE];
    double occured;
} EH_LINK;

typedef struct ih_biases {
    double b[MAX_HID_SIZE];
    double i_b_del[MAX_HID_SIZE];
    double s_b_del[MAX_HID_SIZE];
    double occured;
} IH_LINK;

typedef struct im_biases {
    double b[MAX_EMB_SIZE];
    double i_b_del[MAX_EMB_SIZE];
    double s_b_del[MAX_EMB_SIZE];
    double occured;
} IM_LINK;
typedef struct out_link {
    double b[MAX_OUT];
    double i_b_del[MAX_OUT];
    double s_b_del[MAX_OUT];
    double w[MAX_OUT][MAX_HID_SIZE];
    double i_w_del[MAX_OUT][MAX_HID_SIZE];
    double s_w_del[MAX_OUT][MAX_HID_SIZE];

    void *info;
} OUT_LINK;


#define DEPREL_TYPE 1
#define POS_TYPE 2
#define WORD_TYPE 3
#define FEAT_TYPE 4
#define CPOS_TYPE 5 
#define LEMMA_TYPE 6

#define INP_SRC 1
#define STACK_SRC 2

typedef struct inp_feature_spec {
    int type;  //type of information (_TYPE)
    int src;   //src of information: input string, stack or partial dep graph (_SRC)
    int offset_curr; //offset in current structure
    int offset_orig; //offset in original input 
    int head;  //number of head operations  
    int rc; //number of rightmost child operations or leftmost (if negative)
    int rs; //number of right seebling applications or left seebling (if negative) 

} IH_LINK_SPEC;

#define FIND_FIRST 1
#define FIND_LAST 2

//hidden to hidden connection specification
typedef struct hh_links_spec {
    //either find first match (=leftmost) or the last one (=rightmost)
    int when;
    
    //take node A with the following properties
    int orig_src;   //src of original node: input or stack (_SRC)
    int orig_offset; //offset in original node 
    int orig_head; //number of head operations
    //NB: NEXT OPERATIONS ARE NOT RC OPERATIONS: here, it  righmost RIGHT child (child_pos > head_pos)
    int orig_rrc; //number of rightmost right child or leftmost left child ops (if negative)
    int orig_rs; //number of right seebling applications or left seebling (if negative)
    
    //find state in the history with where A matches to the following node
    int target_src;
    int target_offset;
    
    //next operation after match: SHIFT, RED, LA, RA and -1, which matches anything
    int act; 

    //number of states before current (1 - previous, 2 ... - further in the history; -1 - any)
    int offset;

} HH_LINK_SPEC;



typedef struct outputs {
    //number of outputs
    int num;
    
    //output mask (1 for used, 0 for unused), output is action and labels both in one vector
    double mask[MAX_OUT];
    int is_masked;

    
    //output activataions
    double q[MAX_OUT];
    //output error
    double del[MAX_OUT];


    //parts under exponent in output activations
    double parts[MAX_OUT];
    //exponents in outputs activations
    double exp[MAX_OUT];
    //norm 
    double norm;

    double  b[MAX_OUT];
} OUTPUTS;

typedef struct stack {
    int size;
    int elems[MAX_SENT_LEN];

} STACK;

typedef struct node {
    int child_num, head, deprel;
    int pos;
    //sorted from left to right
    int childs[MAX_SENT_LEN];

    // field for traversing: the previous node on the SST
    int _prev_node;
    int _distance;
    
} NODE;

typedef struct part_tree {
    int len;
    NODE *nodes;
} PART_TREE;




#define MAX_IH_LINKS 100
//maximum number of distrinct (elementary) features for any word
#define MAX_SIMULT_FEAT 30

//bounded by MAX_SIMULT_FEAT * MAX_IH_LINKS, 
#define MAX_IH_LINKS_OPT MAX_IH_LINKS + MAX_SIMULT_FEAT * 10 


#define MAX_HH_LINKS 30

typedef struct state {

    //log probability of preceeding steps
    double lprob;               
    
    int previous_action;
    int previous_out;
    struct state *previous_state;
    //period - id of element in queue
    int period;
    
    struct state_list *next_states;


    LAYER int_layer;


    LAYER prev_layer;
   
    double un_hidden[MAX_HID_SIZE];
    

    //current state outputs
    OUTPUTS outputs;

    OUT_LINK *out_link;

    
    //links
    LAYER  *rel_layers[MAX_HH_LINKS];
    HH_LINK *rel_weights[MAX_HH_LINKS];
    int rel_num;


    int composed[MAX_IH_LINKS_OPT];

    IH_LINK *inp_biases[5];
    IM_LINK *i_biases[MAX_IH_LINKS_OPT];
    EH_LINK *eh_biases[MAX_IH_LINKS_OPT];
    IH_LINK *composed_biases[MAX_IH_LINKS_OPT];
    int input_num;
    int i_num;


    STACK stack;
    int queue;
   
    //stored only in the frontier states
    PART_TREE *pt; 
} STATE;

typedef struct state_list {
    struct state_list *next, *prev;
    STATE *elem;
} STATE_LIST;

typedef struct word_info {
    int num;;
} WORD_INFO;

typedef struct feat_info {
    int num;
    WORD_INFO word_infos[MAX_FEAT_INP_SIZE];
} FEAT_INFO;

typedef struct pos_info {
    int num;
    char *s_pos[MAX_POS_INP_SIZE + 1];
    FEAT_INFO feat_infos[MAX_POS_INP_SIZE + 1];
} POS_INFO;




typedef struct tr_state {
    char where[MAX_NAME];
    double curr_reg, curr_eta;
    double max_score, last_score;
    double last_tr_err;
    int loops_num, num_sub_a_state;

    //derivative of curr_reg and curr_eta (not to be saved)
    double decay;

} TR_STATE;







//NB: it starts from index 1. 'len' counts only meaningful elements
//So, len + 1 elements in total
typedef struct sentence {
    int len;
    char s_word[MAX_SENT_LEN + 2][MAX_REC_LEN];
    int  word_in[MAX_SENT_LEN + 2], word_out[MAX_SENT_LEN + 2], word[MAX_SENT_LEN+2];

    char s_lemma[MAX_SENT_LEN + 2][MAX_REC_LEN];
    int lemma[MAX_SENT_LEN + 2];

    char s_cpos[MAX_SENT_LEN + 2][MAX_REC_LEN];
    int cpos[MAX_SENT_LEN + 2];

    char s_pos[MAX_SENT_LEN + 2][MAX_REC_LEN];
    int pos[MAX_SENT_LEN + 2];

    char s_feat[MAX_SENT_LEN + 2][MAX_REC_LEN];
    int feat_in[MAX_SENT_LEN + 2], feat_out[MAX_SENT_LEN + 2], elfeat_len[MAX_SENT_LEN + 2], elfeat[MAX_SENT_LEN + 2][MAX_SIMULT_FEAT];

    int head[MAX_SENT_LEN + 2];

    char s_deprel[MAX_SENT_LEN + 2][MAX_REC_LEN];
    int  deprel[MAX_SENT_LEN + 2];

} SENTENCE;

//information about the networks (weights, connection specification etc)
typedef struct model_params {

	int feature_found ;
	int feature_notfound ;
    //input links and input specification
    int ih_links_num;
    IH_LINK_SPEC ih_links_specs[MAX_IH_LINKS];


    /////// embeddings
    IM_LINK
    		*elfeat,
            *deprel, //[deprel]
            *pos, //[pos]
            *lemma, //[lemma]
            *word; //[word]

    ///////// cached features
    IH_LINK *composed_feat;

    //// position matrices
    EH_LINK
            *eh_link_ih; //[link]



    ////////////// biases
    IH_LINK

	init_bias,
	hid_bias,
	//for decision states
	prev_act_bias[ACTION_NUM],
	prev_deprel_bias[ACTION_NUM][MAX_DEPREL_SIZE];
    
    //hh links specifications and weights
    int hh_links_num;
    HH_LINK_SPEC hh_links_specs[MAX_HH_LINKS];
    HH_LINK *hh_link; //[link_id]
    

    POS_INFO pos_info_out, pos_info_in;
    
    OUT_LINK 
        out_link;

    int cpos_num, deprel_num, lemma_num, elfeat_num, word_num, composed_feat_num;
  
    
    char s_deprel[MAX_DEPREL_SIZE][MAX_REC_LEN];
    char s_deprel_root[MAX_REC_LEN]; 
    
    SENTENCE* trainset;
    SENTENCE* testset;

    /////// hacky implemetation of feature caching
    int chosenfeature[100][MAX_WORD_SIZE];
    
} MODEL_PARAMS;
typedef struct approx_params {


	FILE* fp;
    int beam;
    int seed;
    double rand_range, em_rand_range;
    
    int hid_size;
    int emb_size;
    int INITCOMP;
    int WITHCACHE;
    int WITHNEGPATH;
	int WE_FIXED;
	int WITHWE;


    int trainset_size;
    int testset_size;
    int testing;
    

    int return_cand;
    int cand_num;
    
    double init_eta, eta_red_rate, max_eta_red;
    double mom;
    double init_reg, reg_red_rate, max_reg_red;
    int max_loops, max_loops_wo_accur_impr, loops_between_val;

    //current state
    TR_STATE st;
    
    char train_file[MAX_NAME];


    int train_dataset_size;
    char test_file[MAX_NAME];
    

    int test_dataset_size;
    char model_name[MAX_NAME];


    //a file to write output to
    char out_file[MAX_NAME];

    //specification file (output/input specification file)
    char io_spec_file[MAX_NAME];
    
    
    char ih_links_spec_file[MAX_NAME];

    //specification of hid-hid links 
    char hh_links_spec_file[MAX_NAME]; 

    
    double lambda;


} APPROX_PARAMS;



typedef struct action_set {
    //set to one if possible
    int acts[ACTION_NUM];
} ACTION_SET;

#define PL_STARTED "STARTED"
#define PL_TRAIN_LOOP_END "TRAIN_LOOP_END"
#define PL_VAL_END "VAL_END"
#define PL_FINISHED "FINISHED"

//METHODS

double sigmoid(double x);

int  read_ih_links_spec(char *fname, MODEL_PARAMS *mp, IH_LINK_SPEC *specs, int *num);
int read_hh_links_spec(char *fname, MODEL_PARAMS *mp);

/// Load word Embedding file
void load_WE(APPROX_PARAMS *ap, MODEL_PARAMS *mp, char* fname);

//reads IO specification file
void read_io_spec(char *fname, MODEL_PARAMS *mp);

//allocates memory space for all the weights
void allocate_weights(APPROX_PARAMS *ap, MODEL_PARAMS *mp);
void free_weights(MODEL_PARAMS *mp, APPROX_PARAMS *ap);

void load_weights(APPROX_PARAMS *ap, MODEL_PARAMS *mp, char* fname);
void save_weights(APPROX_PARAMS *ap, MODEL_PARAMS *mp, char* fname);
void load_weight_dels(APPROX_PARAMS *ap, MODEL_PARAMS *mp, char* fname);
void save_weight_dels(APPROX_PARAMS *ap, MODEL_PARAMS *mp, char* fname);
void initialize_weights(APPROX_PARAMS *ap, MODEL_PARAMS *mp, char* WEfile, int WE);

char *print_sent(SENTENCE *sent, int with_links);
void append_sentence(char *fname, SENTENCE *sen, int with_links);
SENTENCE * read_sentence(MODEL_PARAMS *mp, FILE *fp, int with_links);

void save_sentence(FILE *fp, SENTENCE *sen, int with_links);
void save_candidates(APPROX_PARAMS *ap, MODEL_PARAMS *mp, SENTENCE *inp_sent, STATE **best_states, int avail_num);
SENTENCE *create_blind_sent(SENTENCE *gld_sent);


int get_matched(SENTENCE *sent1, SENTENCE *sent2, int labeled);


char *get_params_string(APPROX_PARAMS *ap);
APPROX_PARAMS *read_params(char *fname);

void print_state(char *s, APPROX_PARAMS *ap);
void save_state(APPROX_PARAMS *ap);
int load_state(APPROX_PARAMS *ap);

void err_chk(void *p, char* msg);
char *field_missing_wrapper(char* s, char *name);


int st_look(STACK *st, int i);
int st_peek(STACK *st);
int st_pop(STACK *st);
void st_push(STACK *st, int x);


PART_TREE *pt_alloc(int sent_len);
PART_TREE *pt_clone(PART_TREE *pt);
void pt_free(PART_TREE **pt);
int pt_lc(PART_TREE *pt, int i);
int pt_llc(PART_TREE *pt, int i);
int pt_rc(PART_TREE *pt, int i);
int pt_rrc(PART_TREE *pt, int i);
int pt_ls(PART_TREE *pt, int i);
int pt_rs(PART_TREE *pt, int i);
void pt_add_link(PART_TREE *pt, int h, int d, int deprel);
void pt_fill_sentence(PART_TREE *pt, MODEL_PARAMS *mp, SENTENCE *sent);

#define pt_head(pt, i)  (pt)->nodes[i].head
#define pt_deprel(pt,i) (pt)->nodes[i].deprel
STATE* get_seq_list_fromlast(APPROX_PARAMS *ap, MODEL_PARAMS *mp, SENTENCE *sent, int do_free_pt, STATE* last_a_state);
STATE *root_prediction(APPROX_PARAMS *ap, MODEL_PARAMS *mp, SENTENCE *sent);

//processes sentence and creates derivations
//returns tail of the derivation
STATE *get_oracle_seq(APPROX_PARAMS *ap, MODEL_PARAMS *mp, SENTENCE *sent, int do_free_pt);//, FILE* fp
//the same but puts the states in the list
STATE *get_seq_list(APPROX_PARAMS *ap, MODEL_PARAMS *mp, SENTENCE *sent,
        STATE_LIST **der_first, STATE_LIST **der_last, int do_free_pt);//, FILE* fp

STATE *create_state(int previous_act, int previous_output, STATE *previous_state, int period, STACK *stack,
        int queue, PART_TREE *pt, OUT_LINK *out_link, int out_num);
void set_links(APPROX_PARAMS *ap, MODEL_PARAMS *mp, STATE *a_state, SENTENCE *sent);//, FILE* fp
ACTION_SET get_next_actions(APPROX_PARAMS *ap, MODEL_PARAMS *mp, STATE *a_state);
void set_mask(MODEL_PARAMS *mp, STATE *a_state, ACTION_SET *as);

int get_word_by_spec_ih(STATE *a_state, IH_LINK_SPEC *spec);
int get_prev_deprel(MODEL_PARAMS *mp, STATE *a_state);


STATE_LIST *increase_list(STATE_LIST *prev_node, STATE *a_state);
STATE_LIST *increase_list_asc(STATE_LIST *tail, STATE *a_state);
STATE_LIST *reduce_list(STATE_LIST *last_node);
void free_state_list(STATE_LIST *last);
void free_state(STATE *a_state);
void free_states(STATE *last_a_state);
void free_derivation(STATE_LIST *last);
STATE_LIST *concat(STATE_LIST *ol1, STATE_LIST *ol2);
int prune_free(STATE **best_states, STATE_LIST *tail, int beam);
int Chunk_Beam_Search(APPROX_PARAMS *ap, MODEL_PARAMS *mp, SENTENCE *sent, STATE **prev_b_states, int prev_cand_num, STATE **new_b_states);
void read_sentence_train(MODEL_PARAMS *mp, FILE *fp, int with_links, int c) ;
void read_sentence_test(MODEL_PARAMS *mp, FILE *fp, int with_links, int c) ;
double testing(APPROX_PARAMS *p, MODEL_PARAMS *w);
double training(APPROX_PARAMS *p, MODEL_PARAMS *w);
void validation_training(APPROX_PARAMS *ap, MODEL_PARAMS *mp, char* WEfile);

double comp_sentence(APPROX_PARAMS *ap, MODEL_PARAMS *w, STATE_LIST *head);
void comp_err(APPROX_PARAMS *ap, MODEL_PARAMS *w, STATE_LIST *der_last, int positive);
void update_weights(APPROX_PARAMS *ap, MODEL_PARAMS *w, int WEfixed);
void comp_state_states(APPROX_PARAMS *ap, MODEL_PARAMS *w, STATE *a_state);
void comp_state_outputs(APPROX_PARAMS *ap, MODEL_PARAMS *w, STATE *a_state);
void comp_state(APPROX_PARAMS *ap, MODEL_PARAMS *w, STATE *a_state);
void fill_lprob(STATE *a_state);


double parse_sentence(APPROX_PARAMS *ap, MODEL_PARAMS *mp, SENTENCE *sent);//, FILE* fp

void rewrite_parameters(APPROX_PARAMS *ap, int argc, char** argv, int start_idx); 

#endif
