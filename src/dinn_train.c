/*
    Majid Yazdani and James Henderson

	Incremental Recurrent Neural Network Dependency Parser with Search-based Discriminative Training , CoNLL 2015
*/

#include "dinn.h"

double sigmoid(double x) {
    return 1. / (1 + exp(-x));
}



void comp_output_parts(APPROX_PARAMS *ap, STATE *a_state, double *hidden, double *hidden_mask) {

    ASSERT(a_state->outputs.num >= 0);
    
    int out, i;

    a_state->outputs.norm = 0;
    if (a_state->outputs.is_masked) {
        for (out = 0; out < a_state->outputs.num-1; out++) {
            if (!a_state->outputs.mask[out]) {
                a_state->outputs.parts[out] = - MAXDOUBLE;
                a_state->outputs.exp[out] = 0;
                continue;
            }
            a_state->outputs.parts[out] = a_state->out_link->b[out];
            for (i = 0; i < ap->hid_size; i++) {
            	if(WITHdrop>0 && ap->testing ==0 && hidden_mask==0)
            		continue;
                a_state->outputs.parts[out]  +=  a_state->out_link->w[out][i] * hidden[i];
            }
            a_state->outputs.exp[out] = exp(a_state->outputs.parts[out]);
            a_state->outputs.norm += a_state->outputs.exp[out];
        }
        
        if (!a_state->outputs.mask[a_state->outputs.num-1]) {
			a_state->outputs.parts[a_state->outputs.num-1] = - MAXDOUBLE;
			a_state->outputs.exp[a_state->outputs.num-1] = 0;

		}
        else{
        	a_state->outputs.parts[a_state->outputs.num-1] = a_state->out_link->b[a_state->outputs.num-1];
			for (i = 0; i < ap->hid_size; i++) {
				if(WITHdrop>0 && ap->testing ==0 && hidden_mask==0)
				            		continue;
				a_state->outputs.parts[a_state->outputs.num-1]  +=  a_state->out_link->w[a_state->outputs.num-1][i] * hidden[i];
			}
			a_state->outputs.exp[out] = exp(a_state->outputs.parts[out]);

        }


    } else {
        for (out = 0; out < a_state->outputs.num-1; out++) {
            a_state->outputs.parts[out] = a_state->out_link->b[out];
            for (i = 0; i < ap->hid_size; i++) {
            	if(WITHdrop>0 && ap->testing ==0 && hidden_mask==0)
            	            		continue;
                a_state->outputs.parts[out]  +=  a_state->out_link->w[out][i] * hidden[i];
            }
            a_state->outputs.exp[out] = exp(a_state->outputs.parts[out]);
            a_state->outputs.norm += a_state->outputs.exp[out];
        }
        a_state->outputs.parts[a_state->outputs.num-1] = a_state->out_link->b[a_state->outputs.num-1];
		for (i = 0; i < ap->hid_size; i++) {
			if(WITHdrop>0 && ap->testing ==0 && hidden_mask==0)
			            		continue;
			a_state->outputs.parts[a_state->outputs.num-1]  +=  a_state->out_link->w[a_state->outputs.num-1][i] * hidden[i];
		}
		a_state->outputs.exp[a_state->outputs.num-1] = exp(a_state->outputs.parts[a_state->outputs.num-1]);

    }

   // printf("comp out done\n");
}



void fill_lprob(STATE *a_state) {

	//// log prob. of the partial parse sequence, correctness probability at every shift

    if (a_state->previous_state != NULL) {
        STATE *prev_a_state = a_state->previous_state;
        a_state->lprob = prev_a_state->lprob + log(prev_a_state->outputs.q[a_state->previous_out]);
        if(a_state->previous_action == SHIFT)
        	a_state->lprob += log(prev_a_state->outputs.q[MAX_OUT-1]);//
    } else {
        a_state->lprob = 0.;
    }
}

void comp_un_hidden(APPROX_PARAMS *ap, MODEL_PARAMS *w, STATE *a_state) {

    int i, j, l;
    
    for (i = 0; i < ap->hid_size; i++) {
        a_state->un_hidden[i] = 0;
        double drop = rand()%2;

        a_state->int_layer.hidden_mask[i]=drop;

        if(WITHdrop >0 && ap->testing==0 && drop==0){
        			continue;
        }


        for (l = 0;  l < a_state->input_num; l++) {
        	a_state->un_hidden[i] += a_state->inp_biases[l]->b[i];
        }


        for (l = 0;  l < a_state->i_num; l++) {

        	if(a_state->composed[l] == 0){
        		for (j = 0; j < ap->emb_size; j++) {
        		    a_state->un_hidden[i] += a_state->eh_biases[l]->w[i][j] * a_state->i_biases[l]->b[j];
				}
        	}
        	else{
        		a_state->un_hidden[i] += a_state->composed_biases[l]->b[i];
        	}



        }

        if(WITHHH>0){ ///////////// H->H in RNN
        	for (l = 0; l < a_state->rel_num; l++) {
				for (j = 0; j < ap->hid_size; j++) {
					if(WITHdrop >0 && ap->testing==0 && a_state->rel_layers[l]->hidden_mask[j]==0)
						continue;
					a_state->un_hidden[i] += a_state->rel_weights[l]->w[i][j] * a_state->rel_layers[l]->hidden[j];
				}

			}
        }

    }

//printf("comp done\n");

}



void comp_state_states(APPROX_PARAMS *ap, MODEL_PARAMS *w, STATE *a_state)
{
    int i;


	if (a_state->previous_state != NULL) {
		memcpy(&a_state->prev_layer, &a_state->previous_state->int_layer, sizeof(LAYER));
	}

	comp_un_hidden(ap, w, a_state);

	//forward initialization
	for (i = 0; i < ap->hid_size; i++) {

		a_state->int_layer.hidden[i] = sigmoid(a_state->un_hidden[i]);
		if(WITHdrop>0 && ap->testing==0){
			a_state->int_layer.hidden[i] *= a_state->int_layer.hidden_mask[i];
		}
		if(WITHdrop > 0 && ap->testing>0){
			a_state->int_layer.hidden[i] *=0.5;
		}

	}



}
//norm and exp  fields of outputs should be precomputed
void comp_output_distr_from_parts(STATE *a_state) {
    int out;
    for (out = 0; out < a_state->outputs.num-1; out++) {
        a_state->outputs.q[out] = a_state->outputs.exp[out] / (a_state->outputs.norm);
    }
    //// correctness probability
    a_state->outputs.q[a_state->outputs.num-1] = sigmoid(a_state->outputs.parts[a_state->outputs.num-1]);
}

void comp_state_outputs(APPROX_PARAMS *ap, MODEL_PARAMS *w, STATE *a_state) {
    comp_output_parts(ap, a_state,  a_state->int_layer.hidden, a_state->int_layer.hidden_mask);
    comp_output_distr_from_parts(a_state);
}

void comp_state(APPROX_PARAMS *ap, MODEL_PARAMS *w, STATE *a_state) {
    fill_lprob(a_state);
    comp_state_states(ap, w, a_state);
    comp_state_outputs(ap, w, a_state);
}


/////////// forward computation
double comp_sentence(APPROX_PARAMS *ap, MODEL_PARAMS *w, STATE_LIST *head) {


    STATE_LIST *curr_node = head;
    STATE *curr_a_state = NULL;

    while(curr_node->next->next != NULL) {
        curr_a_state = curr_node->elem;

        fill_lprob(curr_a_state);

        comp_state_states(ap, w, curr_a_state);

        comp_state_outputs(ap, w, curr_a_state);

        curr_node = curr_node->next;
    }

    curr_a_state = curr_node->elem;
    fill_lprob(curr_a_state);

    return curr_a_state->lprob;
}


void comp_out_local_err(APPROX_PARAMS *ap, STATE *a_state, int corr_out, int positive) {
    
    double *hidden = a_state->int_layer.hidden;
    double *hidden_err = a_state->int_layer.hidden_err;

    int out;


    if(positive >0){

    	////////// sfotmax
    	for (out = 0; out < a_state->outputs.num-1; out++) {

    			a_state->outputs.del[out] = - a_state->outputs.q[out];

    	}
    	a_state->outputs.del[corr_out] += 1.;

    	/////////// correctness
    	if(a_state->previous_action==SHIFT)
					a_state->outputs.del[MAX_OUT-1] += ap->lambda*(1-a_state->outputs.q[MAX_OUT-1])*a_state->outputs.q[MAX_OUT-1];


    }
    else{

    	///// correctness
		if(a_state->previous_action==SHIFT)
					a_state->outputs.del[MAX_OUT-1] -= ap->lambda*(1-a_state->outputs.q[MAX_OUT-1])*a_state->outputs.q[MAX_OUT-1];


  	}




    //////////// err to hidden
    for (out = 0; out < a_state->outputs.num; out++) {
        a_state->out_link->i_b_del[out] += a_state->outputs.del[out];
        int j;
        for (j = 0; j < ap->hid_size; j++) {
        	if(WITHdrop >0 && ap->testing==0 && a_state->int_layer.hidden_mask[j]==0)
        		continue;
            a_state->out_link->i_w_del[out][j] += a_state->outputs.del[out] * hidden[j];
           	hidden_err[j] += a_state->outputs.del[out] * a_state->out_link->w[out][j];
        }
    }


}


//hidden_err error for this state to distribute to input links
void comp_linkerr(APPROX_PARAMS *ap, STATE *first_a_state, double *hidden, double* hidden_mask, double *hidden_err) {


    int i, j, l;

    for (l = 0;  l < first_a_state->i_num; l++) {
    	if(first_a_state->composed[l]==0){
			first_a_state->i_biases[l]->occured++;
			first_a_state->eh_biases[l]->occured++;
    	}
    	else{
    		first_a_state->composed_biases[l]->occured++;
    	}
    }
    for (l = 0;  l < first_a_state->input_num; l++)
    	first_a_state->inp_biases[l]->occured++;

    for (l = 0; l < first_a_state->rel_num; l++) {
    	first_a_state->rel_weights[l]->occured++;
    }


    for (i = 0; i < ap->hid_size; i++) {
    	if(WITHdrop >0 && ap->testing==0 && hidden_mask[i]==0)
    		continue;
        double const_del = hidden_err[i] * hidden[i] * (1 - hidden[i]);
        for (l = 0;  l < first_a_state->input_num; l++) {
        	double *i_b_del = first_a_state->inp_biases[l]->i_b_del;
        	i_b_del[i] += const_del;


        }

        for (l = 0;  l < first_a_state->i_num; l++) {

        	if(first_a_state->composed[l ]==0){

        		double *i_b_del = first_a_state->i_biases[l]->i_b_del;
				double *rel_mju = first_a_state->i_biases[l]->b;
				//first_a_state->i_biases[l]->occured++;
				double *i_w_del = first_a_state->eh_biases[l]->i_w_del[i];
				double *w = first_a_state->eh_biases[l]->w[i];

				for (j = 0; j < ap->emb_size; j++) {
					i_w_del[j] += const_del * rel_mju[j];
					i_b_del[j] += const_del* w[j];
				}

        	}
        	else{

        		//first_a_state->composed_biases[l]->occured++;
        		double *i_b_del = first_a_state->composed_biases[l]->i_b_del;
        		i_b_del[i] += const_del;
        	}
        }
        if(WITHHH>0){
        	for (l = 0; l < first_a_state->rel_num; l++) {

				double *i_w_del = first_a_state->rel_weights[l]->i_w_del[i];
				double *rel_mju = first_a_state->rel_layers[l]->hidden;

				for (j = 0; j < ap->hid_size; j++) {
					if(WITHdrop >0 && ap->testing==0 && first_a_state->rel_layers[l]->hidden_mask[j]==0)
					        				continue;
					i_w_del[j] += const_del * rel_mju[j];
				}
			}
        }




    } //for i
   // printf("error links done\n");
}

//hidden_err error for this state to distribute
void comp_hidden_err(APPROX_PARAMS *ap, STATE *first_a_state, double *hidden, double *hidden_mask, double *hidden_err) {

    int i, j, l;
    for (l = 0; l < first_a_state->rel_num; l++) {
    	first_a_state->rel_weights[l]->occured++;
    }
    for (l = 0; l < first_a_state->rel_num; l++) {

        double *rel_hidden_err = first_a_state->rel_layers[l]->hidden_err;
        HH_LINK *rel_weight = first_a_state->rel_weights[l];
        for (j = 0; j < ap->hid_size; j++) {
        	if(WITHdrop >0 && ap->testing==0 && hidden_mask[j]==0)
					continue;
            double del_j = hidden_err[j] *  hidden[j] * (1 - hidden[j]);
            double *rel_weight_j = rel_weight->w[j];
            for (i = 0; i < ap->hid_size; i++) {
            	if(WITHdrop >0 && ap->testing==0 && first_a_state->rel_layers[l]->hidden_mask[i]==0)
            	    continue;
                rel_hidden_err[i] += del_j * rel_weight_j[i];
            }
        }        
    }
    
  //  printf("err H done\n");

}



void update_weights(APPROX_PARAMS *ap, MODEL_PARAMS *mp, int WEfixed)
{


	//////////////////////// SGD with momentum , adagrad doesn't work

    double eta = ap->st.curr_eta;
    double mom = ap->mom;
    double dec = ap->st.decay;
	#define UPDATE_IE_WEIGHTS(ih)\
		{\
			for (h = 0; h < ap->emb_size; h++) { \
				(ih).b[h] += eta * (ih).i_b_del[h];\
				(ih).b[h] *= dec; \
				(ih).i_b_del[h] *= mom;\
			}\
			(ih).occured *= mom;\
		}
	#define UPDATE_IH_WEIGHTS(ih)\
		{\
			for (h = 0; h < ap->hid_size; h++) { \
				(ih).b[h] += eta * (ih).i_b_del[h];\
				(ih).b[h] *= dec; \
				(ih).i_b_del[h] *= mom;\
			}\
			(ih).occured *= mom;\
		}

    //save input links
    int i;
    int l;
    int h;


    for (i = 0; i < mp->deprel_num + 2; i++) {
    	if(mp->deprel[i].occured > OCCURED_MIN){
               UPDATE_IE_WEIGHTS(mp->deprel[i]);
    	}
    	else{
    		mp->deprel[i].occured=0;
    	}
    }
    for (i = 0; i < mp->pos_info_in.num + 1; i++) {
    	if(mp->pos[i].occured > OCCURED_MIN){
            UPDATE_IE_WEIGHTS(mp->pos[i]);
    	}
    	else{
    		mp->pos[i].occured=0;
    	}
	}

    if(WEfixed ==0){
    	for (i = 0; i < mp->word_num + 1; i++) {
    			if(mp->word[i].occured > OCCURED_MIN){
    				 UPDATE_IE_WEIGHTS(mp->word[i]);
    			}
    			else{
    				mp->word[i].occured=0;
    			}
    		 }
    }
    for (i = 0; i < mp->lemma_num + 1; i++) {
		if(mp->lemma[i].occured > OCCURED_MIN){
			 UPDATE_IE_WEIGHTS(mp->word[i]);
		}
		else{
			mp->lemma[i].occured=0;
		}
	 }

    for (i = 0; i < mp->elfeat_num + 1; i++) {
		if(mp->elfeat[i].occured > OCCURED_MIN){
			 UPDATE_IE_WEIGHTS(mp->elfeat[i]);
		}
		else{
			mp->elfeat[i].occured=0;
		}
	 }
    for (i = 0; i < mp->composed_feat_num + 1; i++) {
    	if(mp->composed_feat[i].occured > OCCURED_MIN){
                UPDATE_IH_WEIGHTS(mp->composed_feat[i]);
    	}
    	else{
    		mp->composed_feat[i].occured=0;
    	}
    }




    int h1, h2;
    for (l = 0; l < mp->ih_links_num; l++) {

    	if( mp->eh_link_ih[l].occured > OCCURED_MIN){
    		for (h1 = 0; h1 < ap->hid_size; h1++) {
				for (h2 = 0; h2 < ap->emb_size; h2++) {



					mp->eh_link_ih[l].w[h1][h2] += eta * mp->eh_link_ih[l].i_w_del[h1][h2];
					mp->eh_link_ih[l].w[h1][h2] *= dec;
					mp->eh_link_ih[l].i_w_del[h1][h2] *= mom;


				}
			}
    		mp->eh_link_ih[l].occured *= mom;

    	}
    	else{
    		mp->eh_link_ih[l].occured  = 0;
    	}

	}


    UPDATE_IH_WEIGHTS(mp->init_bias);
    UPDATE_IH_WEIGHTS(mp->hid_bias);

    int a;
    for (a = 0; a < ACTION_NUM; a++) {
    	if(mp->prev_act_bias[a].occured > OCCURED_MIN){
    		UPDATE_IH_WEIGHTS(mp->prev_act_bias[a]);
    	}
    	else{
    		mp->prev_act_bias[a].occured = 0;
    	}
    }

    for (a = 0; a < ACTION_NUM; a++) {
        int d;
        for (d = 0; d < mp->deprel_num; d++) {
        	if(mp->prev_deprel_bias[a][d].occured> OCCURED_MIN){
        		UPDATE_IH_WEIGHTS(mp->prev_deprel_bias[a][d]);
        	}
        	else{
        		mp->prev_deprel_bias[a][d].occured = 0;
        	}
        }
    }

    // HID-HID weight matrixes
    if(WITHHH){
    	for (l = 0; l < mp->hh_links_num; l++) {

			for (h1 = 0; h1 < ap->hid_size; h1++) {
				for (h2 = 0; h2 < ap->hid_size; h2++) {
					mp->hh_link[l].w[h1][h2] += eta * mp->hh_link[l].i_w_del[h1][h2];
					mp->hh_link[l].w[h1][h2] *= dec;
					mp->hh_link[l].i_w_del[h1][h2] *= mom;

				}
			}
			mp->hh_link[l].occured *= mom;

		}

    }


    //output weights
#define UPDATE_OUT_LINK(out_link)\
    {\
        (out_link).b[t] += eta * (out_link).i_b_del[t];\
        (out_link).b[t] *= dec; \
        (out_link).i_b_del[t] *= mom; \
        for (i = 0; i < ap->hid_size; i++) { \
            (out_link).w[t][i] += eta * (out_link).i_w_del[t][i];\
            (out_link).w[t][i] *= dec; \
            (out_link).i_w_del[t][i]  *= mom; \
        }\
    }

    int t;
    for (t = 0; t < MAX_OUT; t++) {
        UPDATE_OUT_LINK(mp->out_link);
    }




}



//////////  error backward computations

void comp_err(APPROX_PARAMS *ap, MODEL_PARAMS *w, STATE_LIST *der_last, int positive) {


	STATE_LIST  *curr_node = der_last->prev->prev;
	int corr_out = der_last->prev->elem->previous_out;
	ASSERT(corr_out >= 0);

	while (curr_node != NULL) {
		STATE *a_state = curr_node->elem;

		comp_output_parts(ap, a_state, a_state->int_layer.hidden, a_state->int_layer.hidden_mask);

		/// error from one local decision in sequence

		comp_out_local_err(ap, a_state, corr_out, positive);



		STATE *first_a_state = a_state;

		comp_linkerr(ap, first_a_state, a_state->int_layer.hidden,a_state->int_layer.hidden_mask, a_state->int_layer.hidden_err);

		//update error for the previous layers
		if(WITHHH>0){
			comp_hidden_err(ap, first_a_state, a_state->int_layer.hidden,a_state->int_layer.hidden_mask, a_state->int_layer.hidden_err);
		}

		if (a_state->previous_state != NULL) {
			//first a_state in the previous slice (in the one we actually consider here)
			STATE *first_a_state = a_state->previous_state;
			//it was analytically computed  - errors in respect to rho-rho weights

			comp_linkerr(ap, first_a_state, a_state->prev_layer.hidden,a_state->prev_layer.hidden_mask,a_state->prev_layer.hidden_err);

			//update error for the previous layers
			if(WITHHH>0)
				comp_hidden_err(ap, first_a_state, a_state->prev_layer.hidden,a_state->prev_layer.hidden_mask, a_state->prev_layer.hidden_err);

		}







		corr_out = a_state->previous_out;
		curr_node = curr_node->prev;
	}

}

