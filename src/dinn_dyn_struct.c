/*
    Majid Yazdani and James Henderson

	Incremental Recurrent Neural Network Dependency Parser with Search-based Discriminative Training , CoNLL 2015
*/

#include "dinn.h"

//*****************************************************
// Operations with dynamic structure of states
//****************************************************


STATE_LIST *increase_list(STATE_LIST *prev_node, STATE *a_state) {
    DEF_ALLOC(next_node, STATE_LIST);
    next_node->prev = prev_node;
    ASSERT((prev_node == NULL && a_state == NULL) || (prev_node != NULL && a_state != NULL))
    if (prev_node != NULL) {
        prev_node->next = next_node;
        prev_node->elem = a_state;
    }
    return next_node;
}

//adds an element to the sorted list (list is sorted in ascending order of the lprob)
STATE_LIST *increase_list_asc(STATE_LIST *tail, STATE *a_state) {
    ASSERT(tail != NULL && a_state != NULL);
    DEF_ALLOC(new_node, STATE_LIST);
    new_node->elem = a_state;
    
    STATE_LIST *curr_node = tail;
    while (curr_node->prev != NULL) {
        if (curr_node->prev->elem->lprob <= a_state->lprob) {
            STATE_LIST *prev_node = curr_node->prev;
            new_node->next = prev_node->next;
            new_node->prev = prev_node;
            new_node->next->prev = new_node;
            new_node->prev->next = new_node;
            break;
        }
        curr_node = curr_node->prev;
    }
    //it is the smallest element in the list
    if (curr_node->prev == NULL) {
        curr_node->prev = new_node;
        new_node->next = curr_node;
    }
    return tail;
}

STATE_LIST *reduce_list(STATE_LIST *last_node) {
    STATE_LIST *prev_node = last_node->prev;
    prev_node->elem = NULL;
    free(last_node);
    return prev_node;
}

void free_state_list(STATE_LIST *last) {
    if (last == NULL) {
        return;
    }
    STATE_LIST *prev = NULL, *curr = last;
    while(curr != NULL) {
        prev = curr->prev;
        free(curr);
        curr = prev;
    }
}

void free_state(STATE *a_state) {
    ASSERT(a_state != NULL);
    free_state_list(a_state->next_states);
    if (a_state->previous_state != NULL) {
        STATE_LIST *node = a_state->previous_state->next_states->prev;
        int has_deleted = 0;
        while (node != NULL) {
            if (node->elem == a_state) {
                node->next->prev = node->prev;
                if (node->prev != NULL) {
                    node->prev->next = node->next;
                }
                free(node);
                has_deleted = 1;
                break;
            }
            node = node->prev;
        }
        ASSERT(has_deleted)
    }
    if (a_state->pt != NULL) {
        pt_free(&(a_state->pt));
    }
    
    free(a_state);
}

void free_states(STATE *last_a_state) {
    ASSERT(last_a_state != NULL);
    STATE *a_state = last_a_state, *prev_a_state = NULL;
    while(a_state !=  NULL) {
        prev_a_state = a_state->previous_state;
        free_state(a_state);
        

        if (prev_a_state != NULL) {
            //some next states are still left (e.g. a derivation tree is considered and one branch is freed)
            if (prev_a_state->next_states->prev != NULL) {
               break; 
            }
        }
        a_state = prev_a_state;
    }
}

void free_derivation(STATE_LIST *last) {
    ASSERT(last->prev != NULL);
    ASSERT(last->prev->elem != NULL);
    STATE *last_a_state = last->prev->elem;
    free_states(last_a_state);
    free_state_list(last);
}

STATE_LIST *concat(STATE_LIST *ol1, STATE_LIST *ol2)  {
    ASSERT(ol1 != NULL && ol2 != NULL && ol2->elem == NULL);
    STATE_LIST *node = ol1;

    while (node->prev != NULL) {
        node = node->prev;
    }
    node->prev = ol2->prev;
    if (ol2->prev != NULL) {
        ol2->prev->next = node;
    }
    free(ol2);
    return ol1;
}

int prune_free(STATE **best_states, STATE_LIST *tail, int beam) {
    int i;
    for (i = 0; i < beam; i++) {
        STATE_LIST *node = tail->prev;
        STATE_LIST *best_node = NULL;
        double max_lprob = - MAXDOUBLE;
        while (node != NULL) {
            //already in the list
            if (node->elem == NULL) {
                node = node->prev;
                continue;
            }
            if (max_lprob < node->elem->lprob) {
                max_lprob = node->elem->lprob;
                best_node = node;
            }
            node = node->prev;
        }
        //empty list
        if (best_node == NULL) {
            break;
        }
        best_states[i] = best_node->elem;
        best_node->elem = NULL;
    }

    STATE_LIST *node = tail->prev;

    while (node != NULL) {
        if (node->elem != NULL) {
            free_states(node->elem);
        }
        node = node->prev;
    }
    free_state_list(tail);

    return i;
}



