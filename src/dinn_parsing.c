/*
    Majid Yazdani and James Henderson

	Incremental Recurrent Neural Network Dependency Parser with Search-based Discriminative Training , CoNLL 2015
*/
#include "dinn.h"



ACTION_SET get_next_actions(APPROX_PARAMS *ap, MODEL_PARAMS *mp, STATE *a_state) {
    ACTION_SET as; 

    int s = st_peek(&a_state->stack);
    ASSERT(a_state->queue != a_state->pt->len + 1);

    //RA is always followed by SHIFT
    if (a_state->previous_action == RA) {
        as.acts[RED] = 0;
        as.acts[LA] = 0;
        as.acts[RA] = 0;
        as.acts[SHIFT] = 1;
    } else {
        //empty stack
        if (s == 0) {


            as.acts[RED] = 0;
            as.acts[LA] = 0;

            //allows to attach to root
            as.acts[RA] = 1;

            as.acts[SHIFT] = 1;
        } else {

        	//we allow to reduce elemenets attached to root from the stack

            as.acts[RED] = 1;

            //if head is not assigned to s
            if (a_state->pt->nodes[s].head == 0 && a_state->pt->nodes[s].deprel == ROOT_DEPREL) {
                as.acts[LA] = 1;
            } else {
                as.acts[LA] = 0;
            }   
            as.acts[RA] = 1;
            as.acts[SHIFT] = 1;
        }
    } 
    return as;
}



STATE *create_state(int previous_act, int previous_output, STATE *previous_state, int period, STACK *stack,
        int queue, PART_TREE *pt, OUT_LINK *out_link, int out_num) {
    DEF_ALLOC(a_state, STATE);

    a_state->previous_action = previous_act;
    a_state->previous_out = previous_output;
    a_state->previous_state = previous_state;
    a_state->period = period;
    memcpy(&a_state->stack, stack, sizeof(STACK));
    a_state->queue = queue;
    a_state->pt = pt;

    a_state->next_states = increase_list(NULL, NULL);
    if  (previous_state != NULL) {
        previous_state->next_states
            = increase_list(previous_state->next_states, a_state);
    
    }
    
    a_state->out_link = out_link;
    a_state->outputs.num = out_num;

    return a_state;
}
    

int check_pt_t_equality(PART_TREE *pt, SENTENCE *sent) {
    int w;
    for (w = 1; w <= sent->len; w++) {
        if (sent->head[w] != pt->nodes[w].head) {
            return 0;
        }
        if (sent->deprel[w] != pt->nodes[w].deprel) {
            return 0;
        }
    }
    return 1;
}

//check if it is possible to shift an element (if its left part is built)
int left_part_complete(PART_TREE *pt, SENTENCE *sent, int q) {
    //head is not found 
    if (pt_head(pt, q) != sent->head[q] && sent->head[q] < q) {
        return 0;
    }
    
    int w;
    for (w = 1; w < q; w++) {
        if (sent->head[w] == q) {
            //check if it is already in the list of its children
            if (pt->nodes[w].head != q) {
                return 0;
            }
        }
    }
    return 1;
}
int everything_complete(PART_TREE *pt, SENTENCE *sent, int s) { 
    //head is not found 
    if (pt_head(pt, s) != sent->head[s]) {//&& sent->head[s] < s) {
        return 0;
    }

    int w;
    for (w = 1; w <= sent->len; w++) {
        if (sent->head[w] == s) {
            //check if it is already in the list of its children
            if (pt->nodes[w].head != s) {
                return 0;
            }
        }
    }
    return 1;
}    

int get_word_by_spec_hh(STATE *a_state, HH_LINK_SPEC *spec) {
    PART_TREE *pt = a_state->pt;
    ASSERT(a_state->pt != NULL);
    int w = -1;
    
    ASSERT(spec->orig_offset >= 0);
    if (spec->orig_src == INP_SRC) {
        w = a_state->queue + spec->orig_offset;
    } else if (spec->orig_src == STACK_SRC) {
        w = st_look(&a_state->stack, spec->orig_offset);
    } else {
        ASSERT(0);
    }
    if (w <= 0 || w > pt->len) {
        return 0;
    }
    
    int h;
    ASSERT(spec->orig_head >= 0);
    for (h = 0; h < spec->orig_head; h++) {
        w = pt_head(pt, w);

        if (w == 0) {
            return 0;
        }
    }
    
    if (spec->orig_rrc > 0) {
        int c;
        for (c = 0; c < spec->orig_rrc; c++) {
            w = pt_rrc(pt, w);
            if (w == 0) {
                return 0;
            }
        }
    } else {
        int c;
        for (c = 0; c < - spec->orig_rrc; c++) {
            w = pt_llc(pt, w);
            if (w == 0) {
                return 0;
            }
        }
    }
    
    if (spec->orig_rs > 0) {
        int c;
        for (c = 0; c < spec->orig_rs; c++) {
            w = pt_rs(pt, w);
            if (w == 0) {
                return 0;
            }
        }
    } else {
        int c;
        for (c = 0; c < - spec->orig_rs; c++) {
            w = pt_ls(pt, w);
            if (w == 0) {
                return 0;
            }
        }
    }

    return w;
}


//return word or 0
int get_word_by_spec_ih(STATE *a_state, IH_LINK_SPEC *spec) {
    PART_TREE *pt = a_state->pt;
    ASSERT(a_state->pt != NULL);
    int w = -1;
    
    ASSERT(spec->offset_curr >= 0);
    if (spec->src == INP_SRC) {
        w = a_state->queue + spec->offset_curr;
    } else if (spec->src == STACK_SRC) {
        w = st_look(&a_state->stack, spec->offset_curr);
    } else {
        ASSERT(0);
    }
    if (w <= 0 || w > pt->len) {
        return 0;
    }
    
    w += spec->offset_orig;
    
    if (w <= 0 || w > pt->len) {
        return 0;
    }

    int h;
    ASSERT(spec->head >= 0);
    for (h = 0; h < spec->head; h++) {
        w = pt_head(pt, w);

        if (w == 0) {
            return 0;
        }
    }
    
    if (spec->rc > 0) {
        int c;
        for (c = 0; c < spec->rc; c++) {
            w = pt_rc(pt, w);
            if (w == 0) {
                return 0;
            }
        }
    } else {
        int c;
        for (c = 0; c < - spec->rc; c++) {
            w = pt_lc(pt, w);
            if (w == 0) {
                return 0;
            }
        }
    }
    
    if (spec->rs > 0) {
        int c;
        for (c = 0; c < spec->rs; c++) {
            w = pt_rs(pt, w);
            if (w == 0) {
                return 0;
            }
        }
    } else {
        int c;
        for (c = 0; c < - spec->rs; c++) {
            w = pt_ls(pt, w);
            if (w == 0) {
                return 0;
            }
        }
    }

    return w;
}

int get_prev_deprel(MODEL_PARAMS *mp, STATE *a_state) {

    int deprel=-1;
    if (a_state->previous_action == LA ) {
        deprel = a_state->previous_out-2;

        ASSERT(deprel >= 0 && deprel < mp->deprel_num);
    }
    if (a_state->previous_action == RA ) {
            deprel = a_state->previous_out-2-MAX_DEPREL_SIZE;
            ASSERT(deprel >= 0 && deprel < mp->deprel_num);
        }

    if(deprel == -1)
    	deprel = 0;
    return deprel;
}
void set_ih_links(APPROX_PARAMS *ap, MODEL_PARAMS *mp, STATE *a_state, SENTENCE *sent) { //FILE* fp

    ASSERT(a_state->i_num == 0);
    ASSERT(a_state->input_num == 0);
    PART_TREE *pt = a_state->pt;

    //create links using specifications
    if (a_state->previous_state == NULL) {
        a_state->inp_biases[a_state->input_num++] = &mp->init_bias;
        return;
    } else {
        a_state->inp_biases[a_state->input_num++] = &mp->hid_bias;
        ASSERT(a_state->previous_action >= 0 && a_state->previous_action < ACTION_NUM);
        a_state->inp_biases[a_state->input_num++] = &mp->prev_act_bias[a_state->previous_action];
        int deprel = get_prev_deprel(mp, a_state);
        a_state->inp_biases[a_state->input_num++] = &mp->prev_deprel_bias[a_state->previous_action][deprel];
    }


    int l;
    for (l = 0; l < mp->ih_links_num; l++) {
        if (a_state->i_num >= MAX_IH_LINKS_OPT) {
            fprintf(stderr, "Too many features in an state, exceeded MAX_IH_LINKS_OPT = %d\n", MAX_IH_LINKS_OPT);
            exit(1);
        }
        IH_LINK_SPEC *spec = &mp->ih_links_specs[l];
        int w = get_word_by_spec_ih(a_state, spec);


        if (spec->type == DEPREL_TYPE) {
            int deprel;
            if (w > 0) {
                deprel = pt->nodes[w].deprel;
                if (deprel == ROOT_DEPREL) {
                    deprel = mp->deprel_num + 1;
                }
            } else {
                deprel = mp->deprel_num;
            }

            if(mp->chosenfeature[a_state->i_num][deprel]<=0 || ap->WITHCACHE==0){
            	a_state->i_biases[a_state->i_num] = &mp->deprel[deprel];
            	a_state->eh_biases[a_state->i_num] = &mp->eh_link_ih[a_state->i_num];
            	a_state->composed[a_state->i_num] = 0;
            	mp->feature_notfound++;

            }
            else{
            	a_state->composed[a_state->i_num] = 1;
            	a_state->composed_biases[a_state->i_num] = &mp->composed_feat[mp->chosenfeature[a_state->i_num][deprel]-1];
            	mp->feature_found++;

            }

            a_state->i_num++;


        } else if (spec->type == POS_TYPE) {
            int pos;
            if (w > 0) {
                pos = sent->pos[w];
            } else {
                pos = mp->pos_info_in.num;
            }


            /////////// Hacky implementation of cached features
            if(mp->chosenfeature[a_state->i_num][pos]<=0 || ap->WITHCACHE==0){
            	a_state->i_biases[a_state->i_num] = &mp->pos[pos];
            	a_state->eh_biases[a_state->i_num] = &mp->eh_link_ih[a_state->i_num];
            	a_state->composed[a_state->i_num] = 0;
            	mp->feature_notfound++;
            }
            else{
            	a_state->composed_biases[a_state->i_num] = &mp->composed_feat[mp->chosenfeature[a_state->i_num][pos]-1];
            	a_state->composed[a_state->i_num] = 1;
            	mp->feature_found++;
            }


            a_state->i_num++;

        }  else if (spec->type == WORD_TYPE) {
                    	int word;
			if(w>0){
				word = sent->word[w];
			}
			else{
				word = mp->word_num;
			}
			if(mp->chosenfeature[a_state->i_num][word]<=0 || ap->WITHCACHE==0){
				a_state->i_biases[a_state->i_num] = &mp->word[word];
				a_state->eh_biases[a_state->i_num] = &mp->eh_link_ih[a_state->i_num];
				a_state->composed[a_state->i_num] = 0;
				mp->feature_notfound++;
			}
			else{
				a_state->composed_biases[a_state->i_num] = &mp->composed_feat[mp->chosenfeature[a_state->i_num][word]-1];
				a_state->composed[a_state->i_num] = 1;
				mp->feature_found++;
			}

            a_state->i_num++;
        } else if (spec->type == LEMMA_TYPE) {
        	int lemma;
			if(w>0){
				lemma= sent->lemma[w];
			}
			else{
				lemma = mp->word_num;
			}
			if(mp->chosenfeature[a_state->i_num][lemma]<=0 || ap->WITHCACHE==0){
				a_state->i_biases[a_state->i_num] = &mp->word[lemma];
				a_state->eh_biases[a_state->i_num] = &mp->eh_link_ih[a_state->i_num];
				a_state->composed[a_state->i_num] = 0;
				mp->feature_notfound++;
			}
			else{
				a_state->composed_biases[a_state->i_num] = &mp->composed_feat[mp->chosenfeature[a_state->i_num][lemma]-1];
				a_state->composed[a_state->i_num] = 1;
				mp->feature_found++;
			}

            a_state->i_num++;
        }else if (spec->type == FEAT_TYPE) {
        	int elfeat;
			if (w > 0) {

				int i;
				for (i = 0; i < sent->elfeat_len[w]; i++) {
					elfeat = sent->elfeat[w][i];

					if(mp->chosenfeature[a_state->i_num][elfeat]<=0 || ap->WITHCACHE==0){
						a_state->i_biases[a_state->i_num] = &mp->elfeat[elfeat];
						a_state->eh_biases[a_state->i_num] = &mp->eh_link_ih[a_state->i_num];
						a_state->composed[a_state->i_num] = 0;
						mp->feature_notfound++;
					}
					else{
						a_state->composed_biases[a_state->i_num] = &mp->composed_feat[mp->chosenfeature[a_state->i_num][elfeat]-1];
						a_state->composed[a_state->i_num] = 1;
						mp->feature_found++;
					}

				}
			} else {
				elfeat = mp->elfeat_num;

				if(mp->chosenfeature[a_state->i_num][elfeat]<=0 || ap->WITHCACHE==0){
					a_state->i_biases[a_state->i_num] = &mp->elfeat[elfeat];
					a_state->eh_biases[a_state->i_num] = &mp->eh_link_ih[a_state->i_num];
					a_state->composed[a_state->i_num] = 0;
					mp->feature_notfound++;
				}
				else{
					a_state->composed_biases[a_state->i_num] = &mp->composed_feat[mp->chosenfeature[a_state->i_num][elfeat]-1];
					a_state->composed[a_state->i_num] = 1;
					mp->feature_found++;
				}
			}
        }
        else {
            ASSERT(0);
        }

    }




}

int act_before(STATE *a_state) {
    ASSERT(a_state->previous_state != NULL);

    STATE *prev_a_state = a_state->previous_state, *after_a_state = a_state;

    ASSERT(prev_a_state != NULL);
    return after_a_state->previous_action;
}

void set_hh_links(APPROX_PARAMS *ap, MODEL_PARAMS *mp, STATE *a_state, SENTENCE *sent) {
    ASSERT(a_state->rel_num  == 0);
    //printf("started\n");
    if (1) {
        //create links using specifications
        int l;
        for (l = 0; l < mp->hh_links_num; l++) {
            HH_LINK_SPEC *spec = &mp->hh_links_specs[l];
            ASSERT(spec->orig_offset >= 0 && spec->target_offset >= 0);

            //find original w       
            int orig_w = -1;
            orig_w = get_word_by_spec_hh(a_state, spec);

            if (orig_w == 0) {
                continue;
            }
            
            LAYER *rel_layer = NULL; 
            STATE *prev_a_state = a_state->previous_state;
            //offset defines how many states we passed (previous state is 1)  
            int offset = 0;
            STATE *after_a_state = a_state;
            if (spec->target_src == INP_SRC) {
                while (prev_a_state != NULL) {
                    //passed new_state

                        offset++;

                    if ( prev_a_state->queue + spec->target_offset == orig_w
                            && (spec->offset < 0 || spec->offset == offset)) {
                        if (spec->act == ANY || spec->act == act_before(after_a_state)) {
                            rel_layer = &after_a_state->prev_layer;
                            if (spec->when == FIND_LAST || spec->offset >= 0) {
                                break;
                            }                             
                        }
                    }

                    after_a_state = prev_a_state;
                    prev_a_state = prev_a_state->previous_state;
                }
            } else if (spec->target_src == STACK_SRC) {
                while (prev_a_state != NULL) {
                    //passed new_state

                        offset++;

                    if ( st_look(&prev_a_state->stack, spec->target_offset) == orig_w
                            && (spec->offset < 0 || spec->offset == offset)) {
                        if (spec->act == ANY || spec->act == act_before(after_a_state)) {
                            rel_layer = &after_a_state->prev_layer;
                            if (spec->when == FIND_LAST || spec->offset >= 0) {
                                break;
                            }                             
                        }
                    }
                    after_a_state = prev_a_state;
                    prev_a_state = prev_a_state->previous_state;
                }
            } else {
                ASSERT(0);
            }
            if (rel_layer != NULL) {
                a_state->rel_layers[a_state->rel_num] = rel_layer;
                a_state->rel_weights[a_state->rel_num] = &mp->hh_link[l];
                a_state->rel_num++;
            }
            
    
        } // l
    } else {
        //copy links from the previous
        memcpy(a_state->rel_weights, a_state->previous_state->rel_weights, sizeof(HH_LINK*) * a_state->previous_state->rel_num);
        memcpy(a_state->rel_layers, a_state->previous_state->rel_layers, sizeof(LAYER*) * a_state->previous_state->rel_num);
        a_state->rel_num = a_state->previous_state->rel_num;
    }

    //printf("ended\n");
}
/*
void set_links(APPROX_PARAMS *ap, MODEL_PARAMS *mp, STATE *a_state, SENTENCE *sent) {

    set_ih_links(ap, mp, a_state, sent);
    set_hh_links(ap, mp, a_state, sent);
}*/
void set_links(APPROX_PARAMS *ap, MODEL_PARAMS *mp, STATE *a_state, SENTENCE *sent) {//, FILE* fp

    set_ih_links(ap, mp, a_state, sent);///, fp
    if(WITHHH>0)
    	set_hh_links(ap, mp, a_state, sent);
}
#define TRY_FREE_PT(x) if (do_free_pt)  pt_free(x);

/*STATE * create_word_prediction_seq(APPROX_PARAMS *ap, MODEL_PARAMS *mp, SENTENCE *sent, STATE *a_state, int do_free_pt) {
    ASSERT(ap->input_offset == 1 || ap->input_offset == 0);
    int pos = sent->pos[a_state->period + ap->input_offset];
    int feat = sent->feat_out[a_state->period + ap->input_offset];
    int word = sent->word_out[a_state->period + ap->input_offset];

    
    STATE *new_a_state = create_state(0, a_state->previous_act, pos, a_state, a_state->period, &a_state->stack, a_state->queue, NULL, &mp->out_link_feat[pos], mp->pos_info_out.feat_infos[pos].num);
    set_links(ap, mp, new_a_state, sent);

    
    new_a_state = create_state(0, a_state->previous_act, feat, new_a_state, a_state->period, &a_state->stack, a_state->queue, NULL, &mp->out_link_word[pos][feat],
            mp->pos_info_out.feat_infos[pos].word_infos[feat].num);
    set_links(ap, mp, new_a_state, sent);

    new_a_state = create_state(1, SHIFT, word, new_a_state, a_state->period + 1, &a_state->stack, a_state->queue, pt_clone(a_state->pt), &mp->out_link_act, ACTION_NUM);

    TRY_FREE_PT(&(a_state->pt));

    st_push(&new_a_state->stack, new_a_state->queue);
    new_a_state->queue++;
    set_links(ap, mp, new_a_state, sent);

    return new_a_state;
}

*/

int does_restrict(ACTION_SET *as) {
    int a;
    for (a = 0; a < ACTION_NUM; a++) {
        if (!as->acts[a]) {
            return 1;
        }
            
    }
    return 0;
}

void set_mask(MODEL_PARAMS *mp, STATE *a_state, ACTION_SET *as) {
//    ASSERT(a_state->outputs.num == ACTION_NUM && a_state->out_link == &mp->out_link_act);

    //is it actially masked    
    if (does_restrict(as)) {
        a_state->outputs.is_masked = 1;
        int a;
        for (a = 0; a < ACTION_NUM; a++) {
            if(as->acts[a] &&  a ==0){
            	a_state->outputs.mask[a] = 1;
            	a_state->outputs.mask[MAX_OUT-1] = 1;
            }
            if(as->acts[a] &&  a ==1){
				a_state->outputs.mask[a] = 1;
				a_state->outputs.mask[MAX_OUT-1] = 1;
			}
            if(as->acts[a] &&  a ==2){
            	int j ;
            	for(j=0;j<MAX_DEPREL_SIZE;j++)
            		a_state->outputs.mask[j+2] = 1;
            	a_state->outputs.mask[MAX_OUT-1] = 1;
			}

            if(as->acts[a] &&  a ==3){
				int j ;
				for(j=0;j<MAX_DEPREL_SIZE;j++)
					a_state->outputs.mask[j+MAX_DEPREL_SIZE+2] = 1;
				a_state->outputs.mask[MAX_OUT-1] = 1;
			}

        }
    } else {
        //do nothing, everything is set to 'allowed' in the ACTION_SET as
    }
}


//returns how many POS outputs are needed - takes in account $$$ and ^^^
#define get_pos_out_num() ((ap->input_offset > 0) ? mp->pos_info_out.num + ap->input_offset : mp->pos_info_out.num) 


//processes sentence and creates oracle sequence
STATE *get_oracle_seq(APPROX_PARAMS *ap, MODEL_PARAMS *mp, SENTENCE *sent, int do_free_pt) {//, FILE* fp


    STACK st; st.size = 0;
    STATE *head = create_state(-1, -1, NULL, 1, &st, 1, pt_alloc(sent->len), &mp->out_link, MAX_OUT);
    set_links(ap, mp, head, sent);
   
    STATE *a_state = head;



    while (a_state->queue != sent->len + 1) {

        ACTION_SET as = get_next_actions(ap, mp, a_state);


        set_mask(mp, a_state, &as);
            
        //check LA
        if (as.acts[LA]) {
        	//printf("LA\n");
            int d = st_peek(&a_state->stack);
            int h = a_state->queue;
            if (sent->head[d] == h) {
                //create sequence of LA operations
                //update queue and stack
            	int preOut = 2+sent->deprel[d];


            	a_state = create_state( LA, preOut, a_state, a_state->period, &a_state->stack, a_state->queue, pt_clone(a_state->pt), &mp->out_link, MAX_OUT);

            	pt_add_link(a_state->pt, h, d, sent->deprel[d]);
                st_pop(&a_state->stack);
                set_links(ap, mp, a_state, sent);//, fp
                comp_state(ap, mp, a_state);
                //DEBUG
                //printf("LA\n");

                continue;
            }
        } 
        
        //check RA
        if (as.acts[RA]) {

            int d = a_state->queue;
            int h = st_peek(&a_state->stack);


            if (sent->head[d] == h) {

                if (h != 0 || (sent->deprel[d] != ROOT_DEPREL)) {

                	int preOut = 2+MAX_DEPREL_SIZE+sent->deprel[d];


					a_state = create_state(RA, preOut, a_state, a_state->period, &a_state->stack, a_state->queue, pt_clone(a_state->pt), &mp->out_link, MAX_OUT);


                    pt_add_link(a_state->pt, h, d, sent->deprel[d]);
                    set_links(ap, mp, a_state, sent);//, fp
                    comp_state(ap, mp, a_state);

                    continue;
                }

            }
        }

        //check SHIFT
        if (as.acts[SHIFT]) {

            int q = a_state->queue;
            if (left_part_complete(a_state->pt, sent, q)) {
                if (sent->head[q] != 0  ||
                        ((sent->deprel[q] == ROOT_DEPREL || sent->deprel[q] == a_state->pt->nodes[q].deprel))) {
                    a_state = create_state( SHIFT, 0, a_state, a_state->period+1, &a_state->stack, a_state->queue, pt_clone(a_state->pt), &mp->out_link,
                           MAX_OUT);


                    st_push(&a_state->stack, a_state->queue);
                    a_state->queue++;
                    set_links(ap, mp, a_state, sent);//, fp
                    comp_state(ap, mp, a_state);

                    continue;
                }
            }
        }
        
        //check RED
        if (as.acts[RED]) {
        	//printf("RED\n");
            int s = st_peek(&a_state->stack);
            if (everything_complete(a_state->pt, sent, s)) {
                a_state = create_state( RED, 1, a_state, a_state->period, &a_state->stack, a_state->queue, pt_clone(a_state->pt), &mp->out_link,
                        MAX_OUT);

                st_pop(&a_state->stack);

                set_links(ap, mp, a_state, sent);

                comp_state(ap, mp, a_state);

                continue;
            }
        }

        ASSERT(0);
        exit(1);
    }
   
    
    if (!check_pt_t_equality(a_state->pt, sent)) {
        exit(1);
    }

    DEF_ALLOC(as, ACTION_SET);
    bzero(as, sizeof(ACTION_SET));
    set_mask(mp, a_state, as);
    free(as);
        
    TRY_FREE_PT(&(a_state->pt));
    return a_state;
}


STATE* get_seq_list (APPROX_PARAMS *ap, MODEL_PARAMS *mp, SENTENCE *sent,
        STATE_LIST **der_first, STATE_LIST **der_last, int do_free_pt) {//, FILE* fp

    *der_last = increase_list(NULL, NULL);
    *der_first = *der_last;

    STATE *last_a_state = get_oracle_seq(ap, mp, sent, do_free_pt);//, fp

    STATE *a_state = last_a_state;

    while (a_state->previous_state != NULL) {
        a_state = a_state->previous_state;
    }
    int l=0;
    while (1) {

    	l++;
        *der_last = increase_list(*der_last, a_state);
        if (a_state->next_states->prev == NULL) {
            break;
        }
        a_state = a_state->next_states->prev->elem;
    }
   
    return last_a_state;
}
STATE* get_seq_list_fromlast(APPROX_PARAMS *ap, MODEL_PARAMS *mp, SENTENCE *sent, int do_free_pt, STATE* last_a_state) {///, FILE* fp

		STATE * a_state_t = last_a_state;
		a_state_t->next_states->elem = NULL;
		while(a_state_t->previous_state != NULL){
			a_state_t->previous_state->next_states->elem = a_state_t;

			a_state_t = a_state_t->previous_state;
		}

		//printf("hereo\n");
		STATE* pre = NULL;
		STATE* new_a_state;
		while(a_state_t!=NULL){


			//printf("%d\n", a_state_t->pt->len);
			//printf("before\n");
			new_a_state = create_state( a_state_t->previous_action, a_state_t->previous_out, pre, a_state_t->period, &a_state_t->stack, a_state_t->queue, pt_clone(a_state_t->pt), &mp->out_link, MAX_OUT);
			new_a_state->outputs = a_state_t->outputs;
			//printf("after\n");
			set_links(ap, mp, new_a_state, sent);//, fp
			comp_state(ap, mp, new_a_state);
			fill_lprob(new_a_state);

		    //pt_free(&(a_state_t->pt));
		    //a_state_t->pt = NULL;
			//printf("negcopy %e \n", new_a_state->lprob);
			pre = new_a_state;
			a_state_t = a_state_t->next_states->elem;
		}


		return new_a_state;


	}

/*


STATE *get_derivation_untilshift(APPROX_PARAMS *ap, MODEL_PARAMS *mp, SENTENCE *sent, int do_free_pt, STATE* a_state, int inLearning) {//, FILE* fp
    ASSERT(ap->input_offset == 0 || ap->input_offset == 1);
    //printf("getting derivation!!\n");





    //int j=1;
    //for(j=1;j<sent->len;j++)
    //	printf("%d %d\n", j, sent->deprel[j]);
    //printf("in der\n");

    while (a_state->queue != sent->len + 1) {
        //get next possible actions
        ACTION_SET as = get_next_actions(ap, mp, a_state);
//        /printf("%d %d %d %d\n", as.acts[0],as.acts[1],as.acts[2],as.acts[3]);

        //printf("%d %d\n",  a_state->queue, sent->deprel[a_state->queue]);


        set_mask(mp, a_state, &as);

        //check LA
        if (as.acts[LA]) {
        	//printf("LA\n");
            int d = st_peek(&a_state->stack);
            int h = a_state->queue;
            if (sent->head[d] == h) {
                //create sequence of LA operations
                //update queue and stack
            	int preOut = 2+sent->deprel[d];

            	// printf("%d\n", sent->deprel[d]);
            	a_state = create_state(1, LA, preOut, a_state, a_state->period, &a_state->stack, a_state->queue, pt_clone(a_state->pt), &mp->out_link, MAX_OUT, inLearning);

            	pt_add_link(a_state->pt, h, d, sent->deprel[d]);
                st_pop(&a_state->stack);
                set_links(ap, mp, a_state, sent);//, fp


				comp_state(ap, mp, a_state);
				fill_lprob(a_state);
                //TRY_FREE_PT(&(a_state->previous_state->pt));
                //DEBUG
                //printf("LA\n");

                continue;
            }
        }

        //check RA
        if (as.acts[RA]) {
        	//printf("RA\n");
            int d = a_state->queue;
            int h = st_peek(&a_state->stack);
            //when agrees
            if (sent->head[d] == h) {
                //means if stack is not empty or  we can make RA with empty stack
                if (h != 0 || (ap->parsing_mode >= 3 && sent->deprel[d] != ROOT_DEPREL)) {

                	int preOut = 2+MAX_DEPREL_SIZE+sent->deprel[d];
                	// printf("%d\n", sent->deprel[d]);

					a_state = create_state(1, RA, preOut, a_state, a_state->period, &a_state->stack, a_state->queue, pt_clone(a_state->pt), &mp->out_link, MAX_OUT, inLearning);


                    pt_add_link(a_state->pt, h, d, sent->deprel[d]);
                    set_links(ap, mp, a_state, sent);//, fp
                    //TRY_FREE_PT(&(a_state->previous_state->pt));
    				comp_state(ap, mp, a_state);
    				fill_lprob(a_state);

                    continue;
                }

            }
        }

        //check SHIFT
        if (as.acts[SHIFT]) {
        	//printf("SHIFT\n");
            int q = a_state->queue;
            if (left_part_complete(a_state->pt, sent, q)) {
                //if atached to root with not ROOT_DEPREL and parsing_mode >= 3 then RA, not SHIFT should be peformed
                //state sent->deprel[q] == a_state->pt->nodes[q]->deprel - relates to RA- + SHIFT sequence  and means that RA- is actually preformed
                //and q is already attached
                if (sent->head[q] != 0 || ap->parsing_mode < 3 ||
                        (ap->parsing_mode >= 3 && (sent->deprel[q] == ROOT_DEPREL || sent->deprel[q] == a_state->pt->nodes[q].deprel))) {
                    //create sequence of shift operation
                    //update queue and stack
                    a_state = create_state(1, SHIFT, 0, a_state, a_state->period+1, &a_state->stack, a_state->queue, pt_clone(a_state->pt), &mp->out_link,
                           MAX_OUT, inLearning);
                    //printf("SHIFT Created\n");
                    //TRY_FREE_PT(&(a_state->pt));
                    //TRY_FREE_PT(&(a_state->previous_state->pt));
                    st_push(&a_state->stack, a_state->queue);

                    //printf("%s %d\n", "stack size is ", a_state->stack.size);
                    a_state->queue++;
//                    set_links(ap, mp, a_state, sent);/,fp
    				comp_state(ap, mp, a_state);
    				fill_lprob(a_state);

                    //printf("%d is pt len\n", a_state->pt->len );
                    //TRY_FREE_PT(&(a_state->previous_state->pt));
                    //a_state = create_word_prediction_seq(ap, mp, sent, a_state, do_free_pt);
                    //printf("%s %d\n", "here",  a_state->stack.size);
                    return a_state;
                }
            }
        }

        //check RED
        if (as.acts[RED]) {
        	//printf("RED\n");
            int s = st_peek(&a_state->stack);
            if (everything_complete(a_state->pt, sent, s)) {
                if (ap->parsing_mode != 3 || sent->head[s] != 0 || sent->deprel[s] != ROOT_DEPREL) {
                    //create sequence of reduce operation
                    //update queue and stack
                    a_state = create_state(1, RED, 1, a_state, a_state->period, &a_state->stack, a_state->queue, pt_clone(a_state->pt), &mp->out_link,
                            MAX_OUT, inLearning);
                    //TRY_FREE_PT(&(a_state->previous_state->pt));

                    st_pop(&a_state->stack);
                    set_links(ap, mp, a_state, sent);//, fp
    				comp_state(ap, mp, a_state);
    				fill_lprob(a_state);


                    continue;
                }
            }
        }

        ASSERT(0);
        fprintf(stderr, "Problem: wrong parsing order or in input file (e.g. NON-PROJECTIVITY): can't make a action\n");
        fprintf(stderr, "Try changing PARSING_ORDER in the configuration file to a larger value\n");
        exit(1);
    }


    if (!check_pt_t_equality(a_state->pt, sent)) {
        fprintf(stderr, "Presumably: bug in parsing or in input file (e.g. NON-PROJECTIVITY): resulting trees do not match\n");
        exit(1);
    }

    DEF_ALLOC(as, ACTION_SET);
    bzero(as, sizeof(ACTION_SET));
    set_mask(mp, a_state, as);
    free(as);

    TRY_FREE_PT(&(a_state->pt));
    return a_state;
}


STATE* get_derivation_list_untilshift (APPROX_PARAMS *ap, MODEL_PARAMS *mp, SENTENCE *sent,
         int do_free_pt, STATE* pre_a_state, int inLearning) {//, FILE* fp


		 STATE *last_a_state = get_derivation_untilshift(ap, mp, sent, do_free_pt, pre_a_state, inLearning);//, fp
	    return last_a_state;


	}
*/
